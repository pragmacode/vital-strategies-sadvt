# frozen_string_literal: true

module Pairings
  class Runner
    SCRIPTS_PATH = Rails.root.join('lib/pairing').freeze

    class << self
      include Pipeline

      def calculate_pairing(pairing, current_script_index)
        script = AVAILABLE_SCRIPTS[pairing.kind.to_sym][:scripts][current_script_index]
        manage_pairing_state(pairing, script, current_script_index)
        return if pairing.done?

        execute_pairing(pairing, script)
        PairingJob.perform_later(pairing.id, current_script_index + 1)
      end

      def execute_pairing(pairing, script)
        Dir.mktmpdir do |tmp_dir|
          inputs = fetch_inputs(pairing, script[:inputs], tmp_dir)
          arguments = fetch_arguments(pairing, script[:arguments])
          stdout = run(pairing, script[:file], tmp_dir, inputs, arguments)
          save_outputs(pairing, script, tmp_dir, stdout)
        end
      end

      def fetch_inputs(pairing, inputs, directory)
        files_array = inputs.map { |input| fetch_input_wrapper(pairing, input) }
        download_and_copy_inputs(files_array, directory)
        files_array.map(&:filename)
      end

      def download_and_copy_inputs(files_array, directory)
        files_array.map do |file|
          file.open(tmpdir: directory) { |tmpfile| FileUtils.copy(tmpfile.path, "#{directory}/#{file.filename}") }
        end
      end

      def fetch_arguments(pairing, argument_list)
        return '' if argument_list.nil?

        argument_list.map { |argument| "'#{evaluate_argument(argument, pairing)}'" }.join(' ')
      end

      def evaluate_argument(argument, pairing)
        pairing.send(argument[:method])
      end

      def run(pairing, script, directory, inputs, arguments)
        output_file = Tempfile.new(pairing.kind)

        inputs_full_path = inputs.map { |input| "#{directory}/#{input}" }.join(' ')

        Dir.chdir SCRIPTS_PATH do
          `Rscript #{script} #{inputs_full_path} #{directory} #{arguments} &> #{output_file.path}`
        end

        output_file
      end

      def save_outputs(pairing, script, directory, stdout)
        stdout_spec = { filename: "Programacao_de_#{script[:name]}.txt", is_final: false }
        save_output(pairing, script[:name], stdout, stdout_spec)
        Dir.entries(directory).each do |entry|
          check_and_save_dir_entry pairing, script, directory, entry
        end
      end

      def check_and_save_dir_entry(pairing, script, directory, dir_entry)
        the_output = find_output(script[:outputs], dir_entry)

        return if the_output.nil?

        file = File.open("#{directory}/#{dir_entry}")
        save_output(pairing, script[:name], file, the_output)
      end

      def find_output(list_of_outputs, searched_file)
        index = list_of_outputs.index { |output| output[:original_filename] == searched_file }
        index.nil? ? nil : list_of_outputs[index]
      end

      def save_output(pairing, script_name, file, output_spec)
        output = pairing.outputs.build(script_name: script_name, is_final: output_spec[:is_final])
        output.file.attach(io: file, filename: output_spec[:filename])
        output.save!
      end

      def fetch_input_wrapper(pairing, input)
        input[:kind] == 'Input' ? fetch_an_input(pairing, input[:type]) : fetch_an_output(pairing, input[:filename])
      end

      def fetch_an_input(pairing, type)
        pairing.inputs.where(input_type: InputType.find_by(name: type)).first.file
      end

      def fetch_an_output(pairing, filename)
        pairing.list_outputs.find { |out| out.file.filename == filename }.file
      end

      def manage_pairing_state(pairing, script, job_index)
        pairing.run! if job_index.zero?
        pairing.finish! if script.nil?
      end
    end
  end
end

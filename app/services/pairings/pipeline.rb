# frozen_string_literal: true

# rubocop:disable Metrics/ModuleLength
module Pairings
  module Pipeline
    NORMALIZE_SAT_SCRIPT = {
      name: 'normalizacao_sat',
      file: 'normalize_sat.R',
      inputs: [
        { kind: 'Input', type: 'sat_vehicles' },
        { kind: 'Input', type: 'sat_victims' },
        { kind: 'Input', type: 'sat_accidents' }
      ],
      outputs: [
        { original_filename: 'SAT_norm.csv', filename: 'Registros_do_SAT_normalizados.csv', is_final: false }
      ]
    }.freeze
    NORMALIZE_SIM_SCRIPT = {
      name: 'normalizacao_sim',
      file: 'normalize_sim.R',
      inputs: [{ kind: 'Input', type: 'sim' }],
      outputs: [{ original_filename: 'SIM_norm.csv', filename: 'Registros_do_SIM_normalizados.csv', is_final: false }]
    }.freeze
    NORMALIZE_SIH_SCRIPT = {
      name: 'normalizacao_sih',
      file: 'normalize_sih.R',
      inputs: [{ kind: 'Input', type: 'sih' }],
      outputs: [{ original_filename: 'SIH_norm.csv', filename: 'Registros_do_SIH_normalizados.csv', is_final: false }]
    }.freeze
    PAIR_SAT_SIM_SCRIPT = {
      name: 'pareamento_sat_sim',
      file: 'pair.R',
      inputs: [
        { kind: 'Output', filename: 'Registros_do_SAT_normalizados.csv' },
        { kind: 'Output', filename: 'Registros_do_SIM_normalizados.csv' }
      ],
      outputs: [
        { original_filename: 'Registros_do_SAT_normalizados_Registros_do_SIM_normalizados_pairs.csv',
          filename: 'Registros_de_pares_SAT_SIM_normalizados.csv', is_final: false }
      ]
    }.freeze
    MERGE_SAT_SIM_SCRIPT = {
      name: 'juncao_sat_sim',
      file: 'merge_by_pairing.R',
      inputs: [
        { kind: 'Output', filename: 'Registros_do_SAT_normalizados.csv' },
        { kind: 'Output', filename: 'Registros_do_SIM_normalizados.csv' },
        { kind: 'Output', filename: 'Registros_de_pares_SAT_SIM_normalizados.csv' }
      ],
      outputs: [
        { original_filename: 'Registros_do_SAT_normalizados_Registros_do_SIM_normalizados_linked_merged.csv',
          filename: 'Registros_de_pares_SAT_SIM_alta_probabilidade.csv',
          is_final: false },
        { original_filename: 'Registros_do_SAT_normalizados_Registros_do_SIM_normalizados_probable_merged.csv',
          filename: 'Registros_de_pares_SAT_SIM_menor_probabilidade.csv', is_final: false }
      ]
    }.freeze
    NORMALIZE_SAT_SIM_SCRIPT = {
      name: 'normalizacao_sat_sim',
      file: 'normalize_paired.R',
      inputs: [
        { kind: 'Output', filename: 'Registros_de_pares_SAT_SIM_alta_probabilidade.csv' }
      ],
      outputs: [
        { original_filename: 'Registros_de_pares_SAT_SIM_alta_probabilidade_norm.csv',
          filename: 'Registros_de_pares_SAT_SIM_alta_probabilidade_normalizados.csv', is_final: false }
      ]
    }.freeze
    PAIR_SAT_SIM_SIH_SCRIPT = {
      name: 'pareamento_sat_sim_sih',
      file: 'pair.R',
      inputs: [
        { kind: 'Output', filename: 'Registros_de_pares_SAT_SIM_alta_probabilidade_normalizados.csv' },
        { kind: 'Output', filename: 'Registros_do_SIH_normalizados.csv' }
      ],
      # rubocop:disable Layout/LineLength
      outputs: [
        { original_filename: 'Registros_de_pares_SAT_SIM_alta_probabilidade_normalizados_Registros_do_SIH_normalizados_pairs.csv',
          filename: 'Registros_de_pares_SAT_SIM_SIH_normalizados.csv',
          is_final: false }
      ]
      # rubocop:enable Layout/LineLength
    }.freeze
    NORMALIZE_SIM_WITH_DOC_SCRIPT = {
      name: 'normalizacao_sim_com_documento',
      file: 'normalize_sim.R',
      inputs: [{ kind: 'Input', type: 'sim_with_doc' }],
      outputs: [{ original_filename: 'SIM_norm.csv', filename: 'Registros_do_SIM_normalizados.csv', is_final: false }]
    }.freeze
    NORMALIZE_SIH_WITH_DOC_SCRIPT = {
      name: 'normalizacao_sih_com_documento',
      file: 'normalize_sih.R',
      inputs: [{ kind: 'Input', type: 'sih' }],
      outputs: [{ original_filename: 'SIH_norm.csv', filename: 'Registros_do_SIH_normalizados.csv', is_final: false }]
    }.freeze
    NORMALIZE_SAMU_WITH_DOC_SCRIPT = {
      name: 'normalizacao_samu_com_documento',
      file: 'normalize_samu.R',
      inputs: [{ kind: 'Input', type: 'samu_with_doc' }],
      outputs: [{ original_filename: 'SAMU_norm.csv', filename: 'Registros_do_SAMU_normalizados.csv', is_final: false }]
    }.freeze
    NORMALIZE_INFOSIGA_SCRIPT = {
      name: 'normalizacao_infosiga',
      file: 'normalize_infosiga.R',
      inputs: [{ kind: 'Input', type: 'infosiga' }],
      outputs: [{ original_filename: 'InfoSIGA_norm.csv', filename: 'Registros_do_InfoSIGA_normalizados.csv',
                  is_final: false }]
    }.freeze
    MERGE_INFOSIGA_SIM_SCRIPT = {
      name: 'juncao_infosiga_sim',
      file: 'merge_by_doc.R',
      inputs: [
        { kind: 'Output', filename: 'Registros_do_InfoSIGA_normalizados.csv' },
        { kind: 'Output', filename: 'Registros_do_SIM_normalizados.csv' }
      ],
      # rubocop:disable Layout/LineLength
      outputs: [
        { original_filename: 'Registros_do_InfoSIGA_normalizados_Registros_do_SIM_normalizados_deterministic_merged.csv',
          filename: 'Registros_de_pares_InfoSIGA_SIM_exatos.csv',
          is_final: false }
      ]
      # rubocop:enable Layout/LineLength
    }.freeze
    MERGE_INFOSIGA_SIH_SCRIPT = {
      name: 'juncao_infosiga_sih',
      file: 'merge_by_doc.R',
      inputs: [
        { kind: 'Output', filename: 'Registros_do_InfoSIGA_normalizados.csv' },
        { kind: 'Output', filename: 'Registros_do_SIH_normalizados.csv' }
      ],
      # rubocop:disable Layout/LineLength
      outputs: [
        { original_filename: 'Registros_do_InfoSIGA_normalizados_Registros_do_SIH_normalizados_deterministic_merged.csv',
          filename: 'Registros_de_pares_InfoSIGA_SIH_exatos.csv',
          is_final: false }
      ]
      # rubocop:enable Layout/LineLength
    }.freeze
    NORMALIZE_INFOSIGA_SIM_SCRIPT = {
      name: 'normalizacao_infosiga_sim',
      file: 'normalize_paired.R',
      inputs: [
        { kind: 'Output', filename: 'Registros_de_pares_InfoSIGA_SIM_exatos.csv' }
      ],
      outputs: [
        { original_filename: 'Registros_de_pares_InfoSIGA_SIM_exatos_norm.csv',
          filename: 'Registros_de_pares_InfoSIGA_SIM_exatos_normalizados.csv', is_final: false }
      ]
    }.freeze
    NORMALIZE_INFOSIGA_SIH_SCRIPT = {
      name: 'normalizacao_infosiga_sih',
      file: 'normalize_paired.R',
      inputs: [
        { kind: 'Output', filename: 'Registros_de_pares_InfoSIGA_SIH_exatos.csv' }
      ],
      outputs: [
        { original_filename: 'Registros_de_pares_InfoSIGA_SIH_exatos_norm.csv',
          filename: 'Registros_de_pares_InfoSIGA_SIH_exatos_normalizados.csv', is_final: false }
      ]
    }.freeze
    PAIR_INFOSIGA_SIM_SIH_SCRIPT = {
      name: 'pareamento_infosiga_sim_sih',
      file: 'pair.R',
      inputs: [
        { kind: 'Output', filename: 'Registros_de_pares_InfoSIGA_SIM_alta_probabilidade_normalizados.csv' },
        { kind: 'Output', filename: 'Registros_do_SIH_normalizados_sem_pares_exatos.csv' }
      ],
      # rubocop:disable Layout/LineLength
      outputs: [
        { original_filename: 'Registros_de_pares_InfoSIGA_SIM_alta_probabilidade_normalizados_Registros_do_SIH_normalizados_sem_pares_exatos_pairs.csv',
          filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_normalizados.csv',
          is_final: false }
      ]
      # rubocop:enable Layout/LineLength
    }.freeze
    PAIR_INFOSIGA_SIM_SIH_SAMU_SCRIPT = {
      name: 'pareamento_infosiga_sim_sih_samu',
      file: 'pair.R',
      inputs: [
        { kind: 'Output', filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_alta_probabilidade_normalizados.csv' },
        { kind: 'Output', filename: 'Registros_do_SAMU_normalizados_sem_pares_exatos.csv' }
      ],
      # rubocop:disable Layout/LineLength
      outputs: [
        { original_filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_alta_probabilidade_normalizados_Registros_do_SAMU_normalizados_sem_pares_exatos_pairs.csv',
          filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_SAMU_normalizados.csv',
          is_final: false }
      ]
      # rubocop:enable Layout/LineLength
    }.freeze
    MERGE_INFOSIGA_SIM_SIH_SCRIPT = {
      name: 'juncao_infosiga_sim_sih_prob',
      file: 'merge_by_pairing.R',
      inputs: [
        { kind: 'Output', filename: 'Registros_de_pares_InfoSIGA_SIM_alta_probabilidade_normalizados.csv' },
        { kind: 'Output', filename: 'Registros_do_SIH_normalizados_sem_pares_exatos.csv' },
        { kind: 'Output',
          filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_normalizados.csv' }
      ],
      # rubocop:disable Layout/LineLength
      outputs: [
        {
          original_filename: 'Registros_de_pares_InfoSIGA_SIM_alta_probabilidade_normalizados_Registros_do_SIH_normalizados_sem_pares_exatos_linked_merged.csv',
          filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_alta_probabilidade.csv',
          is_final: false
        },
        {
          original_filename: 'Registros_de_pares_InfoSIGA_SIM_alta_probabilidade_normalizados_Registros_do_SIH_normalizados_sem_pares_exatos_probable_merged.csv',
          filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_menor_probabilidade.csv',
          is_final: false
        }
      ]
      # rubocop:enable Layout/LineLength
    }.freeze
    MERGE_INFOSIGA_SIM_SIH_SAMU_SCRIPT = {
      name: 'juncao_infosiga_sim_sih_samu_prob',
      file: 'merge_by_pairing.R',
      inputs: [
        { kind: 'Output', filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_alta_probabilidade_normalizados.csv' },
        { kind: 'Output', filename: 'Registros_do_SAMU_normalizados_sem_pares_exatos.csv' },
        { kind: 'Output',
          filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_SAMU_normalizados.csv' }
      ],
      # rubocop:disable Layout/LineLength
      outputs: [
        {
          original_filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_alta_probabilidade_normalizados_Registros_do_SAMU_normalizados_sem_pares_exatos_linked_merged.csv',
          filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_SAMU_alta_probabilidade.csv',
          is_final: false
        },
        {
          original_filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_alta_probabilidade_normalizados_Registros_do_SAMU_normalizados_sem_pares_exatos_probable_merged.csv',
          filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_SAMU_menor_probabilidade.csv',
          is_final: false
        }
      ]
      # rubocop:enable Layout/LineLength
    }.freeze
    REMOVE_PAIRED_BY_DOC_FROM_SIM_SCRIPT = {
      name: 'remocao_pareados_sim',
      file: 'remove_paired_by_doc.R',
      inputs: [
        { kind: 'Output', filename: 'Registros_do_SIM_normalizados.csv' },
        { kind: 'Output', filename: 'Registros_de_pares_InfoSIGA_SIM_exatos_normalizados.csv' }
      ],
      outputs: [
        { original_filename: 'Registros_do_SIM_normalizados_cleaned.csv',
          filename: 'Registros_do_SIM_normalizados_sem_pares_exatos.csv', is_final: false }
      ]
    }.freeze
    REMOVE_PAIRED_BY_DOC_FROM_SIH_SCRIPT = {
      name: 'remocao_pareados_sih',
      file: 'remove_paired_by_doc.R',
      inputs: [
        { kind: 'Output', filename: 'Registros_do_SIH_normalizados.csv' },
        { kind: 'Output', filename: 'Registros_de_pares_InfoSIGA_SIH_exatos_normalizados.csv' }
      ],
      outputs: [
        { original_filename: 'Registros_do_SIH_normalizados_cleaned.csv',
          filename: 'Registros_do_SIH_normalizados_sem_pares_exatos.csv', is_final: false }
      ]
    }.freeze
    REMOVE_PAIRED_BY_DOC_FROM_INFOSIGA_SCRIPT = {
      name: 'remocao_pareados_infosiga',
      file: 'remove_paired_by_doc.R',
      inputs: [
        { kind: 'Output', filename: 'Registros_do_InfoSIGA_normalizados.csv' },
        { kind: 'Output', filename: 'Registros_de_pares_InfoSIGA_SIM_exatos_normalizados.csv' }
      ],
      outputs: [
        { original_filename: 'Registros_do_InfoSIGA_normalizados_cleaned.csv',
          filename: 'Registros_do_InfoSIGA_normalizados_sem_pares_exatos.csv', is_final: false }
      ]
    }.freeze
    REMOVE_PAIRED_BY_DOC_FROM_INFOSIGA_SIH_SCRIPT = {
      name: 'remocao_pareados_infosiga',
      file: 'remove_paired_by_doc.R',
      inputs: [
        { kind: 'Output', filename: 'Registros_do_InfoSIGA_normalizados.csv' },
        { kind: 'Output', filename: 'Registros_de_pares_InfoSIGA_SIH_exatos_normalizados.csv' }
      ],
      outputs: [
        { original_filename: 'Registros_do_InfoSIGA_normalizados_cleaned.csv',
          filename: 'Registros_do_InfoSIGA_normalizados_sem_pares_exatos.csv', is_final: false }
      ]
    }.freeze
    PAIR_INFOSIGA_SIM_SCRIPT = {
      name: 'pareamento_infosiga_sim',
      file: 'pair.R',
      inputs: [
        { kind: 'Output', filename: 'Registros_do_InfoSIGA_normalizados_sem_pares_exatos.csv' },
        { kind: 'Output', filename: 'Registros_do_SIM_normalizados_sem_pares_exatos.csv' }
      ],
      outputs: [
        # rubocop:disable Layout/LineLength
        { original_filename: 'Registros_do_InfoSIGA_normalizados_sem_pares_exatos_Registros_do_SIM_normalizados_sem_pares_exatos_pairs.csv',
          filename: 'Registros_de_pares_InfoSIGA_SIM_probabilisticos.csv', is_final: false }
        # rubocop:enable Layout/LineLength
      ]
    }.freeze
    PAIR_INFOSIGA_SIH_SCRIPT = {
      name: 'pareamento_infosiga_sih',
      file: 'pair.R',
      inputs: [
        { kind: 'Output', filename: 'Registros_do_InfoSIGA_normalizados_sem_pares_exatos.csv' },
        { kind: 'Output', filename: 'Registros_do_SIH_normalizados_sem_pares_exatos.csv' }
      ],
      outputs: [
        # rubocop:disable Layout/LineLength
        { original_filename: 'Registros_do_InfoSIGA_normalizados_sem_pares_exatos_Registros_do_SIH_normalizados_sem_pares_exatos_pairs.csv',
          filename: 'Registros_de_pares_InfoSIGA_SIH_probabilisticos.csv', is_final: false }
        # rubocop:enable Layout/LineLength
      ]
    }.freeze
    MERGE_INFOSIGA_SIM_PROB_SCRIPT = {
      name: 'juncao_infosiga_sim_probabilistica',
      file: 'merge_by_pairing.R',
      inputs: [
        { kind: 'Output', filename: 'Registros_do_InfoSIGA_normalizados_sem_pares_exatos.csv' },
        { kind: 'Output', filename: 'Registros_do_SIM_normalizados_sem_pares_exatos.csv' },
        { kind: 'Output', filename: 'Registros_de_pares_InfoSIGA_SIM_probabilisticos.csv' }
      ],
      outputs: [
        # rubocop:disable Layout/LineLength
        { original_filename: 'Registros_do_InfoSIGA_normalizados_sem_pares_exatos_Registros_do_SIM_normalizados_sem_pares_exatos_linked_merged.csv',
          filename: 'Registros_de_pares_InfoSIGA_SIM_alta_probabilidade.csv',
          is_final: false },
        { original_filename: 'Registros_do_InfoSIGA_normalizados_sem_pares_exatos_Registros_do_SIM_normalizados_sem_pares_exatos_probable_merged.csv',
          filename: 'Registros_de_pares_InfoSIGA_SIM_menor_probabilidade.csv', is_final: false }
        # rubocop:enable Layout/LineLength
      ]
    }.freeze
    MERGE_INFOSIGA_SIH_PROB_SCRIPT = {
      name: 'juncao_infosiga_sih_probabilistica',
      file: 'merge_by_pairing.R',
      inputs: [
        { kind: 'Output', filename: 'Registros_do_InfoSIGA_normalizados_sem_pares_exatos.csv' },
        { kind: 'Output', filename: 'Registros_do_SIH_normalizados_sem_pares_exatos.csv' },
        { kind: 'Output', filename: 'Registros_de_pares_InfoSIGA_SIH_probabilisticos.csv' }
      ],
      outputs: [
        # rubocop:disable Layout/LineLength
        { original_filename: 'Registros_do_InfoSIGA_normalizados_sem_pares_exatos_Registros_do_SIH_normalizados_sem_pares_exatos_linked_merged.csv',
          filename: 'Registros_de_pares_InfoSIGA_SIH_alta_probabilidade.csv',
          is_final: false },
        { original_filename: 'Registros_do_InfoSIGA_normalizados_sem_pares_exatos_Registros_do_SIH_normalizados_sem_pares_exatos_probable_merged.csv',
          filename: 'Registros_de_pares_InfoSIGA_SIH_menor_probabilidade.csv', is_final: false }
        # rubocop:enable Layout/LineLength
      ]
    }.freeze
    MERGE_INFOSIGA_SIM_SIH_WITH_DOC_SCRIPT = {
      name: 'juncao_infosiga_sim_sih',
      file: 'merge_by_doc.R',
      inputs: [
        { kind: 'Output', filename: 'Registros_de_pares_InfoSIGA_SIM_exatos_normalizados.csv' },
        { kind: 'Output', filename: 'Registros_do_SIH_normalizados.csv' }
      ],
      # rubocop:disable Layout/LineLength
      outputs: [
        { original_filename: 'Registros_de_pares_InfoSIGA_SIM_exatos_normalizados_Registros_do_SIH_normalizados_deterministic_merged.csv',
          filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_exatos.csv',
          is_final: false }
      ]
      # rubocop:enable Layout/LineLength
    }.freeze
    MERGE_INFOSIGA_SIM_SIH_SAMU_WITH_DOC_SCRIPT = {
      name: 'juncao_infosiga_sim_sih_samu',
      file: 'merge_by_doc.R',
      inputs: [
        { kind: 'Output', filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_exatos_normalizados.csv' },
        { kind: 'Output', filename: 'Registros_do_SAMU_normalizados.csv' }
      ],
      # rubocop:disable Layout/LineLength
      outputs: [
        { original_filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_exatos_normalizados_Registros_do_SAMU_normalizados_deterministic_merged.csv',
          filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_SAMU_exatos.csv',
          is_final: false }
      ]
      # rubocop:enable Layout/LineLength
    }.freeze
    NORMALIZE_INFOSIGA_SIM_SIH_SCRIPT = {
      name: 'normalizacao_infosiga_sim_sih',
      file: 'normalize_paired.R',
      inputs: [
        { kind: 'Output', filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_exatos.csv' }
      ],
      outputs: [
        { original_filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_exatos_norm.csv',
          filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_exatos_normalizados.csv', is_final: false }
      ]
    }.freeze
    NORMALIZE_INFOSIGA_SIM_SIH_SAMU_SCRIPT = {
      name: 'normalizacao_infosiga_sim_sih',
      file: 'normalize_paired.R',
      inputs: [
        { kind: 'Output', filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_SAMU_exatos.csv' }
      ],
      outputs: [
        { original_filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_SAMU_exatos_norm.csv',
          filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_SAMU_exatos_normalizados.csv', is_final: false }
      ]
    }.freeze
    REMOVE_PAIRED_BY_DOC_FROM_SIM_SCRIPT_ON_INFOSIGA_SIM_SIH = {
      name: 'remocao_pareados_sim_from_infosiga_sim_sih',
      file: 'remove_paired_by_doc.R',
      inputs: [
        { kind: 'Output', filename: 'Registros_do_SIM_normalizados.csv' },
        { kind: 'Output', filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_exatos_normalizados.csv' }
      ],
      outputs: [
        { original_filename: 'Registros_do_SIM_normalizados_cleaned.csv',
          filename: 'Registros_do_SIM_normalizados_sem_pares_exatos.csv', is_final: false }
      ]
    }.freeze
    REMOVE_PAIRED_BY_DOC_FROM_SIH_SCRIPT_ON_INFOSIGA_SIM_SIH = {
      name: 'remocao_pareados_sih_from_infosiga_sim_sih',
      file: 'remove_paired_by_doc.R',
      inputs: [
        { kind: 'Output', filename: 'Registros_do_SIH_normalizados.csv' },
        { kind: 'Output', filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_exatos_normalizados.csv' }
      ],
      outputs: [
        { original_filename: 'Registros_do_SIH_normalizados_cleaned.csv',
          filename: 'Registros_do_SIH_normalizados_sem_pares_exatos.csv', is_final: false }
      ]
    }.freeze
    REMOVE_PAIRED_BY_DOC_FROM_INFOSIGA_SCRIPT_ON_INFOSIGA_SIM_SIH = {
      name: 'remocao_pareados_infosiga_from_infosiga_sim_sih',
      file: 'remove_paired_by_doc.R',
      inputs: [
        { kind: 'Output', filename: 'Registros_do_InfoSIGA_normalizados.csv' },
        { kind: 'Output', filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_exatos_normalizados.csv' }
      ],
      outputs: [
        { original_filename: 'Registros_do_InfoSIGA_normalizados_cleaned.csv',
          filename: 'Registros_do_InfoSIGA_normalizados_sem_pares_exatos.csv', is_final: false }
      ]
    }.freeze
    NORMALIZE_INFOSIGA_SIM_SCRIPT_PROB_SCRIPT = {
      name: 'normaliza_infosiga_sim_prob',
      file: 'normalize_paired.R',
      inputs: [
        { kind: 'Output', filename: 'Registros_de_pares_InfoSIGA_SIM_alta_probabilidade.csv' }
      ],
      outputs: [
        { original_filename: 'Registros_de_pares_InfoSIGA_SIM_alta_probabilidade_norm.csv',
          filename: 'Registros_de_pares_InfoSIGA_SIM_alta_probabilidade_normalizados.csv', is_final: false }
      ]
    }.freeze
    NORMALIZE_INFOSIGA_SIM_SIH_SCRIPT_PROB_SCRIPT = {
      name: 'normaliza_infosiga_sim_sih_prob',
      file: 'normalize_paired.R',
      inputs: [
        { kind: 'Output', filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_alta_probabilidade.csv' }
      ],
      outputs: [
        { original_filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_alta_probabilidade_norm.csv',
          filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_alta_probabilidade_normalizados.csv', is_final: false }
      ]
    }.freeze
    REMOVE_PAIRED_BY_DOC_FROM_SIM_SCRIPT_ON_INFOSIGA_SIM_SIH_SAMU = {
      name: 'remocao_pareados_sim_from_infosiga_sim_sih_samu',
      file: 'remove_paired_by_doc.R',
      inputs: [
        { kind: 'Output', filename: 'Registros_do_SIM_normalizados.csv' },
        { kind: 'Output', filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_SAMU_exatos_normalizados.csv' }
      ],
      outputs: [
        { original_filename: 'Registros_do_SIM_normalizados_cleaned.csv',
          filename: 'Registros_do_SIM_normalizados_sem_pares_exatos.csv', is_final: false }
      ]
    }.freeze
    REMOVE_PAIRED_BY_DOC_FROM_SIH_SCRIPT_ON_INFOSIGA_SIM_SIH_SAMU = {
      name: 'remocao_pareados_sih_from_infosiga_sim_sih_samu',
      file: 'remove_paired_by_doc.R',
      inputs: [
        { kind: 'Output', filename: 'Registros_do_SIH_normalizados.csv' },
        { kind: 'Output', filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_SAMU_exatos_normalizados.csv' }
      ],
      outputs: [
        { original_filename: 'Registros_do_SIH_normalizados_cleaned.csv',
          filename: 'Registros_do_SIH_normalizados_sem_pares_exatos.csv', is_final: false }
      ]
    }.freeze
    REMOVE_PAIRED_BY_DOC_FROM_INFOSIGA_SCRIPT_ON_INFOSIGA_SIM_SIH_SAMU = {
      name: 'remocao_pareados_infosiga_from_infosiga_sim_sih_samu',
      file: 'remove_paired_by_doc.R',
      inputs: [
        { kind: 'Output', filename: 'Registros_do_InfoSIGA_normalizados.csv' },
        { kind: 'Output', filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_SAMU_exatos_normalizados.csv' }
      ],
      outputs: [
        { original_filename: 'Registros_do_InfoSIGA_normalizados_cleaned.csv',
          filename: 'Registros_do_InfoSIGA_normalizados_sem_pares_exatos.csv', is_final: false }
      ]
    }.freeze
    REMOVE_PAIRED_BY_DOC_FROM_SAMU_SCRIPT_ON_INFOSIGA_SIM_SIH_SAMU = {
      name: 'remocao_pareados_samu_from_infosiga_sim_sih_samu',
      file: 'remove_paired_by_doc.R',
      inputs: [
        { kind: 'Output', filename: 'Registros_do_SAMU_normalizados.csv' },
        { kind: 'Output', filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_SAMU_exatos_normalizados.csv' }
      ],
      outputs: [
        { original_filename: 'Registros_do_SAMU_normalizados_cleaned.csv',
          filename: 'Registros_do_SAMU_normalizados_sem_pares_exatos.csv', is_final: false }
      ]
    }.freeze
    AVAILABLE_SCRIPTS = {
      sat_sim: {
        scripts: [
          NORMALIZE_SAT_SCRIPT,
          NORMALIZE_SIM_SCRIPT,
          PAIR_SAT_SIM_SCRIPT,
          {
            name: 'juncao_sat_sim',
            file: 'merge_by_pairing.R',
            inputs: [
              { kind: 'Output', filename: 'Registros_do_SAT_normalizados.csv' },
              { kind: 'Output', filename: 'Registros_do_SIM_normalizados.csv' },
              { kind: 'Output', filename: 'Registros_de_pares_SAT_SIM_normalizados.csv' }
            ],
            outputs: [
              { original_filename: 'Registros_do_SAT_normalizados_Registros_do_SIM_normalizados_linked_merged.csv',
                filename: 'Registros_de_pares_SAT_SIM_alta_probabilidade.csv', is_final: false },
              { original_filename: 'Registros_do_SAT_normalizados_Registros_do_SIM_normalizados_probable_merged.csv',
                filename: 'Registros_de_pares_SAT_SIM_menor_probabilidade.csv', is_final: false }
            ]
          },
          {
            name: 'filtragem_de_colunas_sat_sim',
            file: 'filter_columns.R',
            inputs: [
              { kind: 'Output', filename: 'Registros_de_pares_SAT_SIM_alta_probabilidade.csv' },
              { kind: 'Output', filename: 'Registros_de_pares_SAT_SIM_menor_probabilidade.csv' }
            ],
            arguments: [{ method: :selected_input_columns_titles }],
            outputs: [
              { original_filename: 'Registros_de_pares_SAT_SIM_alta_probabilidade_filtered.csv',
                filename: 'Registros_de_pares_SAT_SIM_alta_probabilidade_colunas_selecionadas.csv', is_final: true },
              { original_filename: 'Registros_de_pares_SAT_SIM_menor_probabilidade_filtered.csv',
                filename: 'Registros_de_pares_SAT_SIM_menor_probabilidade_colunas_selecionadas.csv', is_final: true }
            ]
          }
        ]
      },
      sat_sih: {
        scripts: [
          NORMALIZE_SAT_SCRIPT,
          NORMALIZE_SIH_SCRIPT,
          {
            name: 'pareamento_sat_sih',
            file: 'pair.R',
            inputs: [
              { kind: 'Output', filename: 'Registros_do_SAT_normalizados.csv' },
              { kind: 'Output', filename: 'Registros_do_SIH_normalizados.csv' }
            ],
            outputs: [
              { original_filename: 'Registros_do_SAT_normalizados_Registros_do_SIH_normalizados_pairs.csv',
                filename: 'Registros_de_pares_SAT_SIH_normalizados.csv',
                is_final: false }
            ]
          },
          {
            name: 'juncao_sat_sih',
            file: 'merge_by_pairing.R',
            inputs: [
              { kind: 'Output', filename: 'Registros_do_SAT_normalizados.csv' },
              { kind: 'Output', filename: 'Registros_do_SIH_normalizados.csv' },
              { kind: 'Output', filename: 'Registros_de_pares_SAT_SIH_normalizados.csv' }
            ],
            outputs: [
              { original_filename: 'Registros_do_SAT_normalizados_Registros_do_SIH_normalizados_linked_merged.csv',
                filename: 'Registros_de_pares_SAT_SIH_alta_probabilidade.csv', is_final: false },
              { original_filename: 'Registros_do_SAT_normalizados_Registros_do_SIH_normalizados_probable_merged.csv',
                filename: 'Registros_de_pares_SAT_SIH_menor_probabilidade.csv', is_final: false }
            ]
          },
          {
            name: 'filtragem_de_colunas_sat_sih',
            file: 'filter_columns.R',
            inputs: [
              { kind: 'Output', filename: 'Registros_de_pares_SAT_SIH_alta_probabilidade.csv' },
              { kind: 'Output', filename: 'Registros_de_pares_SAT_SIH_menor_probabilidade.csv' }
            ],
            arguments: [{ method: :selected_input_columns_titles }],
            outputs: [
              { original_filename: 'Registros_de_pares_SAT_SIH_alta_probabilidade_filtered.csv',
                filename: 'Registros_de_pares_SAT_SIH_alta_probabilidade_colunas_selecionadas.csv', is_final: true },
              { original_filename: 'Registros_de_pares_SAT_SIH_menor_probabilidade_filtered.csv',
                filename: 'Registros_de_pares_SAT_SIH_menor_probabilidade_colunas_selecionadas.csv', is_final: true }
            ]
          }
        ]
      },
      sat_sim_sih: {
        scripts: [
          NORMALIZE_SAT_SCRIPT,
          NORMALIZE_SIM_SCRIPT,
          PAIR_SAT_SIM_SCRIPT,
          MERGE_SAT_SIM_SCRIPT,
          NORMALIZE_SIH_SCRIPT,
          NORMALIZE_SAT_SIM_SCRIPT,
          PAIR_SAT_SIM_SIH_SCRIPT,
          {
            name: 'juncao_sat_sim_sih',
            file: 'merge_by_pairing.R',
            inputs: [
              { kind: 'Output', filename: 'Registros_de_pares_SAT_SIM_alta_probabilidade_normalizados.csv' },
              { kind: 'Output', filename: 'Registros_do_SIH_normalizados.csv' },
              { kind: 'Output',
                filename: 'Registros_de_pares_SAT_SIM_SIH_normalizados.csv' }
            ],
            # rubocop:disable Layout/LineLength
            outputs: [
              {
                original_filename: 'Registros_de_pares_SAT_SIM_alta_probabilidade_normalizados_Registros_do_SIH_normalizados_linked_merged.csv',
                filename: 'Registros_de_pares_SAT_SIM_SIH_alta_probabilidade.csv',
                is_final: false
              },
              {
                original_filename: 'Registros_de_pares_SAT_SIM_alta_probabilidade_normalizados_Registros_do_SIH_normalizados_probable_merged.csv',
                filename: 'Registros_de_pares_SAT_SIM_SIH_menor_probabilidade.csv',
                is_final: false
              }
            ]
            # rubocop:enable Layout/LineLength
          },
          {
            name: 'filtragem_de_colunas_sat_sim_sih',
            file: 'filter_columns.R',
            inputs: [
              { kind: 'Output',
                filename: 'Registros_de_pares_SAT_SIM_SIH_alta_probabilidade.csv' },
              { kind: 'Output',
                filename: 'Registros_de_pares_SAT_SIM_SIH_menor_probabilidade.csv' }
            ],
            arguments: [{ method: :selected_input_columns_titles }],
            outputs: [
              { original_filename: 'Registros_de_pares_SAT_SIM_SIH_alta_probabilidade_filtered.csv',
                filename: 'Registros_de_pares_SAT_SIM_SIH_alta_probabilidade_colunas_selecionadas.csv',
                is_final: true },
              { original_filename: 'Registros_de_pares_SAT_SIM_SIH_menor_probabilidade_filtered.csv',
                filename: 'Registros_de_pares_SAT_SIM_SIH_menor_probabilidade_colunas_selecionadas.csv',
                is_final: true }
            ]
          }
        ]
      },
      sat_sim_sih_samu: {
        scripts: [
          NORMALIZE_SAT_SCRIPT,
          NORMALIZE_SIM_SCRIPT,
          PAIR_SAT_SIM_SCRIPT,
          MERGE_SAT_SIM_SCRIPT,
          NORMALIZE_SIH_SCRIPT,
          NORMALIZE_SAT_SIM_SCRIPT,
          PAIR_SAT_SIM_SIH_SCRIPT,
          {
            name: 'juncao_sat_sim_sih',
            file: 'merge_by_pairing.R',
            inputs: [
              { kind: 'Output', filename: 'Registros_de_pares_SAT_SIM_alta_probabilidade_normalizados.csv' },
              { kind: 'Output', filename: 'Registros_do_SIH_normalizados.csv' },
              { kind: 'Output',
                filename: 'Registros_de_pares_SAT_SIM_SIH_normalizados.csv' }
            ],
            # rubocop:disable Layout/LineLength
            outputs: [
              {
                original_filename: 'Registros_de_pares_SAT_SIM_alta_probabilidade_normalizados_Registros_do_SIH_normalizados_linked_merged.csv',
                filename: 'Registros_de_pares_SAT_SIM_SIH_alta_probabilidade.csv',
                is_final: false
              },
              {
                original_filename: 'Registros_de_pares_SAT_SIM_alta_probabilidade_normalizados_Registros_do_SIH_normalizados_probable_merged.csv',
                filename: 'Registros_de_pares_SAT_SIM_SIH_menor_probabilidade.csv',
                is_final: false
              }
            ]
            # rubocop:enable Layout/LineLength
          },
          {
            name: 'normalizacao_sat_sim_sih',
            file: 'normalize_paired.R',
            inputs: [
              { kind: 'Output',
                filename: 'Registros_de_pares_SAT_SIM_SIH_alta_probabilidade.csv' }
            ],
            outputs: [
              { original_filename: 'Registros_de_pares_SAT_SIM_SIH_alta_probabilidade_norm.csv',
                filename: 'Registros_de_pares_SAT_SIM_SIH_alta_probabilidade_normalizados.csv', is_final: false }
            ]
          },
          {
            # rubocop:disable Layout/LineLength
            name: 'normalizacao_samu',
            file: 'normalize_samu.R',
            inputs: [{ kind: 'Input', type: 'samu' }],
            outputs: [{ original_filename: 'SAMU_norm.csv', filename: 'Registros_SAMU_normalizados.csv', is_final: false }]
            # rubocop:enable Layout/LineLength
          },
          {
            name: 'pareamento_sat_sim_sih_samu',
            file: 'pair.R',
            inputs: [
              { kind: 'Output',
                filename: 'Registros_de_pares_SAT_SIM_SIH_alta_probabilidade_normalizados.csv' },
              { kind: 'Output', filename: 'Registros_SAMU_normalizados.csv' }
            ],
            # rubocop:disable Layout/LineLength
            outputs: [
              {
                original_filename: 'Registros_de_pares_SAT_SIM_SIH_alta_probabilidade_normalizados_Registros_SAMU_normalizados_pairs.csv',
                filename: 'Registro_pares_SAT_SIM_SIH_SAMU_normalizados.csv',
                is_final: false
              }
            ]
            # rubocop:enable Layout/LineLength
          },
          {
            name: 'juncao_sat_sim_sih_samu',
            file: 'merge_by_pairing.R',
            # rubocop:disable Layout/LineLength
            inputs: [
              { kind: 'Output',
                filename: 'Registros_de_pares_SAT_SIM_SIH_alta_probabilidade_normalizados.csv' },
              { kind: 'Output', filename: 'Registros_SAMU_normalizados.csv' },
              {
                kind: 'Output',
                filename: 'Registro_pares_SAT_SIM_SIH_SAMU_normalizados.csv'
              }
            ],
            outputs: [
              {
                original_filename: 'Registros_de_pares_SAT_SIM_SIH_alta_probabilidade_normalizados_Registros_SAMU_normalizados_linked_merged.csv',
                filename: 'Registros_de_pares_SAT_SIM_SIH_SAMU_alta_probabilidade.csv',
                is_final: false
              },
              {
                original_filename: 'Registros_de_pares_SAT_SIM_SIH_alta_probabilidade_normalizados_Registros_SAMU_normalizados_probable_merged.csv',
                filename: 'Registros_de_pares_SAT_SIM_SIH_SAMU_menor_probabilidade.csv',
                is_final: false
              }
            ]
          },
          {
            name: 'filtragem_de_colunas_sat_sim_sih_samu',
            file: 'filter_columns.R',
            inputs: [
              {
                kind: 'Output',
                filename: 'Registros_de_pares_SAT_SIM_SIH_SAMU_alta_probabilidade.csv'
              },
              {
                kind: 'Output',
                filename: 'Registros_de_pares_SAT_SIM_SIH_SAMU_menor_probabilidade.csv'
              }
            ],
            arguments: [{ method: :selected_input_columns_titles }],
            outputs: [
              {
                original_filename: 'Registros_de_pares_SAT_SIM_SIH_SAMU_alta_probabilidade_filtered.csv',
                filename: 'Registros_de_pares_SAT_SIM_SIH_SAMU_alta_probabilidade_colunas_selecionadas.csv',
                is_final: true
              },
              {
                original_filename: 'Registros_de_pares_SAT_SIM_SIH_SAMU_menor_probabilidade_filtered.csv',
                filename: 'Registros_de_pares_SAT_SIM_SIH_SAMU_menor_probabilidade_colunas_selecionadas.csv',
                is_final: true
              }
              # rubocop:enable Layout/LineLength
            ]
          }
        ]
      },
      infosiga_sim: {
        scripts: [
          NORMALIZE_SIM_WITH_DOC_SCRIPT,
          NORMALIZE_INFOSIGA_SCRIPT,
          MERGE_INFOSIGA_SIM_SCRIPT,
          {
            name: 'filtragem_de_colunas_infosiga_sim',
            file: 'filter_columns.R',
            inputs: [
              # rubocop:disable Layout/LineLength
              { kind: 'Output', filename: 'Registros_de_pares_InfoSIGA_SIM_exatos.csv' }, # Intentionally duplicated as a workaround to avid modifying the script
              # rubocop:enable Layout/LineLength
              { kind: 'Output', filename: 'Registros_de_pares_InfoSIGA_SIM_exatos.csv' }
            ],
            arguments: [{ method: :selected_input_columns_titles }],
            outputs: [
              { original_filename: 'Registros_de_pares_InfoSIGA_SIM_exatos_filtered.csv',
                filename: 'Registros_de_pares_InfoSIGA_SIM_exatos_colunas_selecionadas.csv', is_final: true }
            ]
          },
          NORMALIZE_INFOSIGA_SIM_SCRIPT,
          REMOVE_PAIRED_BY_DOC_FROM_SIM_SCRIPT,
          REMOVE_PAIRED_BY_DOC_FROM_INFOSIGA_SCRIPT,
          PAIR_INFOSIGA_SIM_SCRIPT,
          MERGE_INFOSIGA_SIM_PROB_SCRIPT,
          {
            name: 'filtragem_de_colunas_infosiga_sim_probabilistica',
            file: 'filter_columns.R',
            inputs: [
              { kind: 'Output', filename: 'Registros_de_pares_InfoSIGA_SIM_alta_probabilidade.csv' },
              { kind: 'Output', filename: 'Registros_de_pares_InfoSIGA_SIM_menor_probabilidade.csv' }
            ],
            arguments: [{ method: :selected_input_columns_titles }],
            outputs: [
              {
                original_filename: 'Registros_de_pares_InfoSIGA_SIM_alta_probabilidade_filtered.csv',
                filename: 'Registros_de_pares_InfoSIGA_SIM_alta_probabilidade_colunas_selecionadas.csv',
                is_final: true
              },
              {
                original_filename: 'Registros_de_pares_InfoSIGA_SIM_menor_probabilidade_filtered.csv',
                filename: 'Registros_de_pares_InfoSIGA_SIM_menor_probabilidade_colunas_selecionadas.csv',
                is_final: true
              }
            ]
          }
        ]
      },
      infosiga_sih: {
        scripts: [
          NORMALIZE_SIH_WITH_DOC_SCRIPT,
          NORMALIZE_INFOSIGA_SCRIPT,
          MERGE_INFOSIGA_SIH_SCRIPT,
          {
            name: 'filtragem_de_colunas_infosiga_sih',
            file: 'filter_columns.R',
            inputs: [ # Intentionally duplicated files as a workaround to avoid modifying the script
              { kind: 'Output', filename: 'Registros_de_pares_InfoSIGA_SIH_exatos.csv' },
              { kind: 'Output', filename: 'Registros_de_pares_InfoSIGA_SIH_exatos.csv' }
            ],
            arguments: [{ method: :selected_input_columns_titles }],
            outputs: [
              { original_filename: 'Registros_de_pares_InfoSIGA_SIH_exatos_filtered.csv',
                filename: 'Registros_de_pares_InfoSIGA_SIH_exatos_colunas_selecionadas.csv', is_final: true }
            ]
          },
          NORMALIZE_INFOSIGA_SIH_SCRIPT,
          REMOVE_PAIRED_BY_DOC_FROM_SIH_SCRIPT,
          REMOVE_PAIRED_BY_DOC_FROM_INFOSIGA_SIH_SCRIPT,
          PAIR_INFOSIGA_SIH_SCRIPT,
          MERGE_INFOSIGA_SIH_PROB_SCRIPT,
          {
            name: 'filtragem_de_colunas_infosiga_sih_probabilistica',
            file: 'filter_columns.R',
            inputs: [
              { kind: 'Output', filename: 'Registros_de_pares_InfoSIGA_SIH_alta_probabilidade.csv' },
              { kind: 'Output', filename: 'Registros_de_pares_InfoSIGA_SIH_menor_probabilidade.csv' }
            ],
            arguments: [{ method: :selected_input_columns_titles }],
            outputs: [
              {
                original_filename: 'Registros_de_pares_InfoSIGA_SIH_alta_probabilidade_filtered.csv',
                filename: 'Registros_de_pares_InfoSIGA_SIH_alta_probabilidade_colunas_selecionadas.csv',
                is_final: true
              },
              {
                original_filename: 'Registros_de_pares_InfoSIGA_SIH_menor_probabilidade_filtered.csv',
                filename: 'Registros_de_pares_InfoSIGA_SIH_menor_probabilidade_colunas_selecionadas.csv',
                is_final: true
              }
            ]
          }
        ]
      },
      infosiga_sim_sih: {
        scripts: [
          NORMALIZE_SIM_WITH_DOC_SCRIPT,
          NORMALIZE_INFOSIGA_SCRIPT,
          MERGE_INFOSIGA_SIM_SCRIPT,
          NORMALIZE_INFOSIGA_SIM_SCRIPT,
          NORMALIZE_SIH_WITH_DOC_SCRIPT,
          MERGE_INFOSIGA_SIM_SIH_WITH_DOC_SCRIPT,
          {
            name: 'filtragem_de_colunas_infosiga_sim_sih',
            file: 'filter_columns.R',
            inputs: [ # Intentionally duplicated files as a workaround to avoid modifying the script
              { kind: 'Output', filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_exatos.csv' },
              { kind: 'Output', filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_exatos.csv' }
            ],
            arguments: [{ method: :selected_input_columns_titles }],
            outputs: [
              { original_filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_exatos_filtered.csv',
                filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_exatos_colunas_selecionadas.csv', is_final: true }
            ]
          },
          NORMALIZE_INFOSIGA_SIM_SIH_SCRIPT,
          REMOVE_PAIRED_BY_DOC_FROM_SIM_SCRIPT_ON_INFOSIGA_SIM_SIH,
          REMOVE_PAIRED_BY_DOC_FROM_SIH_SCRIPT_ON_INFOSIGA_SIM_SIH,
          REMOVE_PAIRED_BY_DOC_FROM_INFOSIGA_SCRIPT_ON_INFOSIGA_SIM_SIH,
          PAIR_INFOSIGA_SIM_SCRIPT,
          MERGE_INFOSIGA_SIM_PROB_SCRIPT,
          NORMALIZE_INFOSIGA_SIM_SCRIPT_PROB_SCRIPT,
          PAIR_INFOSIGA_SIM_SIH_SCRIPT,
          MERGE_INFOSIGA_SIM_SIH_SCRIPT,
          {
            name: 'filtragem_de_colunas_infosiga_sim_sih_probabilistica',
            file: 'filter_columns.R',
            inputs: [
              { kind: 'Output', filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_alta_probabilidade.csv' },
              { kind: 'Output', filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_menor_probabilidade.csv' }
            ],
            arguments: [{ method: :selected_input_columns_titles }],
            outputs: [
              {
                original_filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_alta_probabilidade_filtered.csv',
                filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_alta_probabilidade_colunas_selecionadas.csv',
                is_final: true
              },
              {
                original_filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_menor_probabilidade_filtered.csv',
                filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_menor_probabilidade_colunas_selecionadas.csv',
                is_final: true
              }
            ]
          }
        ]
      },
      infosiga_sim_sih_samu: {
        scripts: [
          NORMALIZE_SIM_WITH_DOC_SCRIPT,
          NORMALIZE_INFOSIGA_SCRIPT,
          MERGE_INFOSIGA_SIM_SCRIPT,
          NORMALIZE_INFOSIGA_SIM_SCRIPT,
          NORMALIZE_SIH_WITH_DOC_SCRIPT,
          MERGE_INFOSIGA_SIM_SIH_WITH_DOC_SCRIPT,
          NORMALIZE_INFOSIGA_SIM_SIH_SCRIPT,
          NORMALIZE_SAMU_WITH_DOC_SCRIPT,
          MERGE_INFOSIGA_SIM_SIH_SAMU_WITH_DOC_SCRIPT,
          {
            name: 'filtragem_de_colunas_infosiga_sim_sih_samu',
            file: 'filter_columns.R',
            inputs: [ # Intentionally duplicated files as a workaround to avoid modifying the script
              { kind: 'Output', filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_SAMU_exatos.csv' },
              { kind: 'Output', filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_SAMU_exatos.csv' }
            ],
            arguments: [{ method: :selected_input_columns_titles }],
            outputs: [
              { original_filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_SAMU_exatos_filtered.csv',
                filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_SAMU_exatos_colunas_selecionadas.csv', is_final: true }
            ]
          },
          NORMALIZE_INFOSIGA_SIM_SIH_SAMU_SCRIPT,
          REMOVE_PAIRED_BY_DOC_FROM_SIM_SCRIPT_ON_INFOSIGA_SIM_SIH_SAMU,
          REMOVE_PAIRED_BY_DOC_FROM_SIH_SCRIPT_ON_INFOSIGA_SIM_SIH_SAMU,
          REMOVE_PAIRED_BY_DOC_FROM_INFOSIGA_SCRIPT_ON_INFOSIGA_SIM_SIH_SAMU,
          REMOVE_PAIRED_BY_DOC_FROM_SAMU_SCRIPT_ON_INFOSIGA_SIM_SIH_SAMU,
          PAIR_INFOSIGA_SIM_SCRIPT,
          MERGE_INFOSIGA_SIM_PROB_SCRIPT,
          NORMALIZE_INFOSIGA_SIM_SCRIPT_PROB_SCRIPT,
          PAIR_INFOSIGA_SIM_SIH_SCRIPT,
          MERGE_INFOSIGA_SIM_SIH_SCRIPT,
          NORMALIZE_INFOSIGA_SIM_SIH_SCRIPT_PROB_SCRIPT,
          PAIR_INFOSIGA_SIM_SIH_SAMU_SCRIPT,
          MERGE_INFOSIGA_SIM_SIH_SAMU_SCRIPT,
          {
            name: 'filtragem_de_colunas_infosiga_sim_sih_samu_probabilistica',
            file: 'filter_columns.R',
            inputs: [
              { kind: 'Output', filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_SAMU_alta_probabilidade.csv' },
              { kind: 'Output', filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_SAMU_menor_probabilidade.csv' }
            ],
            arguments: [{ method: :selected_input_columns_titles }],
            outputs: [
              {
                original_filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_SAMU_alta_probabilidade_filtered.csv',
                filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_SAMU_alta_probabilidade_colunas_selecionadas.csv',
                is_final: true
              },
              {
                original_filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_SAMU_menor_probabilidade_filtered.csv',
                filename: 'Registros_de_pares_InfoSIGA_SIM_SIH_SAMU_menor_probabilidade_colunas_selecionadas.csv',
                is_final: true
              }
            ]
          }
        ]
      }
    }.freeze
  end
end
# rubocop:enable Metrics/ModuleLength

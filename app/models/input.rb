# frozen_string_literal: true

class Input < ApplicationRecord
  belongs_to :pairing
  belongs_to :input_type

  has_one_attached :file
  has_many :input_columns, dependent: :destroy
  validate :file_attached

  has_paper_trail

  after_save :sanitize_filename

  private

  def file_attached
    return if file.attached?

    errors.add :file, I18n.t(
      '.missing_file',
      scope: %i[activerecord errors models input]
    )
  end

  def sanitize_filename
    file.blob.update(filename: file.filename.to_s.tr(' ', '_')) if file.attached?
  end
end

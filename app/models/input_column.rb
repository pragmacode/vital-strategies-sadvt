# frozen_string_literal: true

class InputColumn < ApplicationRecord
  belongs_to :input

  validates :title, presence: true
  validates :title, uniqueness: { scope: :input_id }

  PRESELECTED_COLUMNS = ['id_acidente', 'id_veiculo', 'nome', 'sexo', 'data_nascimento2', 'data_obito', 'data',
                         'NO_FALECIDO', 'SG_SEXO', 'DTNASC', 'DTOBITO', 'NOME', 'P_SEXO', 'P_DATN', 'P_NDOC',
                         'DT_INTERNA', 'SEXO', 'VÍTIMA', 'ABERTURA', 'CNES', 'idade', 'fx_etaria', 'P_DOC',
                         'FAIXA_ETARIA', 'escolaridade_vit', 'escolaridade', 'tipo_vitima', 'tipo_veiculo_vit',
                         'tipo_veiculo', 'tipo_acide', 'TipoAcidente', 'ATRAB_CBOR', 'CAR_INTEN', 'COMPLEXIDA',
                         'DIAG_PR', 'DIAG_SE', 'DIARIAS', 'DIARIAS_UI', 'DIARIAS_UT', 'DIAS_PERM', 'LV_INSTRU',
                         'MODALIDADE', 'MOT_SAIDA', 'P_IDADE', 'P_RACA', 'PROC_RE', 'TP_RACA_COR', 'RACACOR',
                         'TP_ESCOLARIDADE', 'ESC', 'CO_OCUPACAO_FALECIDO', 'OCUP', 'NU_LOGRADOURO_RESIDENCIA',
                         'CODENDRES', 'CO_LOGRADOURO_RESIDENCIA', 'CODENDOCOR', 'NO_LOGRADOURO_RESIDENCIA',
                         'ENDRES', 'NU_COMPLEMENTO_RESIDENCIA', 'NUMRES', 'NU_CEP_RESIDENCIA', 'CEPRES', 'DOC',
                         'CO_CID_CAUSA_MORTE', 'ATESTADO', 'CO_CID_CAUSA_BASICA', 'CAUSABAS', 'DS_EVENTO',
                         'DSEVENTO', 'PRIORIDADE', 'SUBTIPO', 'GLAS', 'GLASGOW', 'RESP', 'RESPIRA', 'RECURSO',
                         'IDADE', 'Nome pessoa', 'Sexo da vítima', 'Data de nascimento', 'Data do óbito', 'CPF'].freeze

  before_create :preselect

  private

  def preselect
    self.selected = true if PRESELECTED_COLUMNS.include? title
  end
end

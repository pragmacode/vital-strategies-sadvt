# frozen_string_literal: true

class InputType < ApplicationRecord
  NAMES = %w[
    sat_vehicles sat_victims sat_accidents sim sih samu official_file sim_with_doc sih_with_doc samu_with_doc infosiga
  ].freeze

  validates :csv_columns_names, presence: true
  validates :name, presence: true, inclusion: { in: NAMES }, uniqueness: true

  has_many :inputs, dependent: :restrict_with_exception

  def human_name
    I18n.t "activerecord.attributes.input_type.names.#{name}"
  end
end

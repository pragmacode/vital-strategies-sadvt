# frozen_string_literal: true

class Output < ApplicationRecord
  validates :script_name, presence: true

  belongs_to :pairing

  has_one_attached :file

  has_paper_trail
end

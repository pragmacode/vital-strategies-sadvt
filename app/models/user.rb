# frozen_string_literal: true

class User < ApplicationRecord
  # Devise modules. Others available are:
  # :registerable, :recoverable,:confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :rememberable, :validatable
  USER_KINDS = %w[admin manager visitor].freeze

  validates :level, inclusion: { in: USER_KINDS }

  def admin?
    level_check('admin')
  end

  def manager?
    level_check('manager')
  end

  def write_allowed?
    admin? || manager?
  end

  def self.select_user_level
    USER_KINDS.map { |kind| [I18n.t(kind, scope: %i[activerecord attributes user levels]), kind] }
  end

  def can_select_input_columns?(pairing)
    pairing.input_validated? && write_allowed?
  end

  private

  def level_check(checked_level)
    level == checked_level
  end
end

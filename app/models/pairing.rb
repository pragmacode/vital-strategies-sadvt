# frozen_string_literal: true

class Pairing < ApplicationRecord
  include AASM

  KINDS = %w[sat_sim sat_sih sat_sim_sih sat_sim_sih_samu infosiga_sim infosiga_sih infosiga_sim_sih
             infosiga_sim_sih_samu].freeze
  REQUIRED_INPUTS = {
    'sat_sim' => %w[sat_vehicles sat_victims sat_accidents sim],
    'sat_sih' => %w[sat_vehicles sat_victims sat_accidents sih],
    'sat_sim_sih' => %w[sat_vehicles sat_victims sat_accidents sim sih],
    'sat_sim_sih_samu' => %w[sat_vehicles sat_victims sat_accidents sim sih samu],
    'infosiga_sim' => %w[sim_with_doc infosiga],
    'infosiga_sih' => %w[sih infosiga],
    'infosiga_sim_sih' => %w[sim_with_doc sih infosiga],
    'infosiga_sim_sih_samu' => %w[sim_with_doc sih samu_with_doc infosiga]
  }.freeze

  validates :kind, presence: true, inclusion: { in: KINDS }
  validates :description, length: { maximum: 255 }

  has_many :outputs, dependent: :destroy
  has_many :inputs, dependent: :nullify
  accepts_nested_attributes_for :inputs

  has_paper_trail

  before_destroy :destroy_not_official_inputs

  aasm do
    state :created, initial: true
    state :input_validated, :running, :done, :errored

    event :validate_input do
      transitions from: :created, to: :input_validated
    end

    event :run do
      transitions from: :input_validated, to: :running
    end

    event :finish do
      transitions from: :running, to: :done
    end

    event :fail do
      transitions from: %i[created input_validated running], to: :errored
    end
  end

  def human_kind_name
    I18n.t "activerecord.attributes.pairing.kinds.#{kind}"
  end

  def required_input_types
    REQUIRED_INPUTS[kind].map { |name| InputType.find_by(name: name) }
  end

  def list_required_inputs
    list_inputs.where.not('input_types.name' => 'official_file').references(:input_type)
  end

  def official_base
    list_inputs.where('input_types.name' => 'official_file').references(:input_type)
  end

  def list_inputs
    Input.with_attached_file.where(pairing: self).includes(:input_type)
  end

  def list_outputs
    Output.with_attached_file.where(pairing: self)
  end

  def official_file?
    inputs.where(input_type: InputType.find_by(name: 'official_file')).length.positive?
  end

  def input_process_is_incomplete?
    inputs.count < required_input_types.length
  end

  def input_columns
    InputColumn.joins(:input).where('inputs.pairing_id' => id)
  end

  def group_columns_by_origin
    input_columns.includes(input: :input_type).group_by(&:input)
  end

  def selected_input_columns_titles
    columns = input_columns.where(selected: true).pluck(:title) << 'Score'
    columns.join(';')
  end

  private

  def destroy_not_official_inputs
    inputs.joins(:input_type).where.not(input_type: { name: 'official_file' }).destroy_all
  end
end

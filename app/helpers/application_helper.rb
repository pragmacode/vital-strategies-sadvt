# frozen_string_literal: true

module ApplicationHelper
  def css_class_for_flash_type(type)
    case type
    when 'alert'
      'warning'
    when 'error'
      'danger'
    when 'notice'
      'info'
    else
      type
    end
  end
end

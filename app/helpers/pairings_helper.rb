# frozen_string_literal: true

module PairingsHelper
  def options_for_pairing_kind_select
    options = [[]]

    options += Pairing::KINDS.map do |kind|
      [I18n.t("activerecord.attributes.pairing.kinds.#{kind}"), kind]
    end

    options
  end

  def file_type_to_upload(input_fields)
    type = I18n.t('activerecord.attributes.input_type.names')[input_fields.object.input_type.name.to_sym]
    "#{I18n.t('pairings.new_inputs.upload_message')} #{type}"
  end

  def trigger_button_class(pairing)
    pairing.input_validated? ? 'btn btn-success' : 'btn btn-success disabled'
  end

  def show_final_outputs(pairing)
    pairing.list_outputs.where(is_final: true)
  end

  def show_intermediate_outputs(pairing)
    pairing.list_outputs.where(is_final: false)
  end

  def display_upload_file?(input_fields)
    separate_conditions(
      true,
      [input_fields.object.persisted?, false]
    )
  end

  def upload_button_text(pairing)
    if pairing.official_file?
      '.update_official_file'
    else
      '.add_official_file'
    end
  end

  def show_upload_button?(pairing, user)
    separate_conditions(
      false,
      [pairing.input_process_is_incomplete? && user&.write_allowed?, true]
    )
  end

  def input_an_official_file?(input_fields)
    input_fields.object.input_type.name == 'official_file'
  end

  def separate_conditions(default_value, *scenarios)
    scenarios.each do |condition, return_value|
      return return_value if condition
    end

    default_value
  end
end

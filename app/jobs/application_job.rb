# frozen_string_literal: true

class ApplicationJob < ActiveJob::Base
  # Automatically retry jobs that encountered a deadlock
  # retry_on ActiveRecord::Deadlocked

  # Most jobs are safe to ignore if the underlying records are no longer available
  # discard_on ActiveJob::DeserializationError

  private

  def fetch_csv_headers(file)
    enc = encoding(file)
    csv = CSV.open(file, { quote_char: '"', col_sep: ';', encoding: enc })
    csv.readline.map { |header| header&.encode(enc)&.force_encoding('utf-8') }
  end

  def encoding(file)
    detection = CharlockHolmes::EncodingDetector.detect(file.path)
    detection[:encoding]
  end
end

# frozen_string_literal: true

require 'csv'

class ExtractInputColumnsJob < ApplicationJob
  queue_as :default

  def perform(input)
    @input = input
    input.file.open { |file| @csv_headers = fetch_csv_headers(file) }
    save_input_columns_to_db
  end

  def save_input_columns_to_db
    @csv_headers.each do |header|
      InputColumn.find_or_create_by(title: header, input_id: @input.id)
    end
  end
end

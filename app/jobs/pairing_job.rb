# frozen_string_literal: true

class PairingJob < ApplicationJob
  queue_as :default

  rescue_from(RuntimeError) do
    @pairing.fail!
  end

  def perform(pairing_id, current_script_index = 0)
    @pairing = Pairing.find(pairing_id)
    Pairings::Runner.calculate_pairing(@pairing, current_script_index)
  end
end

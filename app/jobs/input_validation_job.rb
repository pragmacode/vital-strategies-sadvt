# frozen_string_literal: true

require 'csv'

class InputValidationJob < ApplicationJob
  queue_as :default

  rescue_from(RuntimeError, NoMethodError) do
    @pairing.fail!
  end

  def perform(pairing_id)
    @pairing = Pairing.find(pairing_id)
    if check_all_inputs @pairing
      @pairing.validate_input!
    else
      @pairing.fail!
    end
  end

  def a_valid_input?(input)
    input_type = input.input_type
    input.file.open do |file|
      check_result = check_input(file, input_type)
      ExtractInputColumnsJob.perform_later(input) if check_result
      check_result
    end
  end

  def check_input(file, input_type)
    csv_headers = fetch_csv_headers(file)
    check_necessary_headers(input_type.csv_columns_names, csv_headers)
  end

  def check_necessary_headers(input_type_headers, csv_headers)
    necessary_headers = input_type_headers.split(';')
    necessary_headers.intersection(csv_headers) == necessary_headers
  end

  def check_all_inputs(pairing)
    pairing.inputs.all? do |input|
      a_valid_input? input
    end
  end
end

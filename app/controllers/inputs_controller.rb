# frozen_string_literal: true

class InputsController < ApplicationController
  def official_files
    @inputs = Input.includes(:input_type, file_attachment: [:blob])
                   .where(input_type: { name: 'official_file' })
                   .order(updated_at: :desc)
  end
end

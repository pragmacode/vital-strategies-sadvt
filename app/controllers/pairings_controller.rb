# frozen_string_literal: true

# rubocop:disable Metrics/ClassLength
class PairingsController < ApplicationController
  before_action :set_pairing_by_id, only: %i[show update destroy]
  before_action :set_pairing_by_pairing_id, except: %i[index new create show update destroy]
  before_action :check_write_access!, except: %i[index show select_input_columns]
  before_action :can_select_inputs_columns, only: [:select_input_columns]

  def index
    @pairings = Pairing.all.order(created_at: :desc)
  end

  def new
    @pairing = Pairing.new
  end

  def create
    @pairing = Pairing.new pairing_params

    if @pairing.save
      flash[:notice] = I18n.t('pairings.create.success')
      redirect_to pairing_inputs_path(@pairing)
    else
      flash.now[:error] = @pairing.errors.full_messages
      render :new
    end
  end

  def show; end

  def new_inputs
    @pairing.required_input_types.each do |type|
      build_input_with_type(type)
    end
  end

  def official_file_input
    input_type = InputType.find_by(name: 'official_file')
    official_file = Input.where(input_type: input_type, pairing: @pairing)
    @official_file_input = fetch_official_file_input(official_file, input_type)
  end

  def select_input_columns
    @columns = @pairing.group_columns_by_origin
  end

  def update
    build_inputs(@pairing, retrieve_inputs_attributes)
  end

  def save_official_file
    upsert_official_base(@pairing, retrieve_inputs_attributes)
  end

  def pick_pairing_columns
    selected_columns = params[:pairing] ? params[:pairing][:selected_columns] : nil
    save_pairing_columns(selected_columns)
  end

  def run
    if @pairing.input_validated?
      PairingJob.perform_later(@pairing.id)
      flash[:notice] = I18n.t('pairings.run.triggered_job')
      redirect_to root_path
    else
      flash[:error] = I18n.t('pairings.run.cannot_trigger_job')
      redirect_to pairing_path(@pairing)
    end
  end

  def destroy
    @pairing.destroy
    redirect_to root_path, notice: I18n.t('pairings.destroy.success')
  end

  private

  def pairing_params
    params.require(:pairing).permit(:kind, :description, inputs_attributes: %i[file input_type_id])
  end

  def set_pairing_by_pairing_id
    @pairing = Pairing.find(params[:pairing_id])
  end

  def set_pairing_by_id
    @pairing = Pairing.find(params[:id])
  end

  def build_input(pairing, input_type_id, file_hash)
    pairing.inputs.build(file: file_hash, input_type_id: input_type_id) if file_hash
  end

  def success_build(pairing)
    pairing.inputs.each(&:save)

    InputValidationJob.perform_later pairing.id if pairing.inputs.length == pairing.required_input_types.length

    flash[:notice] = I18n.t('pairings.update.success')
    render :show
  end

  def failed_build(pairing)
    flash[:error] = pairing.inputs.reject(&:valid?).map { |input| input.errors.messages }
    redirect_to pairing_inputs_path(pairing)
  end

  def build_inputs(pairing, inputs_params)
    inputs_params.each_value do |input_params|
      build_input(pairing, input_params['input_type_id'], input_params['file'])
    end

    if pairing.inputs.includes(:input_type, file_attachment: :blob).any? { |input| !input.valid? }
      failed_build(pairing)
    else
      success_build(pairing)
    end
  end

  def upsert_official_base(pairing, inputs_params)
    official_base = pairing.official_base.first
    if official_base
      official_base.update(file: inputs_params['0']['file'])
      redirect_to pairing_path(@pairing), notice: I18n.t('pairings.official_file_update')
    else
      build_inputs(pairing, inputs_params)
    end
  end

  def check_write_access!
    allow_access_for_a_condition(current_user.write_allowed?, 'only_managers_allowed')
  end

  def can_select_inputs_columns
    allow_access_for_a_condition(current_user&.can_select_input_columns?(@pairing), 'cannot_select_columns')
  end

  def build_input_with_type(type)
    @pairing.inputs.build(input_type: type) unless @pairing.inputs.exists?(input_type: type)
  end

  def redirect_to_root_path
    redirect_to root_path
  end

  def retrieve_inputs_attributes
    pairing_params[:inputs_attributes] || {}
  end

  def fetch_official_file_input(official_file_array, input_type)
    if official_file_array.empty?
      build_input_with_type(input_type)
    else
      official_file_array.first
    end
  end

  def allow_access_for_a_condition(condition, error_message_key)
    return if condition

    flash[:error] = I18n.t(error_message_key, scope: [:pairings])
    redirect_to root_path
  end

  def save_pairing_columns(selected_columns_id)
    update_input_columns(selected_columns_id) if selected_columns_id

    flash[:notice] = I18n.t('pairings.save_pairing_columns.success')
    redirect_to pairing_path(@pairing)
  end

  def update_input_columns(selected_columns_id)
    pairing_columns = @pairing.input_columns
    pairing_columns.includes([:input]).where(id: selected_columns_id).update(selected: true)
    pairing_columns.where.not(id: selected_columns_id).update(selected: false)
  end
end
# rubocop:enable Metrics/ClassLength

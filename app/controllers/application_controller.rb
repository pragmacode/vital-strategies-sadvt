# frozen_string_literal: true

class ApplicationController < ActionController::Base
  protect_from_forgery prepend: true
  before_action :authenticate_user!

  def after_sign_out_path_for(_resource)
    new_user_session_path
  end
end

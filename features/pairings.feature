Feature: Pairings

  In order to manage pairing
  As a regular user
  I should be able to list them

  Scenario: Listing
    Given there is a logged in user
    And there is a pairing
    And I am at "/"
    Then I should see the pairing kind

  Scenario: Creating a pairing as an administrator
    Given there is a logged in user
    And the user is an administrator
    And the default InputTypes exist
    And I am at "/pairings/new"
    When I select the "SAT/SIH" option for "Tipo"
    And I click the "Criar" button
    Then I should see "Pareamento criado com sucesso"

  Scenario: Trying to create a pairing as a visitor
    Given there is a logged in user
    And the default InputTypes exist
    When I am at "/pairings/new"
    Then I should not see "Novo Pareamento"

  Scenario: Showing
    Given there is a logged in user
    And the default InputTypes exist
    And there is a pairing
    And the pairing has inputs
    And the pairing has outputs
    And I am at the pairing page
    Then I should see the pairing kind
    And I should see the download link for the input
    And I should see the download link for the output

  @javascript
  Scenario: Trying to select columns for a pairing as a manager
    Given there is a logged in user
    And the user is a manager
    And the default InputTypes exist
    And there is a pairing
    And the pairing has inputs
    And the input file columns had been extracted
    And the pairing state is "input_validated"
    And I am at the pairing page
    When I click the "Ver Seleção" link
    And I click the "Definir as colunas escolhidas" link
    Then I should see "Seleção de colunas do pareamento"
    When I click the "Salvar colunas escolhidas" button
    Then I should be at the pairing page
    And I should see "As colunas foram selecionadas com sucesso"

  @javascript
  Scenario: Trying to select columns for a pairing as a manager after the job has been fired
    Given there is a logged in user
    And the user is a manager
    And the default InputTypes exist
    And there is a pairing
    And the pairing state is "done"
    When I am at the pairing page
    Then I should not see "Definir as colunas escolhidas"

  @javascript
  Scenario: Regular user tries to select columns for a pairing
    Given there is a logged in user
    And the default InputTypes exist
    And there is a pairing
    When I am at the pairing page
    Then I should not see "Definir as colunas escolhidas"

  @javascript
  Scenario: Regular user tries to select columns for a pairing via URL
    Given there is a logged in user
    And the default InputTypes exist
    And there is a pairing
    When I am at the pairing columns selection page
    Then I should not see "Seleção de colunas do pareamento"
    And I should be at the root page
    And I should see "A seleção de colunas não está disponível"

  @javascript
  Scenario: Trying to destroy a pairing as a manager
    Given there is a logged in user
    And the user is a manager
    And the default InputTypes exist
    And there is a pairing
    And the pairing has inputs
    And the pairing has outputs
    And I am at "/"
    When I click the "Remover" link and confirm
    Then I should see "Pareamento removido com sucesso"

  @javascript
  Scenario: Trying to destroy a pairing as a visitor
    Given there is a logged in user
    And the default InputTypes exist
    And there is a pairing
    And the pairing has inputs
    And the pairing has outputs
    And I am at "/"
    When I should not see "Remover"

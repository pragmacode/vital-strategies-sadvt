Feature: Firing Jobs

  As a manager, user
  I should be able to trigger jobs

  Scenario: User can start the jobs
    Given there is a logged in user
    And the user is a manager
    And there is a pairing
    And the default InputTypes exist
    When I am at the pairing inputs page
    And I should see "Pareamento - Inclusão de bases"
    Then I attach the data bases to the pairings
    And I click the "Salvar bases de dados" button
    When I am at the pairing page
    And R scripts are stubbed
    And I click the "Iniciar Script" link
    Then I should be at the root page
    And the pairing should have the script standard output files

  Scenario: User cannot start the jobs
    Given there is a logged in user
    And there is a pairing
    And the pairing has inputs
    When I am at the pairing page
    Then I should not see "Iniciar Script"

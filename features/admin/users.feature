Feature: Users Administration

  In order to manage users
  As an administrator
  I should be able to interact with the interface interface

  @javascript
  Scenario: Create, list and delete
    Given there is an user with email "user@user.com" and password "user1234"
    And the user is an administrator
    And I am at "/"
    When I click the "Entrar" link
    And I fill the "E-mail" input with "user@user.com"
    And I fill the "Senha" input with "user1234"
    And I click the "Login" button
    And I click the "Administração" link
    And I wait for a while
    Then I should see "Listando Usuário"
    And I should see "user@user.com"
    When I am at "/admin/auth/users/new"
    And I fill the "E-mail" input with "user1@user.com"
    And I fill the "Senha" input with "user5678"
    And I fill the "Confirme sua senha" input with "user5678"
    And I click the "Salvar Usuário" button
    Then I should see "Usuário foi criado com sucesso."
    When I am at "/admin/auth/users"
    Then I should see "user1@user.com"
    When I click the "user1@user.com" link
    And I click the "Excluir Usuário" link
    And I click the "Excluir" link
    Then I should see "O Usuário foi deletado com sucesso."
    Then I should not see "user1@user.com"

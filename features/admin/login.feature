Feature: Admin Authentication

  In order to have access to user management
  As a administrator
  I should be able to access the interface

  Scenario: With an administrative user
    Given there is an user with email "user@user.com" and password "user1234"
    And the user is an administrator
    And I am at "/"
    When I click the "Entrar" link
    And I fill the "E-mail" input with "user@user.com"
    And I fill the "Senha" input with "user1234"
    And I click the "Login" button
    And I click the "Administração" link
    And I wait for a while
    Then I should see "Listando Usuário"

  Scenario: With an unprivileged user
    Given there is an user with email "user@user.com" and password "user1234"
    And I am at "/"
    When I click the "Entrar" link
    And I fill the "E-mail" input with "user@user.com"
    And I fill the "Senha" input with "user1234"
    And I click the "Login" button
    Then I should not see "Administração"
    When I am at "/admin"
    Then I should see "Apenas administradores tem acesso à página que tentou acessar"

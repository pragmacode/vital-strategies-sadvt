Feature: Authentication

  In order to have access to sensitive data
  As a user
  I should be able to login

  Scenario: Login and logout
    Given there is an user with email "user@user.com" and password "user1234"
    When I am at "/"
    Then I should see "Para continuar, faça login ou registre-se."
    When I click the "Entrar" link
    And I fill the "E-mail" input with "user@user.com"
    And I fill the "Senha" input with "user1234"
    And I click the "Login" button
    Then I should see "Login efetuado com sucesso."
    And I should be at the root page
    When I click the "Sair" link
    Then I should see "Logout efetuado com sucesso."
    And I should be at the sign in page

Feature: Inputs

  In order to make data analysis possible
  As an administrator or manager
  I should be able to upload the data bases

  Scenario: Uploading the necessary quantity of files
    Given there is a logged in user
    And the user is an administrator
    And there is a pairing
    And the default InputTypes exist
    When I am at the pairing page
    And I should see "Pareamento"
    Then I click the "Enviar Bases" link
    And I should see "Pareamento - Inclusão de bases"
    Then I attach the data bases to the pairings
    And I click the "Salvar bases de dados" button
    And I should see "Dados carregados com sucesso"
    And the pairing inputs have related columns

  Scenario: Failing to reach the upload page
    Given there is a logged in user
    And there is a pairing
    And the default InputTypes exist
    When I am at the pairing page
    And I should see "Pareamento"
    Then I should not see "Enviar Bases"

  Scenario: Visitor bypasses visual blocks to try to upload a data base
    Given there is a logged in user
    And the user is a visitor
    And there is a pairing
    When I am at the pairing inputs page
    Then I should not see "Pareamento - Inclusão de bases"
    And I should see "Pareamentos"

  Scenario: Uploading an official data base
    Given there is a logged in user
    And the user is an administrator
    And there is a pairing
    And the default InputTypes exist
    When I am at the official files page
    Then I should see no link to download the official files
    When I am at the pairing page
    And I should see "Pareamento"
    Then I click the "Enviar Bases" link
    And I should see "Pareamento - Inclusão de bases"
    Then I attach the data bases to the pairings
    And I click the "Salvar bases de dados" button
    And I should see "Dados carregados com sucesso"
    When the pairing state is "done"
    And I am at the pairing page
    And I click the "Adicionar Base Final Oficial" link
    Then I should see "Pareamento - Inclusão de base final oficial"
    And I attach the official data base to the pairings
    And I click the "Salvar base final oficial" button
    When I am at the official files page
    Then I should see the link to download the official file
    When I am at the pairing page
    And I click the "Atualizar Base Final Oficial" link
    And I attach the official data base to the pairings
    And I click the "Salvar base final oficial" button
    Then I should see "A base final foi atualizada"

  Scenario: Regular user tries to visual blocks to try to upload an official data base
    Given there is a logged in user
    And the user is a visitor
    And there is a pairing
    And the pairing state is "done"
    And the default InputTypes exist
    When I am at the pairing official file inputs page
    Then I should not see "Pareamento - Inclusão de base final oficial"
    And I should see "Pareamentos"

  Scenario: Failing to reach the upload page
    Given there is a logged in user
    And the user is a visitor
    And there is a pairing
    And the pairing state is "done"
    And the default InputTypes exist
    When I am at the pairing page
    And I should see "Pareamento"
    Then I should not see "Adicionar Base Oficial"

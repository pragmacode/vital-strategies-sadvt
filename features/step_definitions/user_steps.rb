# frozen_string_literal: true

Given('there is an user with email {string} and password {string}') do |email, password|
  @user = User.create! email: email, password: password
end

Given('there is a logged in user') do
  steps %(
    Given there is an user with email "user@user.com" and password "user1234"
    And I am at "/"
    When I click the "Entrar" link
    And I fill the "E-mail" input with "user@user.com"
    And I fill the "Senha" input with "user1234"
    And I click the "Login" button
  )
end

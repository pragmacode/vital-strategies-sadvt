# frozen_string_literal: true

Given('there is a pairing') do
  @pairing = FactoryBot.create :pairing
end

Given('the pairing state is {string}') do |pairing_status|
  @pairing.update!(aasm_state: pairing_status)
end

Given('I am at the pairing page') do
  step "I am at \"#{pairing_path(@pairing)}\""
end

Given('I am at the pairing inputs page') do
  step "I am at \"#{pairing_inputs_path(@pairing)}\""
end

Given('I am at the pairing official file inputs page') do
  step "I am at \"#{pairing_official_base_path(@pairing)}\""
end

Given('I am at the pairing columns selection page') do
  step "I am at \"#{pairing_select_input_columns_path(@pairing)}\""
end

Then('I should see the pairing kind') do
  step "I should see \"#{@pairing.human_kind_name}\""
end

Then('the pairing should have the script standard output files') do
  pipeline = Pairings::Pipeline::AVAILABLE_SCRIPTS[@pairing.kind.to_sym][:scripts]
  pipeline.each do |script|
    expect(
      @pairing.outputs.find { |imp| imp.file.filename == "Programacao_de_#{script[:name]}.txt" }
    ).not_to be_nil
  end
end

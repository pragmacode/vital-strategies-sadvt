# frozen_string_literal: true

Given('I am at {string}') do |url|
  visit url
end

When('I wait for a while') do
  sleep(3)
end

When('I click the {string} link') do |link|
  click_link link
end

When('I fill the {string} input with {string}') do |input, value|
  fill_in input, with: value
end

When('I click the {string} button') do |button|
  click_button button
end

When('I select the {string} option for {string}') do |value, field|
  select value, from: field
end

When('I click the {string} link and confirm') do |link|
  accept_confirm do
    step "I click the \"#{link}\" link"
  end
end

Then('I should see {string}') do |content|
  expect(page).to have_content(content)
end

Then('I should not see {string}') do |content|
  expect(page).not_to have_content(content)
end

Then('I should be at the root page') do
  expect(page).to have_current_path root_path, ignore_query: true
end

Then('I should be at the sign in page') do
  expect(page).to have_current_path new_user_session_path, ignore_query: true
end

Then('I should be at the pairing page') do
  expect(page).to have_current_path pairing_path(@pairing), ignore_query: true
end

Then('the {string} button should be disabled') do |text|
  expect(page.find('.btn.disabled').text).to eq text
end

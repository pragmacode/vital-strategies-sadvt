# frozen_string_literal: true

Given('the default InputTypes exist') do
  InputType::NAMES.each do |name|
    InputType.find_or_create_by!(name: name, csv_columns_names: ';')
  end
end

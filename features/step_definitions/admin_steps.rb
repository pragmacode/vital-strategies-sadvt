# frozen_string_literal: true

Given('the user is an administrator') do
  @user.update!(level: 'admin')
end

Given('the user is a visitor') do
  @user.update!(level: 'visitor')
end

Given('the user is a manager') do
  @user.update!(level: 'manager')
end

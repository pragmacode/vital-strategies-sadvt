# frozen_string_literal: true

Given('the pairing has inputs') do
  input_type = InputType.find_or_create_by(name: InputType::NAMES.first, csv_columns_names: ';')

  @input = FactoryBot.build(
    :input,
    pairing: @pairing,
    input_type: input_type
  )

  @input.file.attach(
    io: File.open(Rails.root.join('features/support/fixtures/sample_input.csv')),
    filename: 'sample_input.csv'
  )

  @input.save!
end

Given('the input file columns had been extracted') do
  FactoryBot.create_list(:input_column, 3, input: @input)
end

Given('the pairing has outputs') do
  @output = FactoryBot.build(:output, pairing: @pairing)

  @output.file.attach(
    io: File.open(Rails.root.join('features/support/fixtures/sample_input.csv')),
    filename: @output.script_name
  )

  @output.save!
end

Then('I should see the download link for the input') do
  expect(page).to have_link(
    "#{I18n.t('inputs.input.download')} #{@input.input_type.human_name}"
  )
end

Then('I should see the download link for the output') do
  expect(page).to have_link(
    "#{I18n.t('outputs.output.download')} #{@output.script_name}"
  )
end

When('I attach the data bases to the pairings') do
  @pairing.required_input_types.length.times do |base_index|
    page.attach_file("pairing[inputs_attributes][#{base_index}][file]",
                     Rails.root.join("lib/pairing/samples/#{@pairing.required_input_types[base_index].name}.csv"),
                     visible: false)
  end
end

When('the pairing inputs have related columns') do
  @pairing.inputs.each do |input|
    expect(input.input_columns.length).to be_positive
  end
end

When('I attach the official data base to the pairings') do
  page.attach_file('pairing[inputs_attributes][0][file]',
                   Rails.root.join('lib/pairing/samples/official_file.csv'), visible: false)
end

When('I am at the official files page') do
  visit inputs_official_files_path
end

Then('I should see no link to download the official files') do
  expect(page).not_to have_link(I18n.t('inputs.input.download'))
end

Then('I should see the link to download the official file') do
  expect(page).to have_link(I18n.t('inputs.input.download'))
end

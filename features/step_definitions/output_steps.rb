# frozen_string_literal: true

When('R scripts are stubbed') do
  pipeline = Pairings::Pipeline::AVAILABLE_SCRIPTS[@pairing.kind.to_sym][:scripts]
  needed_outputs = pipeline.map do |script|
    script[:inputs].filter { |input| input[:kind] == 'Output' }
  end
  needed_outputs.flatten.each do |output|
    pairing_output = FactoryBot.build(:output, pairing: @pairing)
    pairing_output.file.attach(
      io: File.open(Rails.root.join('features/support/fixtures/sample_input.csv')),
      filename: output[:filename]
    )
    pairing_output.save!
  end
end

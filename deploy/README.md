# SPVT deployment instructions

Requirements:

* Ubuntu 20.04 host (or compatible)
  - Python 3
* [ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)

Configuration:

* Create inventory file
  - https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html
  - You can base yourself on the [staging](deploy/staging)
  - Set the `nginx_server_name` accordingly to your server DNS configuration and/or IP

Running:

* `ansible-playbook -i staging setup.yml -u root`
  - This playbook installs system dependencies, you probably won't want to run it more than once
  - Instead of `-u root` you can also use `-K` if you have an user with sudo privilege
* Build and push the docker container (the CI should be doing this)
  - `docker build -t registry.gitlab.com/pragmacode/vital-strategies-sadvt .`
  - `docker push registry.gitlab.com/pragmacode/vital-strategies-sadvt`
* `ansible-playbook -i staging deploy.yml`
  - This will take care of bringing up the application, invoke it after every release (the CI should be doing this)
  - **Before running**
    * login to the docker registry within the host with the deployer user (`docker login registry.gitlab.com`)

# Hacking SPVT

## Ruby version

The version is defined by the `.ruby-version` file.

## System dependencies

* Ruby
  - Our preferred way of installation is using [RVM](https://rvm.io/)

* [Yarn](https://yarnpkg.com/lang/en/)
  - You can install it using npm by running `npm install -g yarn`

* [Redis](https://redis.io/)

* [PostgreSQL](https://www.postgresql.org/)

* [R](https://www.postgresql.org/)

  Try:
  - [How to install R on Ubuntu](https://linuxize.com/post/how-to-install-r-on-ubuntu-20-04/)

  After finish the installation, you need to enter the pairing folder to install and/or update the R dependencies. Try:
  - `cd lib/pairing/` to acess the folder
  - `R` to run the R
  - Then use `renv::restore()` to install packages recorded in the lockfile.



## Configuration

You need to create a role on postgresql with your local username:

```
CREATE ROLE <your_username> WITH CREATEDB LOGIN PASSWORD '<your_username>';
```

This can be done via the postgresql console or some GUI such as pgAdmin. Just change `<your_username>` with your local
username.

Then, just run `./bin/setup` to get an development environment working.

## Run the Project

Run `rails s`

### How to use the application

- Follow the application demonstration on [docs folder](https://gitlab.com/pragmacode/vital-strategies-sadvt/-/tree/master/doc).

## How to run the test suite

* Linter:
  - dependencies security risks
    * `gem install bundle-audit && bundle audit --update`
    * `yarn audit`
  - rails security: `gem install brakeman && brakeman`
  - dependencies licenses: `license_finder`
  - for code style run `rubocop`
  - for code duplications run `bundle exec flay` or simply `flay`
  - for TypeScript run `yarn lint`
* All tests: `rake`
* Unit tests: `rails spec`
* Acceptance tests: `rails cucumber`


## Generate DB dump through docker

```shell
# that's how you create the dump file
# the given path is exactly the same as in the file deploy/deploy.yml where the volume for postgres is being set
docker exec -it sadvt-database-postgresql13 pg_dump -U sadvt -d sadvt_production -f /var/lib/postgresql/data/dump.sql
# after it is written the dump file can be retrieved from the mapped postgres folder
cp ~/postgresql-13/data/dump.sql <some_destination_file>
```

## Saving ActiveStorage files

In order to preserve the ActiveStorage files, we should copy content
from `${HOME}/storage` folder
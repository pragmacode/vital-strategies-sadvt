FROM ruby:2.7.5

RUN gem install bundler --no-document
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update -y && apt-get install -y yarn
RUN apt-get update -y && apt-get install -y --install-recommends dirmngr software-properties-common apt-transport-https
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-key '95C0FAF38DB3CCAD0C080A7BDC78B2DDEABC47B7'
RUN add-apt-repository 'deb http://cloud.r-project.org/bin/linux/debian bullseye-cran40/'
RUN apt-get update -y && apt-get install -y r-base
RUN Rscript -e 'install.packages("remotes")'
RUN Rscript -e 'remotes::install_github("rstudio/renv")'

WORKDIR /sadvt

ADD Gemfile /sadvt/Gemfile
ADD Gemfile.lock /sadvt/Gemfile.lock
RUN bundle install

ADD package.json /sadvt/package.json
ADD yarn.lock /sadvt/yarn.lock
RUN yarn

ADD lib/pairing/renv /sadvt/lib/pairing/renv
ADD lib/pairing/renv.lock /sadvt/lib/pairing/renv.lock
RUN cd lib/pairing && Rscript -e 'renv::restore()' && cd ../..

ADD . /sadvt/
RUN yarn
RUN cd lib/pairing && Rscript -e 'renv::restore()' && cd ../..

RUN apt-get update -y && apt-get install -y vim

EXPOSE 3002
ENTRYPOINT ["./bin/entrypoint"]

# Revision history for SPVT

The version numbers below try to follow the conventions at http://semver.org/.

Please add new changes under the **Unreleased** section as list items from the newest on top to the oldest.

## Unreleased

## 5.2.0 - 31/08/2022

* Split name components for pairing
* Fix pairs merging
* Split date components for pairing
* Clean irrelevant terms from pairing names
* Upgrade to Rails 6.1.6.1

## v5.1.0 - 20/06/2022

* Fix migrations to work on rollback
* Sanitize pairing filename after upload
* Update pre selected columns with missing informations
* Update preselected columns with infosiga columns
* Update infosiga required columns
* Updates the R scripts that handles infosiga 
* Update the infosiga sample file
* Update infosiga normalization script
* Update to rack 2.2.3.1
* Delete SIH with DOC from input_types table in DB
* Update project documentation
* Update lib/HACKING.md
* Update nokogiri
* Remove SIH with DOC
* Adjust SIH normalization to use P_NDOC

## v5.0.0 - 20/05/2022

* Pair and merge InfoSIGA/SIM/SIH/SAMU
* Remove paired rows from SAMU in InfoSIGA/SIM/SIH/SAMU
* Remove paired rows from SIH in InfoSIGA/SIM/SIH/SAMU
* Remove paired rows from SIM in InfoSIGA/SIM/SIH/SAMU
* Remove paired rows from InfoSIGA in InfoSIGA/SIM/SIH/SAMU
* Fix result rows with NA values in the columns for one of the datasets
* Add column filter to InfoSIGA/SIM/SIH/SAMU pipeline
* Merge InfoSIGA/SIM/SIH and SAMU
* Normalize SAMU with DOC for InfoSIGA/SIM/SIH/SAMU
* Normalize InfoSIGA/SIM/SIH for InfoSIGA/SIM/SIH/SAMU
* Merge InfoSIGA/SIM and SIH for InfoSIGA/SIM/SIH/SAMU
* Normalize SIH with DOC for InfoSIGA/SIM/SIH/SAMU
* Normalize InfoSIGA/SIM on InfoSIGA/SIM/SIH/SAMU
* Merge InfoSIGA and SIM on InfoSIGA/SIM/SIH/SAMU
* Normalize InfoSIGA w/ DOC for InfoSIGA/SIM/SIH/SAMU
* Normalize SIM with DOC for InfoSIGA/SIM/SIH/SAMU
* Select columns for InfoSIGA/SIM/SIH dataset
* Generate InfoSIGA/SIM/SIH dataset
* Pair InfoSIGA/SIM (high prob) and and SIH
* Normalize InfoSIGA/SIM with high probability
* Remove paired rows from InfoSIGA in InfoSIGA/SIM/SIH
* Remove paired rows from SIM in InfoSIGA/SIM/SIH
* Create pairing InfoSIGA/SIM/SIH
* Remove paired rows from InfoSIGA in InfoSIGA/SIM pipeline
* Remove paired rows from SIH in InfoSIGA/SIH pipeline
* Normalize InfoSIGA/SIH deterministic pairing results
* Add column filter to InfoSIGA/SIH pipeline
* Create deterministic merge for InfoSIGA/SIH pipeline
* Apply probabilistic pairing to InfoSIGA/SIM
* Update pre-selected fields
* Remove paired rows from InfoSIGA in InfoSIGA/SIM pipeline
* Remove paired rows from SIM in InfoSIGA/SIM pipeline
* Create script to remove rows paired by doc
* Normalize InfoSIGA/SIM deterministic pairing results
* Create sih_with_doc InputType
* Update sim_with_doc columns name
* Update SIM columns name
* Insert abbreviation description screen
* Add column filter to InfoSIGA/SIM pipeline
* Add deterministic merge to InfoSIGA/SIM pipeline
* Create script to merge by document column
* Add InfoSIGA normalization to the pipeline
* Create InfoSIGA normalization script
* Normalize DOC column in SIM
* Create infosiga InputType
* Create sim_with_doc InputType
* Adjust footer informations
* Add logos in footer
* Upgrade to Rails 6.1.5.1
* Add description to Pairing
* Rename normalized SAT filenames
* Rename normalized SIM filenames
* Rename normalized SIH filenames
* Rename normalized SAMU filenames
* Rename programing files output (.txt)
* Upgrade to puma 5.6.4
* Upgrade to nokogiri 1.13.4

## v4.0.0 - 21/03/2021

* Select pre-determined columns
* Fix empty column selection
* Change column selection default to false

## v3.0.1 - 18/03/2022

* Fix R packages installation

## v3.0.0 - 17/03/2022

* List official_file Inputs
* Do not destroy official_file Inputs

## v2.1.0 - 09/03/2022

* Update LaTeX documentation
* Fix missing Score column
* Fix R dependencies installation
* Upgrade to Rails 6.1.4.7
* Upgrade sidekiq
* Upgrade puma
* Modify footer to show app version
* Create InputColumn model
* Create new input type for official files
* Add score to final results
* Change application on simple files
* Update gems after ruby version had changed
* Update ruby to version 2.7.5

## v2.0.1 - 02/12/2021

* Upgrade sidekiq
* Upgrade puma
* Upgrade nokogiri
* Fix R installation in Docker
* Upgrade npm @rails/webpacker

## v2.0.0 - 16/04/2021

* Upgrade trestle-auth
* Document ouput format
* Add more details to documentation about input CSV format
* Fix documentation typo
* Set time zone to America/Sao_Paulo
* Make possible to remove Pairings
* Upgrade Ruby
* Upgrade mimemagic
* Fallback to simple input reading in R if encoding guessed read fails
* Improve date and sex normalization
* Normalize input files to UTF-8 using R
* Install stringi R package
* Detect Inputs character encoding
* Install charlock_holmes
* Allow partial upload of Inputs for Pairing
* Always generate new credentails file
* Create installation script
* Upgrade Rails version

## v1.0.0 - 08/02/2021

* Remove unused R scripts
* Validate the given input files
* Fix filenames not showing when selected for upload
* Create documentation on the expected input format
* Parametize NGINX server_name on deployment
* Fix timeout while sending big datasets
* Remove SAT/SAMU pairing

## v0.4.0 - 30/01/2021

* Create SAT/SIM/SIH/SAMU web application pipeline
* Create new SAMU normalization script
* Shorten saved result file names
* Create SAT/SIM/SIH web application pipeline
* Create normalization script for already paired datasets
* Set SAT/SIM web application pipeline to new scripts
* Create new SIM normalization script
* Set SAT/SIH web application pipeline to new scripts
* Create script to merge datasets based on pairing results
* Create new pairing script
* Create new SIH normalization script
* Create new SAT normalization script
* Create back up strategy
* Create Rscript to split final results accordingly to pairing score
* Differentiate final from intermediate outputs
* Create documentation session for running the scripts on the Docker
* Include in documentation the dependencies used in server
* Fix fresh install
* Create video for showing app working and setup
* Create empty text documentation

## v0.3.0 - 16/01/2021

* Fix the fake pipelines based on the `lib/pairings/HACKING.md`
* Fix R dependencies installation in Docker
* Detail how to input and output data for R scripts in Docker
* Create button to trigger jobs processing
* Show the links to download the output files
* Schedule InputValidationJob after upload
* Create InputValidationJob
* Improve Pairing::Runner class by splitting and reorganizing methods
* Error handler has been implemented in PairingJob
* Create SAT/SIH fake pairing script for application usage
* Create SAT/SAMU fake pairing script for application usage
* Create datasets preparation fake script specific for SAMU
* Parametize IO for SIH preparation script
* Create datasets preparation script specific for SIH
* Parametize IO paths in SAT/SIM pairing script
* Create SAT/SIM pairing script for application usage
* Parametize output dir for SAT and SIM datasets preparation scripts
* Parametize input for SIM preparation script
* Create datasets preparation script specific for SIM
* Parametize input for SAT preparation script
* Create datasets preparation script specific for SAT
* Fix nokogiri security issue
* Allow users to upload the pairing inputs
* Fix missing ActiveStorage configuration
* Allow Input download from Pairing show
* Create Input model
* Create InputType model

## v0.2.1 - 18/12/2020

* Fix broken production assets

## v0.2.0 - 17/12/2020

* Upgrade Rails to version 6.1.0
* Create Pairing show page
* Persist Pairing form submissions to database
* Create new Pairing form
* Create page for listing all pairings
* Install R dependencies in Docker image
* Save scripts STDOUT and STDERR to file
* Associate Outputs with Pairings
* Create Output table
* Install AASM
* Create Pairing model
* Create users administration interface
* Install ActiveStorage
* Install trestle
* Replace SQLite with PostgreSQL

## v0.1.0 - 02/12/2020

* Authenticate users on controller actions
* User logout feature
* User login feature
* Integrate flash messages to the application template
* Install Devise
* Use redis as production cache store
* Measure test coverage
* Draft Parings index
* Include sidekiq on deploy
* Create Pairings runner
* Create Pairing Job
* Install sidekiq
* Install rails-i18n
* Set default language to pt-BR
* Set copyright notice in footer 
* Set application title
* Add favicon
* Install paper_trail
* Extract footer into a partial and add it to the layout
* Extract navbar into a partial and add it to the layout
* Integrate Modern Business Bootstrap template
* Create empty root path
* Install bootstrap
* Fix R scripts dependencies versions
* Create humans.txt
* Add license checker
* Add static code analyzer
* Add acceptance test framework
* Add test framework
* Setup initial configuration files
* Start rails projects
* Add R scripts

# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ApplicationJob, type: :job do
  subject(:job) { described_class.new }

  describe 'fetch_csv_headers' do
    let(:encoding) { 'ISO-8859-1' }
    let(:list_of_headers) { %w[header1 header2] }
    let(:file) { instance_double('file') }
    let(:csv_reader) { instance_double('enumerator') }

    before do
      allow(job).to receive(:encoding).and_return(encoding)
      allow(CSV).to receive(:open).and_return(csv_reader)
      allow(csv_reader).to receive(:readline).and_return(list_of_headers)
      job.send(:fetch_csv_headers, file)
    end

    it 'is expected to detect the CSV char encoding' do
      expect(job).to have_received(:encoding).with(file)
    end

    it 'is expected for CSV to have received :open' do
      expect(CSV).to have_received(:open).with(file, { quote_char: '"', col_sep: ';', encoding: encoding })
    end

    it 'is expected for csv reader to receive enumerator' do
      expect(csv_reader).to have_received(:readline)
    end
  end

  describe 'encoding' do
    let(:file) { instance_double(File) }
    let(:file_path) { 'somewhere/over/the/filesystem' }
    let(:encoding) { 'ISO-8859-1' }
    let(:detection) { { encoding: encoding } }

    returned_value = nil

    before do
      allow(file).to receive(:path).and_return(file_path)
      allow(CharlockHolmes::EncodingDetector).to receive(:detect).and_return(detection)

      returned_value = job.send(:encoding, file)
    end

    it 'is expected to get the file path' do
      expect(file).to have_received(:path)
    end

    it 'is expected to detect the encoding' do
      expect(CharlockHolmes::EncodingDetector).to have_received(:detect).with(file_path)
    end

    it 'is expected to return the detected encoding' do
      expect(returned_value).to eq(encoding)
    end
  end
end

# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ExtractInputColumnsJob, type: :job do
  subject(:job) { described_class.new }

  let(:input) { build(:input) }

  describe 'perform' do
    let(:file) { instance_double('input_file') }
    let(:opened) { instance_double('file_opened') }

    before do
      allow(input).to receive(:file).and_return(file)
      allow(file).to receive(:open).and_yield(opened)
      allow(job).to receive(:fetch_csv_headers)
      allow(job).to receive(:save_input_columns_to_db)

      job.perform(input)
    end

    it 'is expected to have opened the file' do
      expect(file).to have_received(:open)
    end

    it 'is expected to have called :fetch_csv_headers method' do
      expect(job).to have_received(:fetch_csv_headers).with(opened)
    end

    it 'is expected to have called :save_input_columns_to_db method' do
      expect(job).to have_received(:save_input_columns_to_db)
    end
  end

  describe 'save_input_columns_to_db' do
    let(:headers) { %w[a b c] }

    before do
      job.instance_variable_set(:@input, input)
      job.instance_variable_set(:@csv_headers, headers)
      allow(InputColumn).to receive(:find_or_create_by)
      job.save_input_columns_to_db
    end

    it 'is expected to have created InputColumn instances with the CSV headers' do
      headers.each do |header|
        expect(InputColumn).to have_received(:find_or_create_by).with(title: header, input_id: input.id)
      end
    end
  end
end

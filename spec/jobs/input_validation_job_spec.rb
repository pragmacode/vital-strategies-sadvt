# frozen_string_literal: true

require 'rails_helper'
require 'support/shared_contexts/mocking_pairing_failure'
require 'support/shared_examples/checking_if_pairing_has_failed'

RSpec.describe InputValidationJob, type: :job do
  subject(:job) { described_class.new }

  describe 'perform' do
    let(:pairing_id) { 42 }
    let(:pairing) { FactoryBot.build(:pairing, id: pairing_id) }
    let(:is_valid) { nil }

    before do
      allow(Pairing).to receive(:find).and_return(pairing)
      allow(pairing).to receive(:validate_input!)
      allow(pairing).to receive(:fail!)
      allow(job).to receive(:check_all_inputs).and_return(is_valid)

      job.perform pairing_id
    end

    describe 'all inputs are valid' do
      let(:is_valid) { true }

      it 'is expected to transition the pairing state to input_validated' do
        expect(pairing).to have_received(:validate_input!)
      end
    end

    describe 'some inputs are not valid' do
      it 'is expected to transition the pairing state to input_validated' do
        expect(pairing).to have_received(:fail!)
      end
    end
  end

  describe 'rescuing errors' do
    it_behaves_like 'checking if pairing rescue Error', NoMethodError
    it_behaves_like 'checking if pairing rescue Error', RuntimeError
  end

  describe 'a_valid_input?' do
    let(:file) { instance_double('file') }
    let(:input_type) { FactoryBot.build :input_type }
    let(:input) { FactoryBot.build :input, input_type: input_type }
    let(:opened_file) { instance_double('opened_file') }

    before do
      allow(input).to receive(:input_type).and_return(input_type)
      allow(input).to receive(:file).and_return(file)
      allow(file).to receive(:open).and_yield(opened_file)
      allow(job).to receive(:check_input).and_return(true)
      allow(ExtractInputColumnsJob).to receive(:perform_later)

      job.a_valid_input? input
    end

    it 'is expected for InputType to have received :find_by' do
      expect(input).to have_received(:input_type)
    end

    it 'is expected for the input file to receive :open' do
      expect(file).to have_received(:open)
    end

    it 'is expected to check if the input is valid' do
      expect(job).to have_received(:check_input).with(opened_file, input_type)
    end

    it 'is expected for ExtractInputColumnsJob to receive :perform_later' do
      expect(ExtractInputColumnsJob).to have_received(:perform_later).with(input)
    end
  end

  describe 'check_input' do
    let(:file) { instance_double('file') }
    let(:list_of_headers) { %w[header1 header2] }
    let(:input_type) { FactoryBot.build :input_type }

    before do
      allow(job).to receive(:fetch_csv_headers).and_return(list_of_headers)
      allow(job).to receive(:check_necessary_headers)

      job.check_input file, input_type
    end

    it 'is expected to have fetched the csv headers' do
      expect(job).to have_received(:fetch_csv_headers).with(file)
    end

    it 'is expected for the job to have received :check_necessary_header' do
      expect(job).to have_received(:check_necessary_headers).with(input_type.csv_columns_names, list_of_headers)
    end
  end

  describe 'check_necessary_headers' do
    let(:valid_input_type_headers) { 'header1' }
    let(:invalid_input_type_headers) { 'header2;header3' }
    let(:csv_headers) { %w[header1 header2] }

    it 'is expected to return true if the required input headers is a subset of the csv headers' do
      expect(job.check_necessary_headers(valid_input_type_headers, csv_headers)).to be_truthy
    end

    it 'is expected to return false if the required input headers is not a subset of the csv headers' do
      expect(job.check_necessary_headers(invalid_input_type_headers, csv_headers)).to be_falsey
    end
  end

  describe 'check_all_inputs' do
    let(:pairing) { FactoryBot.build(:pairing) }
    let(:input) { FactoryBot.build(:input, pairing: pairing) }
    let(:inputs) { [input] }

    before do
      allow(pairing).to receive(:inputs).and_return(inputs)
      allow(inputs).to receive(:all?).and_yield(input)
      allow(job).to receive(:a_valid_input?)

      job.check_all_inputs pairing
    end

    it 'is expected for the pairing to have inputs' do
      expect(pairing).to have_received(:inputs)
    end

    it 'is expected for the inputs to have received :all?' do
      expect(inputs).to have_received(:all?)
    end

    it 'is expected for the job to have received :a_valid_input?' do
      expect(job).to have_received(:a_valid_input?).with(input)
    end
  end
end

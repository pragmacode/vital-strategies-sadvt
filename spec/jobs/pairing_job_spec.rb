# frozen_string_literal: true

require 'support/shared_contexts/mocking_pairing_failure'
require 'support/shared_examples/checking_if_pairing_has_failed'

RSpec.describe PairingJob, type: :job do
  subject(:job) { described_class.new }

  let(:current_script_index) { 42 }

  describe 'perform' do
    let(:pairing) { FactoryBot.build :pairing, id: 314 }

    before do
      allow(Pairings::Runner).to receive(:calculate_pairing)
    end

    context 'when there is no problem' do
      before do
        allow(Pairing).to receive(:find).and_return(pairing)

        job.perform pairing.id, current_script_index
      end

      it 'is expected to call the :find_pairing method' do
        expect(Pairing).to have_received(:find).with(pairing.id)
      end

      it 'is expected to call the Pairing Runner' do
        expect(Pairings::Runner).to have_received(:calculate_pairing).with(pairing, current_script_index)
      end
    end
  end

  describe 'rescue_from' do
    include_context 'when mocking pairing receiving :fail!'

    before do
      allow(Pairings::Runner).to receive(:calculate_pairing).and_raise(RuntimeError)

      described_class.perform_later pairing.id, current_script_index
    end

    it 'is expected for pairing to be set as errored' do
      expect(pairing).to have_received(:fail!)
    end
  end
end

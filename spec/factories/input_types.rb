# frozen_string_literal: true

FactoryBot.define do
  factory :input_type do
    name { 'MyString' }
  end
end

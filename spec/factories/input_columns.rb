# frozen_string_literal: true

FactoryBot.define do
  factory :input_column do
    association(:input, strategy: :build)
    sequence(:title) { |n| "Input Column #{n}" }
  end
end

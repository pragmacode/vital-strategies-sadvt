# frozen_string_literal: true

FactoryBot.define do
  factory :input do
    pairing { nil }
    input_type { nil }
  end
end

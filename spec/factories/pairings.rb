# frozen_string_literal: true

FactoryBot.define do
  factory :pairing do
    kind { 'sat_sim' }
  end
end

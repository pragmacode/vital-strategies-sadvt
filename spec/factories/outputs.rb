# frozen_string_literal: true

FactoryBot.define do
  factory :output do
    script_name { 'MyString' }
    pairing
  end
end

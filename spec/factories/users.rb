# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    level { 'manager' }
    password { 'Sup3rSecr3tP4ssword' }
    sequence(:email) { |n| "user_test_#{n}@test.com" }
  end
end

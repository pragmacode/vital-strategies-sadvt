# frozen_string_literal: true

RSpec.shared_context 'when fetching outputs by is_final attribute' do
  let(:pairing) { FactoryBot.build(:pairing) }
  let(:outputs) { instance_double('pairing outputs') }

  before do
    allow(pairing).to receive(:list_outputs).and_return(outputs)
    allow(outputs).to receive(:where)
  end

  it 'is expected for pairing to have received :list_outputs' do
    expect(pairing).to have_received(:list_outputs)
  end
end

RSpec.shared_context 'when mocking input fields objects' do
  let(:input_type) { FactoryBot.build(:input_type) }
  let(:input) { FactoryBot.build(:input, input_type: input_type) }
  let(:input_fields) { instance_double('input_fields', object: input) }
end

RSpec.shared_context 'when checking multiple conditions' do
  before do
    allow(helper).to receive(:separate_conditions)
  end

  it 'is expected for pairing to be nil' do
    expect(helper).to have_received(:separate_conditions)
  end
end

RSpec.shared_context 'when mocking pairing methods results' do |method, has_official_file, expected|
  result = nil

  before do
    allow(pairing).to receive(:official_file?).and_return(has_official_file)
    result = helper.send(method, pairing)
  end

  it 'is expected for pairing to have received :official_file?' do
    expect(pairing).to have_received(:official_file?)
  end

  it 'is expected for result to be expected' do
    expect(result).to eq(expected)
  end
end

RSpec.describe PairingsHelper, type: :helper do
  describe 'options_for_pairing_kind_select' do
    returned_value = nil

    before do
      allow(I18n).to receive(:t)

      returned_value = helper.options_for_pairing_kind_select
    end

    it 'is expected to return the options Array' do
      expect(returned_value).to be_an(Array)
    end

    it 'is expected to translate each kind' do
      returned_value.drop(1).each do |value|
        expect(I18n).to have_received(:t).with("activerecord.attributes.pairing.kinds.#{value[1]}")
      end
    end

    it 'is expected to return one Array for each kind' do
      returned_value.drop(1).each do |value|
        expect(Pairing::KINDS).to include(value[1])
      end
    end

    it 'is expected to return an empty option first' do
      expect(returned_value[0]).to eq([])
    end
  end

  describe 'file_type_to_upload' do
    include_context 'when mocking input fields objects'

    let(:translation) { instance_double('message') }
    let(:input_types) { instance_double('input_types') }

    before do
      allow(I18n).to receive(:t).and_return(translation)
      allow(translation).to receive(:[]).and_return('message')
      allow(input_types).to receive(:[]).and_return(input_type)

      file_type_to_upload(input_fields)
    end

    it 'is expected to translate the input type name' do
      expect(I18n).to have_received(:t).once.with('activerecord.attributes.input_type.names')
    end

    it 'is expected to translate the text prefix' do
      expect(I18n).to have_received(:t).once.with('pairings.new_inputs.upload_message')
    end

    it 'is expected to return the predicted outcome' do
      expect(file_type_to_upload(input_fields)).to eq("#{translation} message")
    end
  end

  describe 'trigger_button_class' do
    let(:it_is_valid) { nil }
    let(:pairing) { FactoryBot.build :pairing }

    before do
      allow(pairing).to receive(:input_validated?).and_return(it_is_valid)

      helper.trigger_button_class pairing
    end

    it 'is expected to receive :input_validated? method' do
      expect(pairing).to have_received(:input_validated?)
    end

    [true, false].each do |is_valid|
      context "when the pairing input is #{'not ' unless is_valid}valid" do
        let(:it_is_valid) { is_valid }

        it "is expected to have returned btn btn-success#{' disabled' unless is_valid}" do
          expected_outcome = "btn btn-success#{' disabled' unless is_valid}"
          expect(helper.trigger_button_class(pairing)).to eq expected_outcome
        end
      end
    end
  end

  describe 'show_final_outputs' do
    include_context 'when fetching outputs by is_final attribute'

    before do
      helper.show_final_outputs(pairing)
    end

    it 'is expected for the outputs to have received :where' do
      expect(outputs).to have_received(:where).with(is_final: true)
    end
  end

  describe 'show_intermediate_outputs' do
    include_context 'when fetching outputs by is_final attribute'

    before do
      helper.show_intermediate_outputs(pairing)
    end

    it 'is expected for the outputs to have received :where' do
      expect(outputs).to have_received(:where).with(is_final: false)
    end
  end

  describe 'display_upload_file?' do
    include_context 'when checking multiple conditions'
    include_context 'when mocking input fields objects'

    before do
      helper.display_upload_file?(input_fields)
    end
  end

  describe 'upload_button_text' do
    let(:pairing) { build(:pairing) }

    context 'when pairing is done' do
      include_context 'when mocking pairing methods results', :upload_button_text, true, '.update_official_file'
    end

    context 'when pairing is not done' do
      include_context 'when mocking pairing methods results', :upload_button_text, false, '.add_official_file'
    end
  end

  describe 'separate_conditions' do
    let(:default_value) { instance_double('default_value') }
    let(:true_condition) { [(42.i).instance_of?(Complex), 'is complex number'] }
    let(:false_condition) { [(3.1415).instance_of?(Integer), 'is integer number'] }

    it 'is expected to have returned the given outcome' do
      expect(helper.separate_conditions(nil, true_condition)).to eq(true_condition[1])
    end

    it 'is expected to have returned nil' do
      expect(helper.separate_conditions(default_value, false_condition)).to eq(default_value)
    end
  end

  describe 'input_an_official_file?' do
    include_context 'when mocking input fields objects'

    before do
      helper.display_upload_file?(input_fields)
    end

    it 'is expected to have returned the given outcome' do
      expect(helper.input_an_official_file?(input_fields)).to eq(input_type.name == 'official_file')
    end
  end

  describe 'show_upload_button?' do
    let(:user) { build(:user) }
    let(:pairing) { build(:pairing) }

    include_context 'when checking multiple conditions'

    before do
      helper.show_upload_button?(pairing, user)
    end
  end
end

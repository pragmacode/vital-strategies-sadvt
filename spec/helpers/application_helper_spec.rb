# frozen_string_literal: true

RSpec.describe ApplicationHelper, type: :helper do
  describe 'css_class_for_flash_type' do
    it 'maps alert types to bootstrap css classes' do
      pairings = [%w[alert warning], %w[error danger], %w[notice info], %w[success success], %w[info info]]
      pairings.each do |flash_type, css_class|
        expect(helper.css_class_for_flash_type(flash_type)).to eq(css_class)
      end
    end
  end
end

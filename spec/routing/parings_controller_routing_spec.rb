# frozen_string_literal: true

RSpec.shared_examples 'routes with a pairing ID' do |suffix, action, verb = :get|
  let(:url) { "/pairings/1/#{suffix}" }

  it { is_expected.to route(verb, url).to(controller: :pairings, action: action, pairing_id: 1) }
end

RSpec.describe PairingsController, type: :routing do
  let(:base) { '/pairings/1' }

  describe 'routing' do
    it { is_expected.to route(:get, '/').to(controller: :pairings, action: :index) }
    it { is_expected.to route(:get, '/pairings/new').to(controller: :pairings, action: :new) }
    it { is_expected.to route(:post, '/pairings').to(controller: :pairings, action: :create) }
    it { is_expected.to route(:get, base).to(controller: :pairings, action: :show, id: 1) }
    it { is_expected.to route(:put, base).to(controller: :pairings, action: :update, id: 1) }
    it { is_expected.to route(:delete, base).to(controller: :pairings, action: :destroy, id: 1) }

    it_behaves_like 'routes with a pairing ID', 'run', :run
    it_behaves_like 'routes with a pairing ID', 'inputs', :new_inputs
    it_behaves_like 'routes with a pairing ID', 'official_base', :official_file_input
    it_behaves_like 'routes with a pairing ID', 'select_input_columns', :select_input_columns
    it_behaves_like 'routes with a pairing ID', 'pairing_columns', :pick_pairing_columns, :patch
    it_behaves_like 'routes with a pairing ID', 'official_base_save', :save_official_file, :patch
  end
end

# frozen_string_literal: true

RSpec.describe InputsController, type: :routing do
  describe 'routing' do
    it { is_expected.to route(:get, '/inputs/official_files').to(controller: :inputs, action: :official_files) }
  end
end

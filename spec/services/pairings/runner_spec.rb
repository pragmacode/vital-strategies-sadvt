# frozen_string_literal: true

RSpec.shared_examples 'checking :save_output call with different files' do |something = 'the file(s)'|
  it "is expected to save #{something} for the script" do
    expect(described_class).to have_received(:save_output).with(pairing, script[:name], source, output_specs)
  end
end

RSpec.describe Pairings::Runner, type: :service do
  describe 'calculate_pairing' do
    let(:pairing) { FactoryBot.build(:pairing, kind: 'sat_sim') }
    let(:current_script_index) { 0 }
    let(:it_is_done) { nil }

    before do
      allow(described_class).to receive(:manage_pairing_state)
      allow(described_class).to receive(:execute_pairing)
      allow(PairingJob).to receive(:perform_later)
      allow(pairing).to receive(:done?).and_return(it_is_done)

      described_class.calculate_pairing(pairing, current_script_index)
    end

    it 'is expected for the class to have called manage_pairing_state' do
      expect(described_class).to have_received(:manage_pairing_state)
    end

    context 'when the script is already completed' do
      let(:it_is_done) { true }

      it 'is expected for described class not to have executed pairing' do
        expect(described_class).not_to have_received(:execute_pairing)
      end

      it 'is expected not to try to schedule any other job' do
        expect(PairingJob).not_to have_received(:perform_later)
      end
    end

    context 'when the script is available' do
      let(:it_is_done) { false }

      it 'is expected for described class to have executed pairing' do
        expect(described_class).to have_received(:execute_pairing)
      end

      it 'is expected schedule the next job' do
        expect(PairingJob).to have_received(:perform_later).with(pairing.id, current_script_index + 1)
      end
    end
  end

  describe 'execute_pairing' do
    let(:tmp_dir) { instance_double('tmpdir') }
    let(:pairing) { FactoryBot.build(:pairing, kind: 'sat_sim') }
    let(:script) { { inputs: 'inputs', arguments: 'arguments', file: 'file' } }
    let(:joined_inputs) { instance_double(String) }
    let(:stdout) { instance_double('standard_output') }

    before do
      allow(Dir).to receive(:mktmpdir).and_yield(tmp_dir)
      allow(described_class).to receive(:fetch_inputs).and_return(joined_inputs)
      allow(described_class).to receive(:fetch_arguments).and_return('arguments')
      allow(described_class).to receive(:run).and_return(stdout)
      allow(described_class).to receive(:save_outputs)
      described_class.execute_pairing(pairing, script)
    end

    it 'is expected to have tried to create a temporary Directory' do
      expect(Dir).to have_received(:mktmpdir)
    end

    it 'is expected to have fetched inputs' do
      expect(described_class).to have_received(:fetch_inputs).with(pairing, script[:inputs], tmp_dir)
    end

    it 'is expected to have fetched arguments' do
      expect(described_class).to have_received(:fetch_arguments).with(pairing, script[:arguments])
    end

    it 'is expected to have ran the script' do
      expect(described_class).to have_received(:run).with(
        pairing, script[:file], tmp_dir, joined_inputs, 'arguments'
      )
    end

    it 'is expected to have saved script outputs' do
      expect(described_class).to have_received(:save_outputs).with(pairing, script, tmp_dir, stdout)
    end
  end

  describe 'fetch_inputs' do
    let(:directory) { '/directory' }
    let(:pairing) { FactoryBot.build :pairing }
    let(:inputs) { instance_double('inputs') }
    let(:files_array) { instance_double('files') }
    let(:names_array) { instance_double('names_array') }

    before do
      allow(inputs).to receive(:map).and_return(files_array)
      allow(files_array).to receive(:map).and_return(names_array)
      allow(described_class).to receive(:download_and_copy_inputs)

      described_class.fetch_inputs(pairing, inputs, directory)
    end

    it 'is expected for inputs to have received map' do
      expect(inputs).to have_received(:map)
    end

    it 'is expected for files array to have received :map twice' do
      expect(files_array).to have_received(:map)
    end
  end

  describe 'download_and_copy_inputs' do
    let(:filename) { 'filename' }
    let(:directory) { '/directory' }
    let(:tmpfile_path) { '/tmp/test_path' }
    let(:file) { instance_double('file') }
    let(:tmpfile) { instance_double('tmpfile') }

    before do
      allow(file).to receive(:open).and_yield(tmpfile)
      allow(file).to receive(:filename).and_return(filename)
      allow(tmpfile).to receive(:path).and_return(tmpfile_path)
      allow(FileUtils).to receive(:copy)

      described_class.download_and_copy_inputs([file], directory)
    end

    it 'is expected to open the file' do
      expect(file).to have_received(:open)
    end

    it 'is expected to have copied to the directory' do
      expect(FileUtils).to have_received(:copy).with(tmpfile_path, "#{directory}/#{filename}")
    end
  end

  describe 'fetch_arguments' do
    let(:pairing) { build(:pairing) }
    let(:evaluation) { 'evaluation' }
    let(:argument_list) { %w[arg1 arg2] }
    let(:expected_outcome) { "'evaluation' 'evaluation'" }

    before do
      allow(described_class).to receive(:evaluate_argument).and_return(evaluation)
    end

    it 'is expected to have returned an expected outcome for given args list' do
      expect(described_class.fetch_arguments(pairing, argument_list)).to eq(expected_outcome)
    end

    it 'is expected to return a blank string when given no argument list' do
      expect(described_class.fetch_arguments(pairing, nil)).to eq('')
    end
  end

  describe 'evaluate_argument' do
    let(:pairing) { build(:pairing) }
    let(:argument) { { method: :run_a_method } }

    before do
      allow(pairing).to receive(:send)
      described_class.evaluate_argument(argument, pairing)
    end

    it 'is expected to have sent a method to the pairing instance' do
      expect(pairing).to have_received(:send).with(argument[:method])
    end
  end

  describe 'run' do
    let(:output_file) { instance_double('output_file') }
    let(:output_file_path) { '/tmp/output_file.123' }
    let(:script) { 'script.R' }
    let(:joined_inputs) { %w[input1 input2 input12] }
    let(:pairing) { FactoryBot.build :pairing }

    before do
      allow(Tempfile).to receive(:new).and_return(output_file)
      allow(output_file).to receive(:path).and_return(output_file_path)
      allow(described_class).to receive(:`)
      allow(Dir).to receive(:chdir).and_yield

      described_class.run(pairing, script, '/tmp', joined_inputs, "'set' 'of' 'arguments'")
    end

    it 'is expected to create a temporary file to redirect the output_file' do
      expect(Tempfile).to have_received(:new).with(pairing.kind)
    end

    it 'is expected to get the temporary file path' do
      expect(output_file).to have_received(:path)
    end

    it 'is expected to change the directory to the one with the scripts' do
      expect(Dir).to have_received(:chdir).with(described_class::SCRIPTS_PATH)
    end

    it 'is expected to call the respective pairing script' do
      directory = '/tmp'

      inputs_full_path = joined_inputs.map { |input| "#{directory}/#{input}" }.join(' ')

      expect(described_class).to have_received(:`)
        .with("Rscript #{script} #{inputs_full_path} #{directory} 'set' 'of' 'arguments' &> #{output_file_path}")
    end
  end

  describe 'save_outputs' do
    let(:pairing) { FactoryBot.build :pairing }
    let(:script) do
      { name: 'script', outputs: [{ filename: 'output.txt', is_final: false }] }
    end
    let(:directory) { '/directory' }
    let(:stdout) { instance_double('std_out') }
    let(:dir_entries) { %w[output.txt not_an_output.txt] }

    before do
      allow(described_class).to receive(:check_and_save_dir_entry)
      allow(described_class).to receive(:save_output)
      allow(Dir).to receive(:entries).and_return(dir_entries)

      described_class.save_outputs(pairing, script, directory, stdout)
    end

    it_behaves_like 'checking :save_output call with different files', 'the standard output' do
      let(:source) { stdout }
      let(:output_specs) { { filename: "Programacao_de_#{script[:name]}.txt", is_final: false } }
    end

    it 'is expected to list the directory files' do
      expect(Dir).to have_received(:entries).with(directory)
    end

    it 'is expected to check_and_save_dir_entry for each directory file' do
      dir_entries.each do |entry|
        expect(described_class).to have_received(:check_and_save_dir_entry).with(
          pairing, script, directory, entry
        )
      end
    end
  end

  describe 'check_and_save_dir_entry' do
    let(:pairing) { FactoryBot.build :pairing }
    let(:script) do
      { name: 'script', outputs: [{ filename: 'output.txt', is_final: false }] }
    end
    let(:string_vars) { { directory: '/directory', entry: 'file.txt' } }
    let(:opened_file) { instance_double('file') }

    before do
      allow(File).to receive(:open).and_return(opened_file)
      allow(described_class).to receive(:save_output)
      allow(described_class).to receive(:find_output).and_return(output_spec)

      described_class.check_and_save_dir_entry(pairing, script, string_vars[:directory], string_vars[:entry])
    end

    context 'when the found output is nil' do
      let(:output_spec) { nil }

      [[File, :open], [described_class, :save_output]].each do |test|
        it "is expected for #{test[0]} to not have received #{test[1]}" do
          expect(test[0]).not_to have_received(test[1])
        end
      end
    end

    context 'when the found output is not nil' do
      let(:output_spec) { { filename: 'file.txt', is_final: true } }

      it 'is expected to have opened the file' do
        expect(File).to have_received(:open).with("#{string_vars[:directory]}/#{string_vars[:entry]}")
      end

      it_behaves_like 'checking :save_output call with different files', 'the standard output' do
        let(:source) { opened_file }
        let(:output_specs) { output_spec }
      end
    end
  end

  describe 'find_output' do
    let(:outputs_list) do
      [
        { original_filename: 'file1', filename: 'file1', is_final: true },
        { original_filename: 'file2', filename: 'file2', is_final: false }
      ]
    end

    [
      { searched_file: 'file1', expected_outcome: { original_filename: 'file1', filename: 'file1', is_final: true } },
      { searched_file: 'file3', expected_outcome: nil }
    ].each do |test_case|
      text = test_case[:expected_outcome].nil? ? '' : 'not '
      it "is expected #{text}to return nil when looking for #{test_case[:searched_file]}" do
        expect(described_class.find_output(outputs_list, test_case[:searched_file])).to eq test_case[:expected_outcome]
      end
    end
  end

  describe 'save_output' do
    let(:pairing) { FactoryBot.build :pairing }
    let(:output) { FactoryBot.build(:output) }
    let(:file) { instance_double('file') }
    let(:output_spec) { { filename: 'test_file.txt', is_final: true } }
    let(:script_name) { 'testing_script' }

    before do
      allow(pairing.outputs).to receive(:build).and_return(output)
      allow(output.file).to receive(:attach)
      allow(output).to receive(:save!)
      described_class.save_output(pairing, script_name, file, output_spec)
    end

    it 'is expected to build an Output for the pairing' do
      expect(pairing.outputs).to have_received(:build).with(script_name: script_name,
                                                            is_final: output_spec[:is_final])
    end

    it 'is expected to attach the output_file to the built Output instance' do
      expect(output.file).to have_received(:attach).with(
        io: file, filename: output_spec[:filename]
      )
    end

    it 'is expected to save the built Output instance' do
      expect(output).to have_received(:save!)
    end
  end

  describe 'fetch_input_wrapper' do
    let(:pairing) { FactoryBot.build :pairing }
    let(:input) { { kind: 'Input', type: 'input' } }

    before do
      allow(described_class).to receive(:fetch_an_input)
      allow(described_class).to receive(:fetch_an_output)

      described_class.fetch_input_wrapper(pairing, input)
    end

    context 'when you\'re given an Input' do
      it 'is expected to have received :fetch_an_input' do
        expect(described_class).to have_received(:fetch_an_input).with(pairing, 'input')
      end
    end

    context 'when you\'re given an Output' do
      let(:input) { { kind: 'Output', filename: 'filename' } }

      it 'is expected to have received :fetch_an_output' do
        expect(described_class).to have_received(:fetch_an_output).with(pairing, 'filename')
      end
    end
  end

  describe 'fetch_an_input' do
    let(:pairing) { FactoryBot.build :pairing }
    let(:inputs_list) { instance_double('inputs') }
    let(:input) { instance_double(Input) }
    let(:type) { 'type' }

    before do
      allow(pairing.inputs).to receive(:where).and_return(inputs_list)
      allow(inputs_list).to receive(:first).and_return(input)
      allow(input).to receive(:file)

      described_class.fetch_an_input(pairing, type)
    end

    it 'is expected for the pairing inputs to have received :where' do
      expect(pairing.inputs).to have_received(:where).with(input_type: InputType.find_by(name: type))
    end

    it 'is expected for the inputs list to have received :first' do
      expect(inputs_list).to have_received(:first)
    end

    it 'is expected to have fetched the input file' do
      expect(input).to have_received(:file)
    end
  end

  describe 'fetch_an_output' do
    let(:pairing) { FactoryBot.build :pairing }
    let(:list_of_outputs) { instance_double('outputs') }
    let(:output) { instance_double(Output) }
    let(:filename) { 'the_file_name' }
    let(:file) { instance_double('file') }

    returned = nil

    before do
      allow(pairing).to receive(:list_outputs).and_return(list_of_outputs)
      allow(list_of_outputs).to receive(:find).and_return(output)
      allow(output).to receive(:file).and_return(file)

      returned = described_class.fetch_an_output(pairing, filename)
    end

    it 'is expected for the pairing to have received :list_outputs' do
      expect(pairing).to have_received(:list_outputs)
    end

    it 'is expected for the outputs lits to have received :find' do
      expect(list_of_outputs).to have_received(:find)
    end

    it 'is expected for the output to have received :file' do
      expect(output).to have_received(:file)
    end

    it 'is expected to have returned the file' do
      expect(returned).to eq(file)
    end
  end

  describe 'manage_pairing_state' do
    let(:job_index) { 0 }
    let(:script_is_nil) { false }
    let(:script) { instance_double('script') }
    let(:pairing) { FactoryBot.build :pairing }

    before do
      allow(pairing).to receive(:run!)
      allow(pairing).to receive(:finish!)
      allow(script).to receive(:nil?).and_return(script_is_nil)
      described_class.manage_pairing_state(pairing, script, job_index)
    end

    context 'when job_index is zero and script is not nil' do
      it 'is expected for pairing to receive :run!' do
        expect(pairing).to have_received(:run!)
      end

      it 'is expected for pairing not to receive :finish!' do
        expect(pairing).not_to have_received(:finish!)
      end
    end

    context 'when job_index is not zero and script is nil' do
      let(:job_index) { 42 }
      let(:script_is_nil) { true }

      it 'is expected for pairing not to receive :run!' do
        expect(pairing).not_to have_received(:run!)
      end

      it 'is expected for pairing to receive :finish!' do
        expect(pairing).to have_received(:finish!)
      end
    end
  end
end

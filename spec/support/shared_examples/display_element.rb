# frozen_string_literal: true

RSpec.shared_examples 'display element' do |name, element, classes, match|
  it "is expected to display the #{name}" do
    expect(rendered).to have_selector(element, class: classes)
  end

  it "is expected to match the #{name}" do
    expect(rendered).to match(match)
  end
end

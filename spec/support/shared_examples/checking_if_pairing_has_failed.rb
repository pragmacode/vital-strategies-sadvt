# frozen_string_literal: true

RSpec.shared_examples 'checking if pairing rescue Error' do |exception|
  describe "rescue_from #{exception}" do
    include_context 'when mocking pairing receiving :fail!'

    before do
      allow(pairing).to receive(:validate_input!).and_raise(exception)

      described_class.perform_later pairing
    end

    it 'is expected for pairing to be set as errored' do
      expect(pairing).to have_received(:fail!)
    end
  end
end

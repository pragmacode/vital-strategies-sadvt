# frozen_string_literal: true

RSpec.shared_examples 'validates presence and inclusion of' do |attribute, values|
  it { is_expected.to validate_presence_of(attribute) }
  it { is_expected.to validate_inclusion_of(attribute).in_array(values) }
end

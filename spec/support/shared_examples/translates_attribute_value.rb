# frozen_string_literal: true

RSpec.shared_examples 'translates attribute value' do |attribute, method|
  let(:translation) { 'translation' }

  returned_value = nil

  before do
    allow(I18n).to receive(:t).and_return(translation)

    returned_value = subject.send(method)
  end

  it 'is expected to return the translation' do
    expect(returned_value).to eq(translation)
  end

  it 'is expected to use I18n' do
    expect(I18n).to have_received(:t).with(
      "activerecord.attributes.#{described_class.name.underscore}.#{attribute}s.#{subject.send(attribute)}"
    )
  end
end

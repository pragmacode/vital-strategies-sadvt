# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_examples 'setter methods' do |klass, method|
  before do
    allow(klass).to receive(:find)
    subject.send(method)
  end

  it 'is expected to have called the find method' do
    expect(klass).to have_received(:find)
  end
end

# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_examples 'render specific fields with ID' do |entity, field, id = nil|
  it "is expected for the #{entity} form to render the #{field} field (ID #{id})" do
    field_id = "#{entity}_#{id}_#{field}"
    expect(rendered).to have_field(field_id)
  end
end

RSpec.shared_examples 'render specific fields' do |entity, field|
  it "is expected for the #{entity} form to render the #{field} field" do
    field_id = "#{entity}_#{field}"
    expect(rendered).to have_field(field_id)
  end
end

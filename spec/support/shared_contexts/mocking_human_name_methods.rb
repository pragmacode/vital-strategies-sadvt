# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_context 'when mocking human name methods' do |human_method|
  before do
    allow(instance).to receive(human_method).and_return(human_name)
  end
end

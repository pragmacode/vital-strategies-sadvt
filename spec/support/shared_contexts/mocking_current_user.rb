# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_context 'when mocking :current_user call' do
  let(:current_user) { create(:user) }

  before do
    allow(controller).to receive(:current_user).and_return(current_user)
  end

  it 'is expected to have received the current user data' do
    expect(controller).to have_received(:current_user).at_least(:once)
  end
end

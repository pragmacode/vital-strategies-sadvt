# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_context 'when mocking pairing receiving :fail!' do
  let(:pairing) { FactoryBot.build(:pairing, id: 42) }

  before do
    allow(Pairing).to receive(:find).and_return(pairing)
    allow(pairing).to receive(:fail!)
  end
end

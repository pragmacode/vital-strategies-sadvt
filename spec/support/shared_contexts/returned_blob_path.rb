# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_context 'with returned blob path' do
  let(:file_url) { 'http://host.com.br/file' }

  before do
    allow(view).to receive(:rails_blob_url).and_return(file_url)
  end
end

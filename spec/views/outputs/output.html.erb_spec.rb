# frozen_string_literal: true

require 'rails_helper'
require 'support/shared_contexts/returned_blob_path'

partial = 'outputs/output'

RSpec.describe partial, type: :view do
  include_context 'with returned blob path'
  let(:output) { FactoryBot.build(:output) }
  let(:output_file) { instance_double('file') }
  let(:filename) { 'output.file' }

  before do
    allow(output).to receive(:file).and_return(output_file)
    allow(output_file).to receive(:filename).and_return(filename)

    render partial: partial, locals: { output: output }
  end

  it 'is expected to render the name value' do
    expect(rendered).to have_content(output.file.filename)
  end

  it 'is expected to generate the input file download url' do
    expect(view).to have_received(:rails_blob_url).with(output.file, disposition: 'attachment')
  end

  it 'is expected to render a link to show' do
    expect(rendered).to have_link(I18n.t('inputs.input.download'), href: file_url)
  end
end

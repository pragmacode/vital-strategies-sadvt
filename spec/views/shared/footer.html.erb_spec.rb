# frozen_string_literal: true

require 'rails_helper'
require 'support/shared_examples/display_element'

partial = 'shared/footer'

RSpec.describe partial, type: :view do
  before do
    render partial: partial
  end

  context 'when render logo images' do
    include_examples 'display element', 'first logo', 'p', 'm-3', /CET-branco.*\.png/
    include_examples 'display element', 'second logo', 'p', 'm-3', /Mobilidade.*\.png/
    include_examples 'display element', 'third logo', 'p', 'm-3', /saude.*\.png/
    include_examples 'display element', 'fourth logo', 'p', 'm-3', /BP_IGRS_Logo.*\.png/
  end

  it 'is expected to render the version' do
    expect(rendered).to have_content('Versão')
  end

  it 'is expected to render repository link' do
    expect(rendered).to have_link('Repositório', href: 'https://gitlab.com/pragmacode/vital-strategies-sadvt')
  end

  it 'is expected to render license link' do
    expect(rendered).to have_link('Licença AGPL v3.0', href: 'https://gitlab.com/pragmacode/vital-strategies-sadvt/-/blob/master/COPYING')
  end
end

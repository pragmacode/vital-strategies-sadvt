# frozen_string_literal: true

partial = 'shared/navbar'

RSpec.shared_examples 'check menu item' do |item, href, selector|
  it "is expected to render a link to #{item}" do
    expect(rendered).send(
      selector,
      have_selector(:link, I18n.t("shared.navbar.#{item}"), href: href || '#', class: 'nav-link')
    )
  end
end

RSpec.describe partial, type: :view do
  let(:user_level) { 'visitor' }
  let(:user) { build(:user, level: user_level) }

  before do
    allow(view).to receive(:current_user).and_return user

    render partial: partial
  end

  it 'is expected to render the title' do
    expect(rendered).to have_content('SPVT')
  end

  it 'is expected to check the current user' do
    expect(view).to have_received(:current_user).at_least(:once)
  end

  it_behaves_like 'check menu item', 'pairings', Rails.application.routes.url_helpers.root_path, :to
  it_behaves_like 'check menu item', 'official_files', Rails.application.routes.url_helpers.inputs_official_files_path,
                  :to
  it_behaves_like 'check menu item', 'abbreviations',
                  Rails.application.routes.url_helpers.descriptions_abbreviations_path,
                  :to
  it_behaves_like 'check menu item', 'logout', Rails.application.routes.url_helpers.destroy_user_session_path, :to
  it_behaves_like 'check menu item', 'admin', Rails.application.routes.url_helpers.trestle_path, :to_not

  context 'when the current user is a administrator' do
    let(:user_level) { 'admin' }

    it_behaves_like 'check menu item', 'admin', Rails.application.routes.url_helpers.trestle_path, :to
  end

  context 'without a logged in user' do
    let(:user) { nil }

    it_behaves_like 'check menu item', 'login', Rails.application.routes.url_helpers.new_user_session_path, :to
  end
end

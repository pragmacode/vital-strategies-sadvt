# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_examples 'checking if a link was rendered or not' do |expectancy, i18n_key|
  it 'is expected to render a link to the inputs upload page' do
    expect(rendered).send(expectancy, have_link(I18n.t(i18n_key)))
  end
end

RSpec.describe 'pairings/show.html.erb', type: :view do
  let(:user) { build(:user, level: 'admin') }
  let(:pairing) { FactoryBot.build(:pairing, created_at: Time.zone.now, id: 42, aasm_state: 'done') }
  let(:human_kind_name) { 'SAT/SIH' }
  let(:inputs) { [FactoryBot.build(:input, input_type: build(:input_type, name: 'samu'))] }
  let(:outputs) { [FactoryBot.build(:output)] }

  before do
    allow(view).to receive(:render).and_call_original
    allow(view).to receive(:render).with(inputs)
    allow(view).to receive(:render).with(outputs)
    allow(view).to receive(:current_user).and_return user
    allow(user).to receive(:write_allowed?)
    allow(pairing).to receive(:list_required_inputs).and_return(inputs)
    allow(pairing).to receive(:official_base).and_return(inputs)
    allow(view).to receive(:upload_button_text).and_call_original
    allow(view).to receive(:show_upload_button?).and_call_original
    allow(view).to receive(:show_final_outputs).and_return(outputs)
    allow(view).to receive(:show_intermediate_outputs).and_return(outputs)
    allow(pairing).to receive(:human_kind_name).and_return(human_kind_name)
    allow(pairing).to receive(:group_columns_by_origin).and_return([])

    assign(:pairing, pairing)

    render
  end

  it 'is expected to have listed required inputs' do
    expect(pairing).to have_received(:list_required_inputs)
  end

  it 'is expected to have shown official file' do
    expect(pairing).to have_received(:official_base).twice
  end

  it 'is expected to have received the intermediate outputs' do
    expect(view).to have_received(:show_intermediate_outputs)
  end

  it 'is expected to have received the final outputs' do
    expect(view).to have_received(:show_final_outputs)
  end

  it 'is expected to have received render with inputs' do
    expect(view).to have_received(:render).with(inputs).at_least(:once)
  end

  it 'is expected to have received render with outputs' do
    expect(view).to have_received(:render).with(outputs).twice
  end

  it 'is epxected to render the status value' do
    expect(rendered).to have_content(pairing.aasm.human_state)
  end

  it 'is epxected to render the kind value' do
    expect(rendered).to have_content(human_kind_name)
  end

  it 'is expected to call the model human_kind_name' do
    expect(pairing).to have_received(:human_kind_name)
  end

  it 'is epxected to render the created_at value' do
    expect(rendered).to have_content(I18n.l(pairing.created_at, format: :long))
  end

  it 'is expected to render a link back' do
    expect(rendered).to have_link(I18n.t('pairings.show.back'), href: root_path)
  end

  [true, false].each do |expected_return|
    context "when the user has #{'no' unless expected_return} write access" do
      expectancy = expected_return ? :to : :not_to

      before do
        allow(user).to receive(:write_allowed?).and_return(expected_return)
        allow(user).to receive(:can_select_input_columns?).and_return(expected_return)
        render
      end

      it 'is expected to have checked if the user has write access' do
        expect(user).to have_received(:write_allowed?).at_least(:once)
      end

      it 'is expected to have checked if the user can select columns' do
        expect(user).to have_received(:can_select_input_columns?).with(pairing)
      end

      it_behaves_like 'checking if a link was rendered or not', expectancy, 'pairings.show.run'
      it_behaves_like 'checking if a link was rendered or not', expectancy, 'pairings.show.upload_inputs'
      it_behaves_like 'checking if a link was rendered or not', expectancy, 'pairings.show.set_selected_columns'
    end
  end

  it 'is expected not to render the upload button text' do
    expect(view).not_to have_received(:upload_button_text).with(pairing)
  end

  context 'when pairing is not done' do
    let(:pairing) { FactoryBot.build(:pairing, created_at: Time.zone.now, id: 42, aasm_state: 'created') }

    it 'is expected not to render the upload button text' do
      expect(view).not_to have_received(:upload_button_text).with(pairing)
    end
  end
end

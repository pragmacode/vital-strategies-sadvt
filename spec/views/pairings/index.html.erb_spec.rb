# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'pairings/index.html.erb', type: :view do
  let(:pairings) { 'pairings' }
  let(:user_level) { 'admin' }
  let(:user) { build(:user, level: user_level) }

  before do
    allow(view).to receive(:render).and_call_original
    allow(view).to receive(:render).with(
      partial: 'pairing',
      collection: pairings,
      as: :pairing
    )
    allow(view).to receive(:current_user).and_return user

    assign(:pairings, pairings)

    render
  end

  context 'when current user has write access' do
    let(:user_level) { 'admin' }

    it 'is expected to render the pairings collection partial' do
      expect(view).to have_received(:render).with(
        partial: 'pairing',
        collection: pairings,
        as: :pairing
      )
    end

    it 'is expected to render a link to new' do
      expect(rendered).to have_link(I18n.t('pairings.index.new'), href: new_pairing_path)
    end
  end

  context 'when current user is a visitor' do
    let(:user_level) { 'visitor' }

    it 'is expected to render a link to new' do
      expect(rendered).not_to have_link(I18n.t('pairings.index.new'), href: new_pairing_path)
    end
  end
end

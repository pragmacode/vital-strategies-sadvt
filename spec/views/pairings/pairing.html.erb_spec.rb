# frozen_string_literal: true

require 'rails_helper'
require 'support/shared_contexts/mocking_current_user'
require 'support/shared_contexts/mocking_human_name_methods'

partial = 'pairings/pairing'

RSpec.describe partial, type: :view do
  let(:pairing) { FactoryBot.build(:pairing, created_at: Time.zone.now, id: 42) }
  let(:human_kind_name) { 'SAT/SIH' }

  include_context 'when mocking :current_user call'
  include_context 'when mocking human name methods', :human_kind_name do
    let(:instance) { pairing }
    let(:human_name) { human_kind_name }
  end

  before do
    allow(current_user).to receive(:write_allowed?)
    render partial: partial, locals: { pairing: pairing }
  end

  it 'is expected to render the status value' do
    expect(rendered).to have_content(pairing.aasm.human_state)
  end

  it 'is expected to render the kind value' do
    expect(rendered).to have_content(human_kind_name)
  end

  it 'is expected to call the model human_kind_name' do
    expect(pairing).to have_received(:human_kind_name)
  end

  it 'is expected to render the created_at value' do
    expect(rendered).to have_content(I18n.l(pairing.created_at, format: :long))
  end

  it 'is expected to render a link to show' do
    expect(rendered).to have_link(I18n.t('pairings.pairing.show'), href: pairing_path(pairing))
  end

  it 'is not expected to render a link to delete' do
    expect(rendered).not_to have_link(I18n.t('pairings.pairing.destroy'), href: pairing_path(pairing))
  end
end

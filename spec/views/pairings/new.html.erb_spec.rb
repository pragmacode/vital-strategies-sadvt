# frozen_string_literal: true

require 'rails_helper'
require 'support/shared_examples/render_specific_fields'

RSpec.describe 'pairings/new.html.erb', type: :view do
  let(:pairing) { Pairing.new }

  before do
    assign(:pairing, pairing)

    allow(view).to receive(:options_for_pairing_kind_select).and_return([])

    render
  end

  it 'is expected to render the form tag' do
    expect(rendered).to have_xpath(
      ".//form[@method='post']"\
      "[@action='#{pairings_path(pairing)}']"
    )
  end

  it_behaves_like 'render specific fields', 'pairing', 'kind'

  it 'is expected to use the options_for_pairing_kind_select helper' do
    expect(view).to have_received(:options_for_pairing_kind_select)
  end
end

# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'pairings/official_file_input.html.erb', type: :view do
  let(:pairing) { Pairing.new }
  let(:official_file_input) { Input.new }
  let(:input_type) { InputType.new }
  let(:form) { instance_double('form') }
  let(:input_fields) { instance_double('input_fields') }

  before do
    allow(view).to receive(:form_with).and_yield(form)
    allow(form).to receive(:hidden_field)
    allow(form).to receive(:fields_for).and_yield(input_fields)
    allow(input_fields).to receive(:file_field)
    allow(input_fields).to receive(:label)
    allow(input_fields).to receive(:hidden_field)
    allow(form).to receive(:submit)
    allow(view).to receive(:file_type_to_upload).and_return('file')
    assign(:pairing, pairing)
    assign(:official_file_input, official_file_input)

    render
  end

  it 'is expected to have rendered a form to upsert official file base' do
    expect(view).to have_received(:form_with).with(
      model: pairing, url: { action: :save_official_file, method: :patch },
      local: true, class: 'container form-group pt-4'
    )
  end

  it 'is expected to have a hidden field for input kind' do
    expect(form).to have_received(:hidden_field).with(:kind, value: pairing.kind)
  end

  it 'is expected to have rendered input fields' do
    expect(form).to have_received(:fields_for).with(:inputs, official_file_input)
  end

  it 'is expected to have a file input field' do
    expect(input_fields).to have_received(:file_field).with(:file, class: 'custom-file-input', lang: 'pt')
  end

  it 'is expected to show the file type to upload' do
    expect(view).to have_received(:file_type_to_upload).with(input_fields)
  end

  it 'is expected to show a label for the file field' do
    expect(input_fields).to have_received(:label).with('file', class: 'custom-file-label')
  end

  it 'is expected to have a hidden field for input for input type ID' do
    expect(input_fields).to have_received(:hidden_field).with(:input_type_id)
  end

  it 'is expected to have a submit button' do
    expect(form).to have_received(:submit)
      .with(I18n.t('pairings.official_file_input.save'), class: 'btn btn-primary actions')
  end
end

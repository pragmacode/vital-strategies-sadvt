# frozen_string_literal: true

require 'rails_helper'
require 'support/shared_examples/render_specific_fields'

RSpec.describe 'pairings/new_inputs.html.erb', type: :view do
  let(:pairing) { Pairing.new }
  let(:input) { [Input.new, Input.new] }
  let(:input_type) { InputType.new }
  let(:file_type) { 'file' }

  before do
    allow(view).to receive(:file_type_to_upload).and_return(file_type)
    allow(view).to receive(:display_upload_file?).and_return(true)
    allow(pairing).to receive(:inputs).and_return(input)
    allow(input[0]).to receive(:input_type).and_return(input_type)
    allow(input_type).to receive(:name).and_return('name')

    assign(:pairing, pairing)

    render
  end

  it 'is expected to render the form tag' do
    expect(rendered).to have_xpath(
      ".//form[@method='post']"\
      "[@action='#{pairings_path(pairing)}']"
    )
  end

  it 'is expected to have checked if the upload file is displayed' do
    expect(view).to have_received(:display_upload_file?).exactly(input.length).times
  end

  it_behaves_like 'render specific fields with ID', 'pairing_inputs_attributes', 'file', 0
  it_behaves_like 'render specific fields with ID', 'pairing_inputs_attributes', 'file', 1
end

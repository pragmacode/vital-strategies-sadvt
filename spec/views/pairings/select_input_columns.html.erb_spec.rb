# frozen_string_literal: true

require 'rails_helper'
# require 'support/shared_contexts/mocking_current_user'

RSpec.describe 'pairings/select_input_columns.html.erb', type: :view do
  let(:pairing) { build(:pairing) }
  let(:input_type) { build(:input_type, name: 'samu') }
  let(:form) { instance_double('form') }
  let(:input_columns) { build_list(:input_column, 1, id: 42) }
  let(:input) { build(:input) }

  before do
    allow(view).to receive(:form_with).and_yield(form)
    allow(view).to receive(:pairing_path).and_return('pairing_path')
    allow(I18n).to receive(:t).and_call_original
    allow(view).to receive(:link_to)
    allow(form).to receive(:submit)
    allow(form).to receive(:check_box)
    allow(form).to receive(:label)
    allow(input).to receive(:input_type).and_return(input_type)

    assign(:columns, { input => input_columns })

    render
  end

  it 'is expected to have created a form' do
    expect(view).to have_received(:form_with).with(
      model: nil, local: true, class: 'container form-group pt-4',
      url: { action: :pick_pairing_columns, method: :patch }
    )
  end

  it 'is expected to have a link' do
    expect(view).to have_received(:link_to).with('Voltar', 'pairing_path', class: 'btn btn-secondary')
  end

  it 'is expected to have a submit button' do
    expect(form).to have_received(:submit).with('Salvar colunas escolhidas', class: 'btn btn-primary actions')
  end

  it 'is expected to render a check box' do
    expect(form).to have_received(:check_box).with(:selected_columns,
                                                   { checked: false, multiple: true, class: 'input_columns' },
                                                   input_columns[0].id, nil)
  end

  it 'is expected to render a label' do
    expect(form).to have_received(:label).with(:selected_columns, input_columns[0].title)
  end
end

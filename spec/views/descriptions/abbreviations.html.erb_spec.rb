# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'descriptions/abbreviations.html.erb', type: :view do
  before do
    render
  end

  it 'is expected to render the SAT initials' do
    expect(rendered).to have_content('SAT')
  end

  it 'is expected to render the SAT meaning' do
    expect(rendered).to have_content('Sistemas de Informações de Acidentes de Trânsito')
  end

  it 'is expected to render the SIM initials' do
    expect(rendered).to have_content('SIM')
  end

  it 'is expected to render the SIM meaning' do
    expect(rendered).to have_content('Sistema de Informação sobre Mortalidade')
  end

  it 'is expected to render the SIH initials' do
    expect(rendered).to have_content('SIH')
  end

  it 'is expected to render the SIH meaning' do
    expect(rendered).to have_content('Sistema de Informações Hospitalares do SUS')
  end

  it 'is expected to render the SAMU initials' do
    expect(rendered).to have_content('SAMU')
  end

  it 'is expected to render the SAMU meaning' do
    expect(rendered).to have_content('Serviço de Atendimento Móvel de Urgência')
  end

  it 'is expected to render the InfoSIGA initials' do
    expect(rendered).to have_content('InfoSIGA')
  end

  it 'is expected to render the InfoSIGA meaning' do
    expect(rendered).to have_content('Sistema de Informações Gerenciais de Acidentes de Trânsito')
  end

  it 'is expected to render the SPVT initials' do
    expect(rendered).to have_content('SPVT')
  end

  it 'is expected to render the SPVT meaning' do
    expect(rendered).to have_content('Sistema de Pareamento de dados sobre Violência no Trânsito')
  end
end

# frozen_string_literal: true

require 'rails_helper'
require 'support/shared_contexts/returned_blob_path'

partial = 'inputs/input'

RSpec.describe partial, type: :view do
  include_context 'with returned blob path'
  let(:input_type) { FactoryBot.build(:input_type) }
  let(:input) { FactoryBot.build(:input, input_type: input_type) }
  let(:human_name) { 'SAT' }

  before do
    allow(input_type).to receive(:human_name).and_return(human_name)

    render partial: partial, locals: { input: input }
  end

  it 'is expected to render the name value' do
    expect(rendered).to have_content(human_name)
  end

  it 'is expected to call the model human_name' do
    expect(input_type).to have_received(:human_name)
  end

  it 'is expected to generate the input file download url' do
    expect(view).to have_received(:rails_blob_url).with(input.file, disposition: 'attachment')
  end

  it 'is expected to render a link to show' do
    expect(rendered).to have_link(I18n.t('inputs.input.download'), href: file_url)
  end
end

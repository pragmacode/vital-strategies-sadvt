# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'inputs/_official_file.html.erb', type: :view do
  let(:input) { build(:input, updated_at: Time.zone.now) }

  before do
    allow(view).to receive(:render).and_call_original
    allow(view).to receive(:render).with(input)

    render partial: 'inputs/official_file', locals: { official_file: input }
  end

  it 'is expected to render the input updated_at' do
    expect(rendered).to have_text(input.updated_at)
  end

  it 'is expected to render the input partial' do
    expect(view).to have_received(:render).with(input)
  end
end

# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'inputs/official_files.html.erb', type: :view do
  let(:inputs) { instance_double(ActiveRecord::Relation) }

  before do
    assign(:inputs, inputs)

    allow(view).to receive(:render).and_call_original
    allow(view).to receive(:render).with(partial: 'official_file', collection: inputs)

    render
  end

  it 'is expected to render the title' do
    expect(rendered).to have_text(I18n.t('inputs.official_files.title'))
  end

  it 'is expected to render the official_file partial' do
    expect(view).to have_received(:render).with(partial: 'official_file', collection: inputs)
  end
end

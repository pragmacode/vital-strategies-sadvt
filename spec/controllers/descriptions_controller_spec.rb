# frozen_string_literal: true

require 'rails_helper'

RSpec.describe DescriptionsController, type: :controller do
  before do
    allow(subject).to receive(:authenticate_user!)
  end

  describe 'GET #abbreviations' do
    let(:abbreviations) { 'abbreviations' }

    before do
      allow(subject).to receive(:abbreviations).and_return(abbreviations)

      get :abbreviations, params: { abbreviations: abbreviations }
    end

    it { is_expected.to respond_with(200) }
  end
end

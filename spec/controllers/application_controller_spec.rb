# frozen_string_literal: true

RSpec.describe ApplicationController, type: :controller do
  describe 'after_sign_out_path_for' do
    let(:resource) { instance_double('Devise resource') }

    it 'is expected to go to the new user session path' do
      expect(subject.after_sign_out_path_for(resource)).to eq new_user_session_path
    end
  end
end

# frozen_string_literal: true

RSpec.describe InputsController, type: :controller do
  before do
    allow(subject).to receive(:authenticate_user!)
  end

  describe 'GET #official_files' do
    let(:inputs_including_type_file_attachment_blob) { instance_double(ActiveRecord::Relation) }
    let(:official_files_inputs) { instance_double(ActiveRecord::Relation) }

    before do
      allow(Input).to receive(:includes).and_return(inputs_including_type_file_attachment_blob)
      allow(inputs_including_type_file_attachment_blob).to receive(:where).and_return(official_files_inputs)
      allow(official_files_inputs).to receive(:order)

      get :official_files
    end

    it { is_expected.to respond_with(200) }

    it 'is expected to include InputType and FileAttachment blob in the query' do
      expect(Input).to have_received(:includes).with(:input_type, file_attachment: [:blob])
    end

    it 'is expected to select the official_files' do
      expect(inputs_including_type_file_attachment_blob).to have_received(:where)
        .with(input_type: { name: 'official_file' })
    end

    it 'is expected to order the official files from newest to oldest' do
      expect(official_files_inputs).to have_received(:order).with(updated_at: :desc)
    end
  end
end

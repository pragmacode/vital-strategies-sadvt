# frozen_string_literal: true

require 'support/shared_examples/setter_methods'
require 'support/shared_contexts/mocking_current_user'

RSpec.shared_context 'when retrieving inputs attributes' do
  let(:input_file) { instance_double('file') }
  let(:inputs) { { '0' => { 'file' => input_file } } }

  before do
    allow(subject).to receive(:retrieve_inputs_attributes).and_return(inputs)
  end

  it 'is expected to have checked if the user is and admin or manager' do
    expect(subject).to have_received(:retrieve_inputs_attributes)
  end
end

RSpec.shared_context 'when mocking methods executions' do |method|
  before do
    allow(subject).to receive(method)
  end

  it 'is expected to have checked if the user is and admin or manager' do
    expect(subject).to have_received(method)
  end
end

RSpec.shared_context 'when blocking pairing inputs path' do
  include_context 'when mocking methods executions', :check_write_access!
end

RSpec.shared_context 'when finding pairing by its ID' do
  let(:pairing) { build(:pairing, id: 42) }

  before do
    allow(subject).to receive(:set_pairing_by_pairing_id) do
      subject.instance_variable_set :@pairing, pairing
    end
  end

  it 'is expected to have found the pairing by its ID' do
    expect(subject).to have_received(:set_pairing_by_pairing_id)
  end
end

RSpec.shared_context 'when mocking pairing set and write access setting' do
  include_context 'when blocking pairing inputs path'
  include_context 'when finding pairing by its ID'
end

RSpec.shared_context 'when allowing access for a given condition' do
  include_context 'when mocking methods executions', :allow_access_for_a_condition
end

RSpec.describe PairingsController, type: :controller do
  before do
    allow(subject).to receive(:authenticate_user!)
  end

  describe 'GET #index' do
    let(:all_pairings) { instance_double('all pairings') }

    before do
      allow(Pairing).to receive(:all).and_return(all_pairings)
      allow(all_pairings).to receive(:order)

      get :index
    end

    it { is_expected.to respond_with(200) }

    it 'is expected to list all Pairings' do
      expect(Pairing).to have_received(:all)
    end

    it 'is expected to order Pairings by creation date' do
      expect(all_pairings).to have_received(:order).with(created_at: :desc)
    end
  end

  describe 'GET #new' do
    include_context 'when blocking pairing inputs path'

    before do
      get :new
    end

    it { is_expected.to respond_with(200) }
  end

  describe 'POST #create' do
    include_context 'when blocking pairing inputs path'
    let(:pairing) { FactoryBot.build :pairing, id: 42 }
    let(:pairing_params) { { kind: pairing.kind } }
    let(:can_save) { nil }

    before do
      allow(subject).to receive(:pairing_params).and_return(pairing_params)
      allow(Pairing).to receive(:new).and_return(pairing)
      allow(pairing).to receive(:save).and_return(can_save)
      allow(pairing.errors).to receive(:full_messages)

      allow(I18n).to receive(:t)

      post :create, params: { pairing: pairing_params }
    end

    context 'when the Pairing was successfully saved' do
      let(:can_save) { true }

      it { is_expected.to respond_with(302) }

      it 'is expected to set a success message' do
        expect(I18n).to have_received(:t).with('pairings.create.success')
      end
    end

    context 'when the Pairing has an error' do
      let(:can_save) { false }

      it { is_expected.to respond_with(200) }

      it 'is expected to retrieve the error messages' do
        expect(pairing.errors).to have_received(:full_messages)
      end
    end
  end

  describe 'GET #show' do
    let(:pairing) { FactoryBot.build :pairing, id: 42 }

    before do
      allow(Pairing).to receive(:find)

      get :show, params: { id: pairing.id }
    end

    it { is_expected.to respond_with(200) }

    it 'is expected to find the Pairing' do
      expect(Pairing).to have_received(:find).with(pairing.id.to_s)
    end
  end

  describe 'GET #new_inputs' do
    include_context 'when mocking pairing set and write access setting'
    let(:pairing) { FactoryBot.build :pairing, id: 42 }
    let(:input_type) { FactoryBot.build(:input_type) }
    let(:inputs) { build_list(:input, 2) }

    before do
      allow(subject).to receive(:build_input_with_type)
      allow(pairing).to receive(:required_input_types).and_return([input_type])

      get :new_inputs, params: { pairing_id: 42 }
    end

    it 'is expected to fetch the required input types' do
      expect(pairing).to have_received(:required_input_types)
    end

    it 'is expected to have build input with given types' do
      expect(subject).to have_received(:build_input_with_type).with(input_type)
    end
  end

  describe 'GET #official_file_input' do
    include_context 'when finding pairing by its ID'
    include_context 'when blocking pairing inputs path'
    let(:pairing) { FactoryBot.build :pairing, id: 42 }
    let(:input_type) { FactoryBot.build(:input_type) }
    let(:inputs) { instance_double('inputs') }

    before do
      allow(InputType).to receive(:find_by).and_return(input_type)
      allow(Input).to receive(:where).and_return(inputs)
      allow(subject).to receive(:fetch_official_file_input)
      get :official_file_input, params: { pairing_id: 42 }
    end

    it 'is expected to have build input with given types' do
      expect(subject).to have_received(:fetch_official_file_input).with(inputs, input_type)
    end

    it 'is expected to have fetched the official input' do
      expect(Input).to have_received(:where).with(input_type: input_type, pairing: pairing)
    end

    it 'is expected to have queried the input type' do
      expect(InputType).to have_received(:find_by).with(name: 'official_file')
    end
  end

  describe 'GET #select_input_columns' do
    include_context 'when finding pairing by its ID'

    before do
      allow(pairing).to receive(:group_columns_by_origin)
      allow(subject).to receive(:can_select_inputs_columns)

      get :select_input_columns, params: { pairing_id: pairing.id }
    end

    it 'is expected to have checked if the user can select input columns' do
      expect(subject).to have_received(:can_select_inputs_columns)
    end

    it 'is expected to have fetched the input columns for the pairing' do
      expect(pairing).to have_received(:group_columns_by_origin)
    end
  end

  describe 'PUT #update' do
    include_context 'when blocking pairing inputs path'
    include_context 'when retrieving inputs attributes'

    let(:pairing_params) { { inputs_attributes: inputs } }
    let(:pairing) { FactoryBot.build :pairing, id: 42 }

    before do
      allow(Pairing).to receive(:find).and_return(pairing)
      allow(subject).to receive(:build_inputs)
      put :update, params: { id: 42, pairing: pairing_params }
    end

    it 'is expected to have set the pairing' do
      expect(Pairing).to have_received(:find).with(pairing.id.to_s)
    end

    it 'is expected to call build_inputs' do
      expect(subject).to have_received(:build_inputs).with(pairing, inputs)
    end
  end

  describe 'PATCH #save_official_file' do
    include_context 'when finding pairing by its ID'
    include_context 'when blocking pairing inputs path'
    include_context 'when retrieving inputs attributes'

    let(:pairing) { FactoryBot.build :pairing, id: 42 }

    before do
      allow(subject).to receive(:upsert_official_base)
      patch :save_official_file, params: { pairing_id: 42 }
    end

    it 'is expected to call build_inputs' do
      expect(subject).to have_received(:upsert_official_base).with(pairing, inputs)
    end
  end

  describe 'PATCH #save_pairing_columns' do
    include_context 'when mocking pairing set and write access setting'
    let(:selected_columns) { %w[col1 col2 col3] }
    let(:pairing) { FactoryBot.build :pairing, id: 42 }

    before do
      allow(subject).to receive(:save_pairing_columns)
      patch :pick_pairing_columns, params: { pairing: { selected_columns: selected_columns }, pairing_id: 42 }
    end

    it 'is expected to have saved pairing columns' do
      expect(subject).to have_received(:save_pairing_columns).with(selected_columns)
    end
  end

  describe 'GET #run' do
    include_context 'when blocking pairing inputs path'
    let(:pairing) { FactoryBot.build :pairing, id: 42 }
    let(:it_is_ready) { nil }

    before do
      allow(subject).to receive(:set_pairing_by_pairing_id) do
        subject.instance_variable_set :@pairing, pairing
      end
      allow(pairing).to receive(:input_validated?).and_return(it_is_ready)
      allow(PairingJob).to receive(:perform_later)
      allow(subject).to receive(:redirect_to).and_call_original

      get :run, params: { pairing_id: pairing.id }
    end

    it 'is expected to have run set_paring_by_pairing_id method' do
      expect(subject).to have_received(:set_pairing_by_pairing_id)
    end

    context 'when the job is ready to be run' do
      let(:it_is_ready) { true }

      it { is_expected.to set_flash[:notice].to(I18n.t('pairings.run.triggered_job')) }

      it 'is expected to have triggered PairingJob' do
        expect(PairingJob).to have_received(:perform_later).with(pairing.id)
      end

      it 'is expected to redirect to the same page' do
        expect(subject).to have_received(:redirect_to).with(root_path)
      end
    end

    context 'when the job is not ready to be run' do
      let(:it_is_ready) { false }

      it { is_expected.to set_flash[:error].to(I18n.t('pairings.run.cannot_trigger_job')) }

      it 'is expected to redirect to the same page' do
        expect(subject).to have_received(:redirect_to).with(pairing_path(pairing))
      end
    end
  end

  describe 'DELETE #destroy' do
    include_context 'when blocking pairing inputs path'
    let(:pairing) { FactoryBot.build :pairing, id: 42 }

    before do
      allow(subject).to receive(:set_pairing_by_id) do
        subject.instance_variable_set :@pairing, pairing
      end
      allow(subject).to receive(:redirect_to).and_call_original
      allow(pairing).to receive(:destroy)

      delete :destroy, params: { id: pairing.id }
    end

    it { is_expected.to respond_with(302) }

    it { is_expected.to set_flash[:notice].to(I18n.t('pairings.destroy.success')) }

    it 'is expected to find the Pairing' do
      expect(subject).to have_received(:set_pairing_by_id)
    end

    it 'is expected to destroy the pairing' do
      expect(pairing).to have_received(:destroy)
    end

    it 'is expected to redirect back to root' do
      expect(subject).to have_received(:redirect_to).with(root_path,
                                                          { notice: 'Pareamento removido com sucesso' })
    end
  end

  describe 'private method' do
    let(:params) { { id: 1, pairing_id: 42 } }

    describe 'pairing_params' do
      returned_value = nil

      let(:permitted) { instance_double('permitted_pairing_attributes') }
      let(:pairing) { instance_double('pairing') }
      let(:params) { instance_double('params') }

      before do
        allow(pairing).to receive(:permit).and_return(permitted)
        allow(params).to receive(:require).and_return(pairing)
        allow(subject).to receive(:params).and_return(params)

        returned_value = subject.send(:pairing_params)
      end

      it 'is expected to return the permitted values' do
        expect(returned_value).to eq(permitted)
      end

      it 'is expected to retrieve the request params' do
        expect(subject).to have_received(:params)
      end

      it 'is expected to require the pairing from params' do
        expect(params).to have_received(:require).with(:pairing)
      end
      # rubocop:disable Layout/LineLength

      it 'is expected to permit the kind param' do
        expect(pairing).to have_received(:permit).with(:kind, :description, { inputs_attributes: %i[file input_type_id] })
      end
      # rubocop:enable Layout/LineLength
    end

    it_behaves_like 'setter methods', Pairing, :set_pairing_by_id
    it_behaves_like 'setter methods', Pairing, :set_pairing_by_pairing_id

    describe 'build_input' do
      let(:pairing) { FactoryBot.build :pairing }
      let(:input_type_id) { 42 }

      before do
        allow(pairing.inputs).to receive(:build)

        subject.send(:build_input, pairing, input_type_id, file)
      end

      context 'when there is a file attached' do
        let(:file) { instance_double('file') }

        it 'is expected for the input to have received :build method once' do
          expect(pairing.inputs).to have_received(:build).with(file: file, input_type_id: input_type_id)
        end
      end

      context 'when there is no file attached' do
        let(:file) { nil }

        it 'is expected to not build the input' do
          expect(pairing.inputs).not_to have_received(:build)
        end
      end
    end

    describe 'success_build' do
      let(:pairing) { FactoryBot.build :pairing, id: 42 }
      let(:inputs) { instance_double('input', length: 1) }

      before do
        allow(subject).to receive(:render)
        allow(pairing).to receive(:inputs).and_return(inputs)
        allow(pairing).to receive(:required_input_types).and_return([{}])
        allow(inputs).to receive(:each)
        allow(InputValidationJob).to receive(:perform_later)

        subject.send(:success_build, pairing)
      end

      it 'is expected for the pairing to have received :inputs method' do
        expect(pairing).to have_received(:inputs).exactly(2).times
      end

      it 'is expected to have run required_input_types' do
        expect(pairing).to have_received(:required_input_types)
      end

      it { is_expected.to set_flash[:notice].to(I18n.t('pairings.update.success')) }

      it 'is expected to render the show view' do
        expect(subject).to have_received(:render).with(:show)
      end

      it 'is expected to schedule the InputValidationJob' do
        expect(InputValidationJob).to have_received(:perform_later).with(pairing.id)
      end
    end

    describe 'failed_build' do
      let(:pairing) { FactoryBot.build :pairing, id: 42 }
      let(:errors) { instance_double(:errors) }
      let(:input) { FactoryBot.build(:input) }
      let(:errors_messages) { instance_double('errors_messages') }

      before do
        allow(subject).to receive(:redirect_to)
        allow(pairing).to receive(:inputs).and_return([input])
        allow(input).to receive(:valid?).and_return(false)
        allow(input.errors).to receive(:messages).and_return(errors_messages)

        subject.send(:failed_build, pairing)
      end

      it { is_expected.to set_flash[:error].to([errors_messages]) }

      it 'is expected to query the inputs' do
        expect(pairing).to have_received(:inputs)
      end

      it 'is expected to check if the input is valid' do
        expect(input).to have_received(:valid?)
      end

      it 'is expected to fetch the input error message' do
        expect(input.errors).to have_received(:messages)
      end

      it 'is expected to redirect to the same page' do
        expect(subject).to have_received(:redirect_to).with(pairing_inputs_path(pairing))
      end
    end

    describe 'build_inputs' do
      let(:pairing) { FactoryBot.build :pairing }
      let(:file) { { 'file' => instance_double('file'), 'input_type_id' => 42 } }
      let(:input) { FactoryBot.build(:input) }
      let(:valid_input) { nil }

      before do
        allow(subject).to receive(:build_input)
        allow(subject).to receive(:failed_build)
        allow(subject).to receive(:success_build)

        allow(pairing.inputs).to receive(:includes).and_return([input])
        allow(input).to receive(:valid?).and_return(valid_input)

        subject.send(:build_inputs, pairing, { '0' => file })
      end

      it 'is expected to call :build_input' do
        expect(subject).to have_received(:build_input).with(pairing, file['input_type_id'], file['file'])
      end

      it 'is expected to query the pairing inputs' do
        expect(pairing.inputs).to have_received(:includes).with(:input_type, file_attachment: :blob)
      end

      it 'is expected to check if the input is valid' do
        expect(input).to have_received(:valid?)
      end

      context 'when there are errors' do
        let(:valid_input) { false }

        it 'is expected to call :failed_build method' do
          expect(subject).to have_received(:failed_build).with(pairing)
        end
      end

      context 'when there are no errors' do
        let(:valid_input) { true }

        it 'is expected to call :success_build method' do
          expect(subject).to have_received(:success_build).with(pairing)
        end
      end
    end

    describe 'upsert_official_base' do
      let(:pairing) { build(:pairing) }
      let(:pairing_input) { build(:input) }
      let(:inputs) { { '0' => { 'file' => 'input_file' } } }

      before do
        subject.instance_variable_set :@pairing, pairing
        allow(pairing_input).to receive(:update)
        allow(subject).to receive(:redirect_to)
        allow(subject).to receive(:build_inputs)
        allow(I18n).to receive(:t).and_return('translation')
        allow(subject).to receive(:pairing_path).and_return('pairing_path')
      end

      [
        [true, :to, :not_to], [false, :not_to, :to]
      ].each do |has_official_base, update_expectation, building_expectation|
        context "when pairing has #{has_official_base ? 'an' : 'no'} official base" do
          let(:official_base_array) { has_official_base ? [pairing_input] : [] }

          before do
            allow(pairing).to receive(:official_base).and_return(official_base_array)
            subject.send(:upsert_official_base, pairing, inputs)
          end

          it 'is expected to have searched for the pairing official bases' do
            expect(pairing).to have_received(:official_base)
          end

          it "is expected #{update_expectation} have updated do official file" do
            expect(pairing_input).send(update_expectation, have_received(:update).with(file: 'input_file'))
          end

          it "is expected #{update_expectation} have rendered show view after update" do
            expect(subject).send(update_expectation, have_received(:redirect_to)
                                                       .with('pairing_path', notice: 'translation'))
          end

          it "is expected #{update_expectation} have retrieved the pairing path" do
            expect(subject).send(update_expectation, have_received(:pairing_path).with(pairing))
          end

          it "is expected #{update_expectation} have rendered a translated message" do
            expect(I18n).send(update_expectation, have_received(:t).with('pairings.official_file_update'))
          end

          it "is expected #{building_expectation} have built the official file input" do
            expect(subject).send(building_expectation, have_received(:build_inputs).with(pairing, inputs))
          end
        end
      end
    end

    describe 'check_write_access!' do
      include_context 'when mocking :current_user call'
      include_context 'when allowing access for a given condition'

      before do
        allow(current_user).to receive(:write_allowed?)

        subject.send(:check_write_access!)
      end

      it 'is expected for current user to have received the :write_allowed? method' do
        expect(current_user).to have_received(:write_allowed?)
      end
    end

    describe 'can_select_inputs_columns' do
      let(:pairing) { build(:pairing) }

      include_context 'when mocking :current_user call'
      include_context 'when allowing access for a given condition'

      before do
        subject.instance_variable_set(:@pairing, pairing)
        allow(current_user).to receive(:can_select_input_columns?)
        subject.send(:can_select_inputs_columns)
      end

      it 'is expected to have checked if the user can select columns' do
        expect(current_user).to have_received(:can_select_input_columns?).with(pairing)
      end
    end

    describe 'build_input_with_type' do
      let(:input_type) { FactoryBot.build(:input_type) }
      let(:pairing) { FactoryBot.build :pairing, id: 42 }

      before do
        controller.instance_variable_set :@pairing, pairing
        allow(pairing.inputs).to receive(:exists?).with(input_type: input_type).and_return(false)
        allow(pairing.inputs).to receive(:build)
        controller.send(:build_input_with_type, input_type)
      end

      it 'is expected to build new inputs' do
        expect(pairing.inputs).to have_received(:build).with(input_type: input_type)
      end

      it 'is expected to check if there is an Input of the given type already created' do
        expect(pairing.inputs).to have_received(:exists?).with(input_type: input_type)
      end
    end

    describe 'redirect_to_root_path' do
      let(:path) { 'root' }

      before do
        allow(subject).to receive(:root_path).and_return(path)
        allow(subject).to receive(:redirect_to)
        subject.send(:redirect_to_root_path)
      end

      it 'is expected to have got the the root path' do
        expect(subject).to have_received(:root_path)
      end

      it 'is expected to have redirected to the root path' do
        expect(subject).to have_received(:redirect_to).with(path)
      end
    end

    describe 'retrieve_inputs_attributes' do
      let(:params) { (1..3).map { |i| { "key_#{i}" => "Value_#{i * (i + 1)}" } } }
      let(:pairing_params) { { inputs_attributes: params } }

      before do
        allow(subject).to receive(:pairing_params).and_return(pairing_params)
      end

      it 'is expected to have returned the pairing attributes' do
        expect(subject.send(:retrieve_inputs_attributes)).to eq(params)
      end
    end

    describe 'fetch_official_file_input' do
      let(:built_input) { build(:input) }
      let(:input_type) { build(:input_type) }
      let(:official_file_array) { build_list(:input, 2) }

      before do
        allow(subject).to receive(:build_input_with_type).and_return(built_input)
      end

      it 'is expected to return a built input when array is empty' do
        expect(subject.send(:fetch_official_file_input, [], input_type)).to eq(built_input)
      end

      it 'is expected to return the first member of the array' do
        expect(subject.send(:fetch_official_file_input, official_file_array, input_type)).to eq(official_file_array[0])
      end
    end

    describe 'save_pairing_columns' do
      let(:column_ids) { %w[1 2 3 4 5 6 7 8 9 10] }
      let(:path) { 'path' }
      let(:pairing) { build(:pairing) }

      before do
        subject.instance_variable_set(:@pairing, pairing)
        allow(subject).to receive(:update_input_columns)
        allow(subject).to receive(:pairing_path).and_return(path)
        allow(subject).to receive(:redirect_to)
        subject.send(:save_pairing_columns, column_ids)
      end

      it 'is expected to have set the other columns as not picked' do
        expect(subject).to have_received(:update_input_columns).with(column_ids)
      end

      it 'is expected to have fetched the pairing path' do
        expect(subject).to have_received(:pairing_path).with(pairing)
      end

      it 'is expected to have redirected the user' do
        expect(subject).to have_received(:redirect_to).with(path)
      end

      it { is_expected.to set_flash[:notice].to(I18n.t('pairings.save_pairing_columns.success')) }
    end
  end

  describe 'update_input_columns' do
    let(:pairing) { build(:pairing) }
    let(:input_columns) { instance_double('input_columns') }
    let(:where_query) { instance_double('where_query') }
    let(:where_not_query) { instance_double('where_not_query') }
    let(:included) { instance_double('included') }

    before do
      subject.instance_variable_set(:@pairing, pairing)
      allow(pairing).to receive(:input_columns).and_return(input_columns)
      allow(input_columns).to receive(:includes).and_return(included)
      allow(included).to receive(:where).and_return(where_query)
      allow(input_columns).to receive(:where).and_return(where_query)
      allow(where_query).to receive(:not).and_return(where_not_query)
      allow(where_query).to receive(:update)
      allow(where_not_query).to receive(:update)
      subject.send(:update_input_columns, [42])
    end

    it 'is expected to have fetched the pairing inputs columns' do
      expect(pairing).to have_received(:input_columns)
    end

    it 'is expected to have included the pairing inputs' do
      expect(input_columns).to have_received(:includes).with([:input])
    end

    it 'is expected to have queried the input columns' do
      expect(included).to have_received(:where).with(id: [42])
    end

    it 'is expected to have queried the opposite group' do
      expect(where_query).to have_received(:not).with(id: [42])
    end

    it 'is expected to have updated the picked columns' do
      expect(where_query).to have_received(:update).with(selected: true)
    end

    it 'is expected not to have updated the non picked column' do
      expect(where_not_query).to have_received(:update).with(selected: false)
    end
  end

  describe 'allow_access_for_a_condition' do
    let(:path) { 'root' }
    let(:translation_key) { 'only_managers_allowed' }
    let(:translated_message) { 'translated message' }

    before do
      allow(subject).to receive(:root_path).and_return(path)
      allow(subject).to receive(:redirect_to)
      allow(I18n).to receive(:t).and_return(translated_message)
    end

    [[true, :not_to], [false, :to]].each do |condition, expectancy|
      context "when condition is '#{condition}'" do
        before do
          subject.send(:allow_access_for_a_condition, condition, translation_key)
        end

        it "is expected #{expectancy} have got the the root path" do
          expect(subject).send(expectancy, have_received(:root_path))
        end

        it { is_expected.send(expectancy, set_flash[:error].to(I18n.t(translation_key, scope: [:pairings]))) }

        it "is expected #{expectancy} have redirected the user" do
          expect(subject).send(expectancy, have_received(:redirect_to).with(path))
        end

        it "is expected #{expectancy} have translated the message" do
          expect(I18n).send(expectancy, have_received(:t).with(translation_key, scope: [:pairings]))
        end
      end
    end
  end
end

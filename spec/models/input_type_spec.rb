# frozen_string_literal: true

require 'rails_helper'

require 'support/shared_examples/validate_presence_and_inclusion'
require 'support/shared_examples/translates_attribute_value'

RSpec.describe InputType, type: :model do
  subject(:input_type) { FactoryBot.build(:input_type) }

  describe 'validation' do
    it_behaves_like 'validates presence and inclusion of', :name, described_class::NAMES
    it { is_expected.to validate_uniqueness_of(:name) }
    it { is_expected.to validate_presence_of(:csv_columns_names) }
  end

  describe 'association' do
    it { is_expected.to have_many(:inputs).dependent(:restrict_with_exception) }
  end

  describe 'method' do
    describe 'human_name' do
      it_behaves_like 'translates attribute value', :name, :human_name
    end
  end
end

# frozen_string_literal: true

require 'aasm/rspec'
require 'support/shared_examples/validate_presence_and_inclusion'
require 'support/shared_examples/translates_attribute_value'

RSpec.describe Pairing, type: :model do
  subject(:pairing) { FactoryBot.build(:pairing, id: 42) }

  describe 'validation' do
    it_behaves_like 'validates presence and inclusion of', :kind, described_class::KINDS
  end

  describe 'description lenght validation' do
    it { is_expected.to validate_length_of(:description).is_at_most(255) }
  end

  describe 'associations' do
    it { is_expected.to have_many(:outputs).dependent(:destroy).class_name('Output') }
    it { is_expected.to have_many(:inputs).dependent(:nullify) }
  end

  it { is_expected.to be_versioned }

  describe 'AASM' do
    it { is_expected.to transition_from(:created).to(:input_validated).on_event(:validate_input) }
    it { is_expected.to transition_from(:input_validated).to(:running).on_event(:run) }
    it { is_expected.to transition_from(:running).to(:done).on_event(:finish) }

    %i[created input_validated running].each do |from|
      it { is_expected.to transition_from(from).to(:errored).on_event(:fail) }
    end
  end

  describe 'callbacks' do
    describe 'before_destroy' do
      describe 'destroy_not_official_inputs' do
        before do
          allow(pairing).to receive(:destroy_not_official_inputs)

          pairing.destroy
        end

        it { is_expected.to have_received(:destroy_not_official_inputs) }
      end
    end
  end

  describe 'method' do
    describe 'human_kind_name' do
      it_behaves_like 'translates attribute value', :kind, :human_kind_name
    end

    describe 'list_files' do
      let(:pairing) { FactoryBot.build :pairing }
      let(:results_set) { instance_double('results') }
      let(:entity_with_file) { instance_double('entity_with_file') }
      let(:inputs_with_types) { instance_double('inputs_and_types') }

      returned = nil

      before do
        [Input, Output].each do |klass|
          allow(klass).to receive(:with_attached_file).and_return(entity_with_file)
        end
        allow(entity_with_file).to receive(:where).and_return(results_set)
        allow(results_set).to receive(:includes).and_return(inputs_with_types)

        returned = pairing.list_inputs
      end

      it 'is expected for input_with_files to have received where' do
        expect(entity_with_file).to have_received(:where).with(pairing: pairing)
      end

      context 'when running list_inputs' do
        it 'is expected for Input to have received files' do
          expect(Input).to have_received(:with_attached_file)
        end

        it 'is expected for return_set to have received includes' do
          expect(results_set).to have_received(:includes).with(:input_type)
        end

        it 'is expected to have returned inputs_with_types' do
          expect(returned).to eq(inputs_with_types)
        end
      end

      context 'when running list_outputs' do
        before do
          returned = pairing.list_outputs
        end

        it 'is expected for Output to have received files' do
          expect(Output).to have_received(:with_attached_file)
        end

        it 'is expected to have returned inputs_with_types' do
          expect(returned).to eq(results_set)
        end
      end
    end

    describe 'list_inputs dependent methods' do
      let(:list_query) { instance_double('list_query') }
      let(:list_inputs) { instance_double('list_inputs') }
      let(:list_not_query) { instance_double('list_not_query') }

      before do
        allow(pairing).to receive(:list_inputs).and_return(list_inputs)
        allow(list_inputs).to receive(:where).and_return(list_query)
        allow(list_query).to receive(:not).and_return(list_not_query)
        allow(list_not_query).to receive(:references)
        allow(list_query).to receive(:references)
      end

      context 'when running :list_required_inputs' do
        before do
          pairing.list_required_inputs
        end

        it 'is expected to have listed inputs' do
          expect(pairing).to have_received(:list_inputs)
        end

        it 'is expected to have queried the list' do
          expect(list_inputs).to have_received(:where)
        end

        it 'is expected to have opposed the query' do
          expect(list_query).to have_received(:not).with('input_types.name' => 'official_file')
        end

        it 'is expected to have referenced input type model' do
          expect(list_not_query).to have_received(:references).with(:input_type)
        end
      end

      context 'when running :official_base' do
        before do
          pairing.official_base
        end

        it 'is expected to have listed inputs' do
          expect(pairing).to have_received(:list_inputs)
        end

        it 'is expected to have queried the list' do
          expect(list_inputs).to have_received(:where).with('input_types.name' => 'official_file')
        end

        it 'is expected to have referenced input type model' do
          expect(list_query).to have_received(:references).with(:input_type)
        end
      end
    end
  end

  describe 'required_input_types' do
    let(:pairing) { FactoryBot.build(:pairing, kind: 'test') }
    let(:kinds_array) { ['sat'] }
    let(:kinds_hash) { { 'test' => kinds_array } }
    let(:input_types) { [InputType.new] * 2 }

    before do
      stub_const('Pairing::REQUIRED_INPUTS', kinds_hash)
      allow(kinds_array).to receive(:map).and_return(input_types)

      pairing.required_input_types
    end

    it 'is expected for kinds array to have received :map' do
      expect(kinds_array).to have_received(:map)
    end

    it 'is expected to have returned input types' do
      expect(pairing.required_input_types).to eq(input_types)
    end
  end

  describe 'official_file?' do
    let(:pairing) { build(:pairing) }
    let(:inputs) { instance_double('pairing_inputs') }
    let(:an_input) { instance_double('official_file_input') }
    let(:response_length) { instance_double(Integer) }
    let(:input_type) { build(:input_type) }

    before do
      allow(InputType).to receive(:find_by).and_return(input_type)
      allow(pairing).to receive(:inputs).and_return(inputs)
      allow(inputs).to receive(:where).and_return(an_input)
      allow(an_input).to receive(:length).and_return(response_length)
      allow(response_length).to receive(:positive?)
      pairing.official_file?
    end

    it 'is expected to have found the input type' do
      expect(InputType).to have_received(:find_by).with(name: 'official_file')
    end

    it 'is expected to have checked the pairing inputs' do
      expect(pairing).to have_received(:inputs)
    end

    it 'is expected to have queried the inputs' do
      expect(inputs).to have_received(:where).with(input_type: input_type)
    end

    it 'is expected to have checked the query length' do
      expect(an_input).to have_received(:length)
    end

    it 'is expected to have checked if the length is positive' do
      expect(response_length).to have_received(:positive?)
    end
  end

  describe 'input_process_is_incomplete?' do
    let(:inputs) { instance_double(ActiveRecord::Associations::CollectionProxy, count: 1) }
    let(:required_input_types) { instance_double(Array, length: described_class::KINDS.length) }

    before do
      allow(pairing).to receive(:inputs).and_return(inputs)
      allow(pairing).to receive(:required_input_types).and_return(required_input_types)
    end

    it 'is expected to check if the input process is incomplete' do
      expect(pairing).to be_input_process_is_incomplete
    end
  end

  describe 'input_columns' do
    let(:joined) { instance_double('joined') }

    before do
      allow(InputColumn).to receive(:joins).and_return(joined)
      allow(joined).to receive(:where)
      pairing.input_columns
    end

    it 'is expected to have joined input and its columns' do
      expect(InputColumn).to have_received(:joins).with(:input)
    end

    it 'is expected to have queried the joined data' do
      expect(joined).to have_received(:where).with('inputs.pairing_id' => pairing.id)
    end
  end

  describe 'group_columns_by_origin' do
    let(:input_columns) { instance_double('input_columns') }
    let(:included_models) { instance_double('included_models') }

    before do
      allow(pairing).to receive(:input_columns).and_return(input_columns)
      allow(input_columns).to receive(:includes).and_return(included_models)
      allow(included_models).to receive(:group_by)
      pairing.group_columns_by_origin
    end

    it 'is expected to have checked the input columns' do
      expect(pairing).to have_received(:input_columns)
    end

    it 'is expected to have included the input and its type' do
      expect(input_columns).to have_received(:includes).with(input: :input_type)
    end

    it 'is expected to have grouped the results' do
      expect(included_models).to have_received(:group_by)
    end
  end

  describe 'selected_input_columns_titles' do
    let(:input_columns) { instance_double(ActiveRecord::Relation) }
    let(:selected_input_columns) { build_list(:input_column, 3) }
    let(:expected_result) { selected_input_columns.pluck(:title).join(';') << ';Score' }

    returned = nil

    before do
      allow(pairing).to receive(:input_columns).and_return(input_columns)
      allow(input_columns).to receive(:where).and_return(selected_input_columns)

      returned = pairing.selected_input_columns_titles
    end

    it 'is expected to return the selected columns titles joined by ";" with "Score" appended' do
      expect(returned).to eq(expected_result)
    end

    it 'is expected to list the pairing input columns' do
      expect(pairing).to have_received(:input_columns)
    end

    it 'is expected to filter out the not selected columns' do
      expect(input_columns).to have_received(:where).with(selected: true)
    end
  end

  describe 'private method' do
    describe 'destroy_not_official_inputs' do
      let(:inputs) { instance_double(ActiveRecord::Relation) }
      let(:inputs_joined_with_types) { instance_double(ActiveRecord::Relation) }
      let(:where) { instance_double(ActiveRecord::QueryMethods::WhereChain) }
      let(:not_official_inputs) { instance_double(ActiveRecord::Relation) }

      before do
        allow(pairing).to receive(:inputs).and_return(inputs)
        allow(inputs).to receive(:joins).and_return(inputs_joined_with_types)
        allow(inputs_joined_with_types).to receive(:where).and_return(where)
        allow(where).to receive(:not).and_return(not_official_inputs)
        allow(not_official_inputs).to receive(:destroy_all)

        pairing.send(:destroy_not_official_inputs)
      end

      it { is_expected.to have_received(:inputs) }

      it 'is expected to join the inputs with its types' do
        expect(inputs).to have_received(:joins).with(:input_type)
      end

      it 'is expected to start a where condition' do
        expect(inputs_joined_with_types).to have_received(:where)
      end

      it 'is expected to select only the inputs that are not official ones' do
        expect(where).to have_received(:not).with(input_type: { name: 'official_file' })
      end

      it 'is expected to detroy all the not official inputs' do
        expect(not_official_inputs).to have_received(:destroy_all)
      end
    end
  end
end

# frozen_string_literal: true

RSpec.describe Output, type: :model do
  subject { FactoryBot.build(:output, pairing: pairing) }

  let(:pairing) { FactoryBot.create(:pairing) }

  it { is_expected.to be_versioned }

  describe 'validation' do
    it { is_expected.to validate_presence_of(:script_name) }
  end

  describe 'associations' do
    it { is_expected.to belong_to(:pairing) }
  end

  it { is_expected.to have_one_attached(:file) }
end

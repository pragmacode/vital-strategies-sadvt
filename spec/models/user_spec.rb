# frozen_string_literal: true

RSpec.describe User, type: :model do
  subject(:user) { FactoryBot.build(:user) }

  describe 'validations' do
    it { is_expected.to allow_values(*User::USER_KINDS).for(:level) }
  end

  describe 'public methods' do
    [[:admin?, 'admin'], [:manager?, 'manager']].each do |method, checked_level|
      describe method do
        before do
          allow(user).to receive(:level_check)
          user.send(method)
        end

        it 'is expected to have received :local_check' do
          expect(user).to have_received(:level_check).with(checked_level)
        end
      end
    end

    describe 'write_allowed?' do
      [['admin', true], ['manager', true], ['visitor', false]].each do |user_level, expected_return|
        describe "when user level is '#{user_level}'" do
          let(:instance) { build(:user, level: user_level) }

          it "is expected to return #{expected_return}" do
            expect(instance.write_allowed?).to eq(expected_return)
          end
        end
      end
    end
  end

  describe 'self.select_user_level' do
    let(:fake_user_kinds) { ['test_kind'] }
    let(:translation) { 'translation' }
    let(:expected_return) { [[translation, fake_user_kinds[0]]] }

    returned = nil

    before do
      stub_const('User::USER_KINDS', fake_user_kinds)
      allow(I18n).to receive(:t).and_return(translation)
      returned = described_class.select_user_level
    end

    it 'is expected to bring the human version of the user kind' do
      expect(I18n).to have_received(:t).with(fake_user_kinds[0], scope: %i[activerecord attributes user levels])
    end

    it 'is expected return the correct result' do
      expect(returned).to eq(expected_return)
    end
  end

  describe 'can_select_input_columns?' do
    let(:pairing) { build(:pairing) }

    before do
      allow(user).to receive(:write_allowed?)
      allow(pairing).to receive(:input_validated?).and_return(true)
      user.can_select_input_columns?(pairing)
    end

    it 'is expected to have received :write_allowed? method' do
      expect(user).to have_received(:write_allowed?)
    end

    it 'is expected to have received :input_validated? method' do
      expect(pairing).to have_received(:input_validated?)
    end
  end

  describe 'private methods' do
    describe 'level_check' do
      let(:user) { build(:user) }
      let(:correct_level) { 'manager' }
      let(:incorrect_level) { 'admin' }

      it 'is expected to return true when given the correct level' do
        expect(user.send(:level_check, correct_level)).to be_truthy
      end

      it 'is expected to return false when given the incorrect level' do
        expect(user.send(:level_check, incorrect_level)).to be_falsey
      end
    end
  end
end

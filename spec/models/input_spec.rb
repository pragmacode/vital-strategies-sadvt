# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Input, type: :model do
  describe 'association' do
    it { is_expected.to belong_to(:pairing) }
    it { is_expected.to belong_to(:input_type) }
    it { is_expected.to have_many(:input_columns).dependent(:destroy) }
  end

  it { is_expected.to be_versioned }

  it { is_expected.to have_one_attached(:file) }

  describe 'private methods' do
    subject(:input) { described_class.new }

    describe 'file_attached' do
      let(:attached) { true }
      let(:file) { instance_double('file') }
      let(:errors) { instance_double('errors') }

      before do
        allow(input).to receive(:file).and_return(file)
        allow(file).to receive(:attached?).and_return(attached)
        allow(input).to receive(:errors).and_return(errors)
        allow(errors).to receive(:add)

        input.send(:file_attached)
      end

      it 'is expected to have fetched the input file' do
        expect(input).to have_received(:file)
      end

      it 'is expected to have query if a file has been attached' do
        expect(input.file).to have_received(:attached?)
      end

      context 'with an attached file' do
        it 'is supposed not to add errors' do
          expect(input).not_to have_received(:errors)
        end
      end

      context 'with no attached file' do
        let(:attached) { false }

        it 'is supposed not to add errors' do
          expect(input.errors).to have_received(:add).with(
            :file,
            anything
          )
        end
      end
    end

    describe 'sanitize_filename' do
      let(:file) { instance_double('file') }
      let(:blob) { instance_double('blob') }
      let(:file_attached) { nil }
      let(:filename) { 'a name with spaces' }
      let(:sanitized_filename) { 'a_name_with_spaces' }

      before do
        allow(input).to receive(:file).and_return(file)

        allow(file).to receive(:blob).and_return(blob)
        allow(file).to receive(:attached?).and_return(file_attached)
        allow(file).to receive(:filename).and_return(filename)

        allow(blob).to receive(:update)

        input.send(:sanitize_filename)
      end

      it 'is expected to check for a file attached' do
        expect(file).to have_received(:attached?)
      end

      it 'is expected to fetch the input file' do
        expect(input).to have_received(:file)
      end

      context 'when there is no file attached' do
        let(:file_attached) { false }

        it 'is expected to not update the file' do
          expect(blob).not_to have_received(:update)
        end
      end

      context 'when there is a file attached' do
        let(:file_attached) { true }

        it 'is expected to update the file to the sanitized_filename' do
          expect(blob).to have_received(:update).with(filename: sanitized_filename)
        end

        it 'is expected to fetch the file blob' do
          expect(file).to have_received(:blob)
        end
      end
    end
  end
end

# frozen_string_literal: true

require 'rails_helper'

RSpec.describe InputColumn, type: :model do
  subject(:input_column) { build(:input_column, input_id: input.id) }

  let(:pairing) { create(:pairing) }
  let(:input_type) { create(:input_type, name: 'samu', csv_columns_names: '-') }
  let(:input) do
    built_instance = build(:input, pairing: pairing, input_type: input_type)
    built_instance.file.attach(
      io: File.open(Rails.root.join('features/support/fixtures/sample_input.csv')),
      filename: 'sample_input.csv'
    )
    built_instance.save
    built_instance
  end

  describe 'associations' do
    it { is_expected.to belong_to(:input) }
  end

  describe 'validations' do
    it { is_expected.to validate_presence_of(:title) }

    describe 'scoped uniqueness' do
      before { create(:input_column, input: input) }

      it { is_expected.to validate_uniqueness_of(:title).scoped_to(:input_id) }
    end
  end

  describe 'callbacks' do
    describe 'before_create' do
      before do
        allow(input_column).to receive(:preselect)

        input_column.run_callbacks :create
      end

      it 'is expected to run preselect' do
        expect(input_column).to have_received(:preselect)
      end
    end
  end

  describe 'private method' do
    describe 'preselect' do
      context 'when the title matches an item in PRESELECTED_COLUMNS' do
        before do
          input_column.title = described_class::PRESELECTED_COLUMNS.sample
          input_column.send(:preselect)
        end

        it 'is expected to set selected to true' do
          expect(input_column.selected).to eq(true)
        end
      end

      context 'when the title does not match an item in PRESELECTED_COLUMNS' do
        before do
          input_column.title = 'not_in_the_preselected'
          input_column.send(:preselect)
        end

        it 'is expected to set selected to false' do
          expect(input_column.selected).to eq(false)
        end
      end
    end
  end
end

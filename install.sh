#!/bin/bash

set -e

# VARIABLES TO BE REPLACED
SSH_USERNAME=root
SSH_SERVER_ADDRESS=skynet.pragmacode.com.br # IP or DNS where the server is accessible through SSH
HTTP_SERVER_ADDRESS=sadvt.pragmacode.com.br # IP or DNS where the server is accessible through HTTP

export SADVT_DATABASE_PASSWORD=sadvtdbpass
# END OF VARIABLES TO BE REPLACED


# Warning
printf "**** ATTENTION ****\n\n\tPlease read the instructions in deploy/README.md and the contents of this script before continuing.\n\n\tOnly continue if you are sure that this is the installation procedure you wish to follow.\n\nPress any key to continue or Ctrl+C to exit.\n\n"
read -n 1 k <&1
printf "Continuing...\n\n"


# Installation
cd deploy

cp staging production
sed -i "s/skynet.pragmacode.com.br/$SSH_SERVER_ADDRESS/g" production
sed -i "s/sadvt.pragmacode.com.br/$HTTP_SERVER_ADDRESS/g" production

ansible-playbook -i production setup.yml -u $SSH_USERNAME -K
ansible-playbook -i production deploy.yml -u $SSH_USERNAME

cd ..

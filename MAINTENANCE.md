# SPVT - Maintenance

There follows a plan on what is the suggested periodicity in months for the different maintenance tasks for this application.

| Task                           | 01 | 02 | 03 | 04 | 05 | 06 | 07 | 08 | 09 | 10 | 11 | 12 |
| ------------------------------ | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- | -- |
| Check production logs          | *  | *  | *  |    |    | *  |    |    | *  |    |    | *  |
| Check server CPU and RAM usage | *  | *  | *  |    |    | *  |    |    | *  |    |    | *  |
| Run the entire pipeline        | *  | *  | *  | *  | *  | *  | *  | *  | *  | *  | *  | *  |
| Update Ruby                    |    |    | *  |    |    | *  |    |    | *  |    |    | *  |
| Update gems                    | *  | *  | *  | *  | *  | *  | *  | *  | *  | *  | *  | *  |
| Update npm packages            | *  | *  | *  | *  | *  | *  | *  | *  | *  | *  | *  | *  |
| Simulate disasters             |    |    |    |    |    | *  |    |    |    |    |    | *  |

After the 12th month follow the same periodicity.

## History

Write below a list in the format `* DD/MM/YYYY - What was done`.

* 02/12/2021 -- The following packages could not be updated
  * mini_portile2 (2.7.1 <~ 2.6.1)
  * rubocop-ast (1.14.0 <~ 1.13.0)
  * spring (3.1.1 <~ 2.1.1)
  * thor (1.1.0 <~ 1.0.1)
  * They could not be updated by `bundle update <package_name>` as the output said:
    * **Bundler attempted to update `<package_name>` but its version stayed the same**

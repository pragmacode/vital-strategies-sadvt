library("naniar")
library("purrr")
library("stringi")
library("readr")
library("dplyr")


args <- commandArgs(trailingOnly=TRUE)
dataset_path <- args[1]
output_directory <- ""
if(length(args) > 1) {
  output_directory <- paste(trimws(args[2]), "/", sep="")
}

read_encoded_input <- function(file_path) {
  encoding = pull(guess_encoding(file_path, n_max=1000)[1,1])
  tryCatch({
    read.csv2(file_path, fileEncoding=encoding, strip.white=T)
  }, error=function(e) {
    read.csv2(file_path, strip.white=T)
  })
}

samu <- read_encoded_input(dataset_path)


samu <- samu[!is.na(samu$CNES),]

samu <- replace_with_na(samu, replace=list(VÍTIMA="",
                                           SEXO="",
                                           RECURSO="",
                                           DESTINO="",
                                           BASE=""))

sex_converter <- function(sex) {
  if(!sex %in% c("M", "Masculino", "masculino", "F", "Feminino", "feminino")) {
    NA
  } else {
    if(sex %in% c("Masculino", "masculino")) {
      "M"
    } else if(sex %in% c("Feminino", "feminino")){
      "F"
    } else {
      sex
    }
  }
}


clean_names <- function(names) {
  names <- gsub("DESCONHECIDO", "", names)
  names <- gsub("-", "", names)
  names <- gsub("[0-9]+", "", names)
  names <- gsub("LAUDO", "", names)
  names <- gsub(" BO ", "", names)
  names <- trimws(names)

  names[grepl("DESCONHECID", names)] <- NA

  names
}


event_date <- as.Date(as.character(samu$ABERTURA), optional=T, tryFormats=c("%d/%m/%Y %H:%M", "%Y-%m-%d %H:%M", "%Y%m%d %H:%M", "%d/%m/%Y", "%Y-%m-%d", "%Y%m%d"))
names <- clean_names(samu$VÍTIMA)


samu$pairing_id <- seq.int(nrow(samu))
samu$pairing_name_first <- sapply(strsplit(names, ' '), function(x) x[1])
samu$pairing_name_last <- sapply(strsplit(names, ' '), function(x) paste(x[c(-1)], collapse=" "))
samu$pairing_sex <- unlist(map(samu$SEXO, sex_converter))
samu$pairing_birth_date_day <- NA
samu$pairing_birth_date_month <- NA
samu$pairing_birth_date_year <- NA
samu$pairing_death_date_day <- NA
samu$pairing_death_date_month <- NA
samu$pairing_death_date_year <- NA
samu$pairing_event_date_day <- format(event_date, format="%d")
samu$pairing_event_date_month <- format(event_date, format="%m")
samu$pairing_event_date_year <- format(event_date, format="%Y")
samu$pairing_cnes <- as.integer(samu$CNES)

if("DOC" %in% colnames(samu)) { # TODO: replace DOC by the correct doc col name
  samu$pairing_doc <- gsub("[^0-9]", "", samu$DOC)
} else {
  samu$pairing_doc <- NA
}


write.csv2(samu, paste(output_directory, "SAMU_norm.csv", sep=""), fileEncoding="UTF-8")

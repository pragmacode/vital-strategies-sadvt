# Vital Strategies SADAT

[![pipeline](https://gitlab.com/pragmacode/vital-strategies-sadvt/badges/master/pipeline.svg)](https://gitlab.com/pragmacode/vital-strategies-sadvt/-/commits/master)

[![cobertura de testes](https://gitlab.com/pragmacode/vital-strategies-sadvt/badges/master/coverage.svg)](https://gitlab.com/pragmacode/vital-strategies-sadvt/-/commits/master)

O Sistema de Pareamento de dados sobre Violência no Trânsito é uma aplicação que visa automatizar o processo de mapreamento
de bases de dados entre o SAT (Sistema de Informações de Acidentes de Trânsito), da CET/SMT, DO InfoSiga, do Governo do Estado de São Paulo, e da Secretaria Municipal de Saúde (SMS). O objetivo é munir o grupo Intersecretarial de Trabalho responsável pelo Pareamento de Dados de uma ferramenta que agilize o trabalho de integração das bases de dados. O projeto visa auxiliar na avaliação das mortes e lesões no trânsito como problema de saúde pública.


## Technical Aspects

* Ruby version
* On how to get started developing, check the [HACKING](HACKING.md) file
* On how to keep the application up and running, check the [MAINTENANCE](MAINTENANCE.md) file
* On how to recover from disasters, check the [DISASTER_RECOVERY](DISASTER_RECOVERY.md) file

## License

* Check the [COPYING](COPYING) file

# Hacking SADVT Documentation

## System dependencies

* [LaTeX](https://www.latex-project.org/)
  - Check the recommended installation for you operating system
* [latexmk](https://mg.readthedocs.io/latexmk.html)

## Running the scripts

```
latexmk -pdf main.tex
```

## Specifications

* Language: Brazilian Portuguese

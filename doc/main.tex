\documentclass[paper=a4, fontsize=11pt]{scrartcl}
\usepackage[T1]{fontenc}
\usepackage{fourier}

\usepackage[brazil]{babel}                              % English language/hyphenation
\usepackage[protrusion=true,expansion=true]{microtype}
\usepackage{amsmath,amsfonts,amsthm} % Math packages
\usepackage[pdftex]{graphicx}
\usepackage[hyphens]{url}
\usepackage{hyperref}
\usepackage{tabu}
\usepackage{tabularx}

% Configure referencing styles
\hypersetup{
  colorlinks=true,
  linkcolor=blue,
  filecolor=magenta,
  urlcolor=blue
}
\urlstyle{same}

%%% Custom sectioning
\usepackage{sectsty}
\allsectionsfont{\centering \normalfont\scshape}


%%% Custom headers/footers (fancyhdr package)
\usepackage{fancyhdr}
\pagestyle{fancyplain}
\fancyhead{}                      % No page header
\fancyfoot[L]{}                      % Empty
\fancyfoot[C]{}                      % Empty
\fancyfoot[R]{\thepage}                  % Pagenumbering
\renewcommand{\headrulewidth}{0pt}      % Remove header underlines
\renewcommand{\footrulewidth}{0pt}        % Remove footer underlines
\setlength{\headheight}{13.6pt}


%%% Equation and float numbering
\numberwithin{equation}{section}    % Equationnumbering: section.eq#
\numberwithin{figure}{section}      % Figurenumbering: section.fig#
\numberwithin{table}{section}        % Tablenumbering: section.tab#


%%% Maketitle metadata
\newcommand{\horrule}[1]{\rule{\linewidth}{#1}}   % Horizontal rule
\newcommand{\columnName}[1]{$#1$}

\title{
  %\vspace{-1in}
  \usefont{OT1}{bch}{b}{n}
  \huge SPVT - Instruções de configuração, uso e desenvolvimento\\
  \horrule{2pt} \\[0.5cm]
}
\author{
  \normalfont \normalsize
  \today
}
\date{}

%%% Begin document
\begin{document}

\maketitle

O documento que abaixo se apresenta tem como objetivo principal a apresentação de métodos de como usar, explorar e
desenvolver a aplicação. Quem precisar de mais informações,
pode buscá-las no
\href{https://gitlab.com/pragmacode/vital-strategies-sadvt/-/blob/master/README.md}{\texttt{README.md}}.

\section{Instalação}

\subsection{Requisitos do servidor}

Os requisitos de software do servidor para que o sistema rode sem problemas são os seguintes:
\begin{itemize}
  \item Instalação Ubuntu 20.04 ou compatível -- Veja o \href{https://ubuntu.com/}{link} para instruções de instalação
  \item Python 3 -- é a base para o ansible. Para saber como instalá-lo, acesse
  \href{https://www.python.org/}{sua página principal}
  \item usuário com permissão de sudo -- este usuário deve poder usar docker
  \item acesso à internet para download das imagens de container\footnote{Após o download das imagens, o acesso à
  internet não é mais necessário. Isso garante que a aplicação não dependerá durante a sua execução de nenhuma outra
  biblioteca ou serviço externo que pode estar indisponível quando funcionando no ambiente de intranet.}
  \item acessível via SSH
\end{itemize}

\begin{center}
  \begin{tabular}{|c|c|c|}
    \hline
    \multicolumn{3}{|c|}{Requisitos de hardware} \\ \hline
    Componente & Mínimo & Recomendável \\ \hline
    RAM & 8 GB & 32 GB \\
    CPU & com 2 núcleos & com 8 núcleos \\
    Armazenamento & HD com 200 GB & SSD com 200 GB \\ \hline
  \end{tabular}
\end{center}

\subsection{Requisitos da máquina que executará a instalação}

Os requisitos necessários à maquina são:
\begin{itemize}
  \item acesso via rede ao servidor
  \item credenciais para acesso via SSH
  \item usuário no servidor com permissão de sudo
  \item instalação atualizada do Ansible \footnote{trata-se de uma ferramenta de gerenciamento de infraestrutura via SSH
  que automatiza muitas das tarefas já processadas pelo docker}
  \item ter uma cópia do repositório de código\footnote{Encontrado em \url{https://gitlab.com/pragmacode/vital-strategies-sadvt}.}
\end{itemize}

\subsection{Preparação}

Para preparar a instalação do sistema é necessário a criação de um arquivo descrevendo as informações de acesso ao servidor
(\textit{inventory}) seguindo o guia \href{https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html}{desta página}.
Um modelo de documento que se espera, pode ser encontrado no arquivo \href{https://gitlab.com/pragmacode/vital-strategies-sadvt/-/blob/master/deploy/deploy.yml}{\texttt{deploy/staging}}

\subsection{Execução}

Antes de iniciar, deve-se averiguar se as seguintes variáveis de ambiente estão definidas na máquina que executará a instalação:

\begin{description}
  \item[$\texttt{SADVT\_DATABASE\_PASSWORD}$] deve ser igual a $\texttt{POSTGRES\_PASSWORD}$, a senha do banco de dados
\end{description}

A execução segue os passos abaixo descritos que devem ser executados na pasta \texttt{/deploy} dentro da máquina local e não do servidor.

\begin{itemize}
  \item Instalar as dependências do sistema.
  \begin{itemize}
    \item \texttt{ansible-playbook -i <staging> setup.yml -u root}
    \item Colocar o arquivo resultante da preparação no lugar de \texttt{<staging>}
    \item Se você tiver privilégios de sudo, você pode usar a opção \texttt{-K} ao invés de
    \texttt{-u root} no referido comando
    \item Não é necessário executar este passo específico mais de uma vez.
  \end{itemize}
  \item A cada novo lançamento, rodar o script \texttt{ansible-playbook -i staging deploy.yml}.
  \begin{itemize}
    \item Este script é o responsável por iniciar a aplicação e garantir que a versão mais recente está sendo executada
  \end{itemize}
\end{itemize}

Ao fim desse processo, a aplicação deve estar acessível via browser no endereço do servidor e na porta padrão do protocolo HTTP\footnote{Porta 80}.

Mais informações sobre a execução da instalação podem ser encontradas no arquivo \href{https://gitlab.com/pragmacode/vital-strategies-sadvt/-/blob/master/deploy/README.md}{\texttt{deploy/README.md}}.

\subsection{Criação do primeiro usuário}

Para criar o primeiro usuário, é necessário que os passos de execução tenha dado certo, em
primeiro lugar. Mas tendo isso garantido, é necessário rodar o comando que abre um shell do
rails $$\texttt{sudo docker exec -it sadvt-webserver rails c}$$ Depois disso, podemos criar um
usuário manualmente com este comando. $$\texttt{User.create(email: 'testando@teste.com', password: 'teste123', level: 'admin')}$$

Depois disso, terá o primeiro usuário de que se precisa.

O modelo dos usuários possui o seguinte campo para controlar o nivel de acesso: $\texttt{level}$. Com isso, o usuário pode estar
em um dos 3 níveis: \textbf{administrador}\footnote{a nível do código $\texttt{admin}$}; \textbf{gerente}\footnote{a nível do código $\texttt{manager}$}; ou \textbf{visitante}\footnote{a nível do código $\texttt{visitor}$}. Abaixo, pode-se conferir as
autorizações relacionadas a cada nível. Autorização de leitura permite ao usuário contemplar os dados resultantes das
interações de usuários com os pareamentos. Autorização de escrita permite que o usuário crie, edite dados relativos, e
que execute pareamentos

\begin{center}
  \begin{tabular}{|c|c|c|}
    \hline
    \multicolumn{3}{|c|}{Permissões de usuário} \\ \hline
    Nível do usuário & Leitura & Escrita \\ \hline
    Visitante & Possui & Não Possui \\
    Gerente & Possui & Possui \\
    Administrador & Possui & Possui \\ \hline
  \end{tabular}
\end{center}

Uma coisa importante pra levar em conta é que apenas o usuário com permissões de administrador tem a capacidade de
gerenciar outros usuários e suas permissões.

\section{Uso}

\subsection{Requisitos}

Os únicos pré-requisitos de uso do sistema são ter um navegador Firefox\footnote{versão 82 e posteriores} ou
Chrome\footnote{versão 87 e posteriores} (derivados e semelhantes também valem) e acesso via rede ao servidor.

\subsection{Criação de usuário}

Quem pode criar usuários via interface são usuários com privilégio de admin. Para criar um, realize o seguinte
procedimento:
\begin{itemize}
  \item Na página principal, deve-se clicar no link 'Administração' no canto superior direito da tela.
  \item Se fizer isso, você deve estar vendo a mensagem 'Listando Usuário' e um botão com ícone de sinal de adição.
  \item Clicar lá te leva pra uma página onde é possível adicionar um usuário.
\end{itemize}


\subsection{Criação de pareamento}

\begin{itemize}
  \item Enquanto na página principal, clique no botão 'Novo'.
  \item Isto fará que você seja redirecionado para a tela onde você escolhe o tipo de pareamento que deseja criar.
  \item Depois da escolher, clique em 'Criar', e então inclua os arquivos com as bases de dados requeridas.
  \item Clique em 'Salvar bases de dados' e então será redirecionado pra página do pareamento
  \item A cada entrada validada, os campos presentes no arquivo serão salvos de forma que você possa decidir se eles
  estarão nas versões filtradas dos data sets resultantes.
  \item Depois que as entradas são salvas e validadas, os campos pré-selecionados (todos os campos são selecionados por
  padrão) podem ser visualizados ao clicar no texto `Ver Seleção'. Isso vai abrir um espaço com todos os campos
  selecionados agrupados por arquivo de entrada.
  \item Para que você possa decidir que campos vai querer na versão filtrada, é necessário clicar no botão `Definir as
  colunas escolhidas'. Se você não conseguir ver onde está esse botão (localizado acima dos campos agrupados), pode
  significar que você não tem permissão de modificar os campos, ou que essa funcionalidade esteja bloqueada nesta ocasião.
  \item Na página de seleção de colunas, você verá um conteúdo parecido com o que viu ao clicar em `Ver seleção', mas
  com muitas caixas de seleção. Depois que terminar de marcar as colunas desejadas para a versão filtrada, basta clicar
  em `Salvar colunas escolhidas'. Este processo pode ser repetido indefinidamente DESDE QUE as entradas estejam
  validadas e que o pareamento não tenha sido processado. Depois disso, qualquer escolha que tenha sido feita é definitiva.
  \item Além das condições citadas acima, é impossível alterar as colunas selecionadas se você for um usuário visitante,
  (em outras palavras, $\mathbf{your\_user.level == 'visitor'}$). Usuários com permissão administrativa e de gerencia podem
  realizar esta operação.
  \item Processando o pareamento
  \begin{itemize}
    \item Com as entradas do pareamento validadas, pode-se clicar no botão 'Iniciar Script'
    \item Atualize a página com alguma frequência até que o trabalho conte como concluído.
    \item Ao final da sequência de processos, o pareamento está concluído ou em falha, mas com certeza haverá alguma
    saída padrão a analisar.
  \end{itemize}
  \item Após o processamento, você pode incluir uma base de dados oficial (pós-verificação do GT). De maneira geral,
  pode-se dizer que trata de um complemento aos dados resultantes feito pelo técnico responsável pela análise.
\end{itemize}

\subsection{Download de resultados}

Uma vez que o processamento da sequência de scripts esteja em curso ou terminada, haverão saídas para baixar. Para
acessá-las, basta clicar no botão com o nome da saída desejada e fazer o seu download.

\section{Formatos de entrada esperados}

Para que os scripts funcionem de forma correta, é necessário que as entradas sejam arquivos CSV \textbf{válidos}, que usem o
caractere ``;'' (ponto e vírgula) como separador de colunas, usem o caractere ``"'' (aspa) para delimitar os textos e preferencialmente
utilizem a codificação UTF-8\footnote{Caso o arquivo esteja codificado em outro formato, o sistema tentará detectar a formatação,
 mas não há garantia do funcionamento.}. O que os confere tal
validade, além do formato, é a presença de determinadas colunas. Quais colunas são necessárias para cada tipo de entrada distinta
serão mencionadas nas seguintes subseções. No ato de inserção das dependências, o sistema confere se as colunas requeridas estão
presentes. Se todos os arquivos contiverem os campos necessários por conta de seu tipo de entrada, o pareamento é validado, e o
usuário\footnote{com os níveis corretos de autorização} torna-se capaz de selecionar as colunas das entradas e realizar o pareamento.

\subsection{SAT - Veículos}

\begin{description}
	\item[\texttt{\columnName{id\_acidente}}] - Código de identificação de um referido acidente;
	\item[\texttt{\columnName{id\_veiculo}}] - Código de identificação de um veículo envolvido no referido acidente.
\end{description}

\subsection{SAT - Vítimas}

\begin{description}
    \item[\texttt{\columnName{id\_acidente}}] - ID de um referido acidente;
    \item[\texttt{\columnName{id\_veiculo}}] - ID de um veículo envolvido no referido acidente;
    \item[\texttt{\columnName{nome}}] - Nome da vítima do acidente;
    \item[\texttt{\columnName{sexo}}] - Sexo da vítima do acidente;
    \item[\texttt{\columnName{data\_nascimento2}}] - Data de nascimento da vítima do acidente;
    \item[\texttt{\columnName{data\_obito}}] - No caso de fatalidade, tal dado indica a data que veio a óbito.
\end{description}

\subsection{SAT - Acidentes}

\begin{description}
  \item[\texttt{\columnName{id\_acidente}}] - ID de um referido acidente;
  \item[\texttt{\columnName{data}}] - Data em que aconteceu o referido acidente.
\end{description}

\subsection{SIM}

\begin{description}
    \item[\texttt{\columnName{NOME}}] - Nome da pessoa falecida;
	\item[\texttt{\columnName{SEXO}}] - Sigla do sexo da pessoa falecida;
    \item[\texttt{\columnName{DTNASC}}] - Representa a data de nascimento;
    \item[\texttt{\columnName{DTOBITO}}] - Representa a data de falecimento.
\end{description}

\subsection{SIH}

\begin{description}
    \item[\texttt{\columnName{NOME}}] - Nome da pessoa internada;
    \item[\texttt{\columnName{P\_SEXO}}] - Sexo do paciente;
    \item[\texttt{\columnName{P\_DATN}}] - Data de nascimento;
    \item[\texttt{\columnName{DT\_INTERNA}}] - Data de internação;
    \item[\texttt{\columnName{CNES}}] - Código nacional de estabelecimentos de saúde;
    \item[\texttt{\columnName{P\_DOC}}] - Indica o tipo de documento do paciente, que podem ser: (1) pis/pasep; (2) RG; (3) Certidão Nascimento; (4) CPF; (5) ignorado ou não informado;
    \item[\texttt{\columnName{P\_NDOC}}] - Indica o número do documento que foi selecionado no campo anterior.
\end{description}

\subsection{SAMU}

\begin{description}
    \item[\texttt{\columnName{SEXO}}] - Sexo da vítima do acidente levada na viatura do SAMU;
    \item[\texttt{\columnName{VÍTIMA}}] - Nome da vítima do acidente;
    \item[\texttt{\columnName{ABERTURA}}] - Horário de abertura do chamado no SAMU;
    \item[\texttt{\columnName{CNES}}] - Código nacional de estabelecimentos de saúde.
\end{description}

%\subsection{InfoSIGA}
%\begin{description}
    % ainda precisa definir para colocar as informações aqui.
%\end{description}


\subsection{SIM com Documento de Identificação do falecido}

\begin{description}
    \item[\texttt{\columnName{NOME}}] - Nome da pessoa falecida;
	\item[\texttt{\columnName{SEXO}}] - Sigla do sexo da pessoa falecida;
    \item[\texttt{\columnName{DTNASC}}] - Representa a data de nascimento;
    \item[\texttt{\columnName{DTOBITO}}] - Representa a data de falecimento;
    \item[\texttt{\columnName{DOC}}] - Representa o documento de identificação do falecido. % precisa alterar quando descobrir o verdadeiro nome do campo
\end{description}

\subsection{SAMU com Documento de Identificação do paciente}

\begin{description}
    \item[\texttt{\columnName{SEXO}}] - Sexo da vítima do acidente levada na viatura do SAMU;
    \item[\texttt{\columnName{VÍTIMA}}] - Nome da vítima do acidente;
    \item[\texttt{\columnName{ABERTURA}}] - Horário de abertura do chamado no SAMU;
    \item[\texttt{\columnName{CNES}}] - Código nacional de estabelecimentos de saúde;
  \item[\texttt{\columnName{DOC}}] - Representa o documento de identificação do paciente. % precisa alterar quando descobrir o verdadeiro nome do campo % precisa alterar quando descobrir o verdadeiro nome do campo
\end{description}


\section{Formatação das saídas}

O processo de pareamento produzirá sempre dois arquivos de saída principais. Um terminado em ``\_alta\_probabilidade.csv'' e
outro terminado em ``\_menor\_probabilidade.csv''. O primeiro contém os resultados de pareamento para os pares classificados
como altamente prováveis. O segundo contém pares que não foram avaliados com probabilidade de correção alta o bastante
para fazer parte do primeiro conjunto, mas também não tem probabilidade baixa o suficiente para serem descartados. Os
limiares de probabilidade definidos nos scripts são: probabilidades menores que $0.5$ são pares descartados; probabilidades
a partir $0.5$ e menores que $0.7$ são pares prováveis mas com menor certeza; e probabilidades de $0.7$ em diante são os
pares considerados de alta probabilidade. Essas probabilidades podem ser conferidas no campo ``Score''

Além desses arquivos principais, também são disponibilizados para download arquivos intermediários que possibilitam ao
usuário avançado verificar aspectos do processo de pareamento. De acordo com o seu padrão de nome, são:

\begin{itemize}
  \item ``(...).txt'' - saída em texto puro da execução dos scripts
  \item ``(...)\_normalizados.csv'' - arquivo de entrada original acrescido de colunas normalizadas que serão utilizadas para o pareamento
  \item ``Registros\_de\_pares\_(...).csv'' - lista dos pares encontrados contendo sua probabilidade e classificação\footnote{Mais informações sobre a probabilidade e classificação estão contidas na documentação oficial da ferramenta de pareamento utilizada \url{https://recordlinkage.readthedocs.io/en/latest/index.html}}
  \item ``\_colunas\_selecionadas.csv'' - são versões reduzidas dos arquivos principais (ou seja, aqueles que terminam em ``\_alta\_probabilidade.csv'' e ``\_menor\_probabilidade.csv'')
\end{itemize}

Como dito anteriormente, para o pareamento são criadas colunas com informação normalizada que será utilizada para o pareamento. Essas são:

\begin{itemize}
  \item ``pairing\_id'' - Identificador único gerado;
  \item ``pairing\_name'' - Nome da vítima;
  \item ``pairing\_sex'' - Sexo da vítima;
  \item ``pairing\_birth\_date'' - Data de nascimento da vítima;
  \item ``pairing\_death\_date'' - Data de óbito da vítima;
  \item ``pairing\_event\_date'' - Data da ocorrência;
  \item ``pairing\_cnes'' - Código do cadastro nacional de estabelecimentos de saúde;
  \item ``pairing\_doc'' - Documento de identificação da vítima.
\end{itemize}

Em alguns dos arquivos de saída essas colunas podem ter um sufixo, por exemplo ``.SAT\_norm'', indicando qual foi a base de dados que a originou.

\section{Desenvolvimento}

Nessa seção serão dadas indicações sobre como realizar algumas tarefas de maior interesse no momento da escrita desse documento. Dessa forma, o conteúdo a seguir não tem o objetivo de ser completo ou substituir o conhecimento de um profissional qualificado para desenvolvimento com as tecnologias envolvidas no projeto.

Mais informações sobre como começar a desenvolver podem ser encontradas no arquivo \href{https://gitlab.com/pragmacode/vital-strategies-sadvt/-/blob/master/HACKING.md}{\texttt{HACKING.md}}.

\subsection{Execução dos scripts via Docker}

No caso de não quiser ou não puder instalar as dependências em sua máquina e tiver Docker instalado, você pode utilizá-lo
para rodar os scripts R. Dado que haja um diretório \texttt{/inputs} provido de todas as bases necessárias para um dito
pareamento, e que haja também um diretório \texttt{/outputs} onde os resultados serão salvos. Dentro do diretório raiz,
rode os seguintes comandos.
\begin{enumerate}
  \item \texttt{docker build -t sadvt .}
  \begin{itemize}
    \item Constrói a imagem com a infraestrutura necessária pra rodar os scripts
  \end{itemize}
  \item \texttt{docker run -it --rm -v \$(pwd)/inputs:lib/pairing/inputs -v \$(pwd)/outputs:lib/pairing/outputs --entrypoint /bin/bash sadvt}
  \begin{itemize}
    \item Este comando cria um container que no qual podemos rodar os comandos de pareamento
  \end{itemize}
  \item \texttt{cd lib/pairing} para acessar a pasta onde os comandos serão executados
\end{enumerate}

\subsubsection{Rodando os scripts}

Depois de mostrar como se faz para rodar o script no docker, é bom saber quais deles podem ser executados e como fazê-lo.
Para isso temos essa sessão. Para que o script rode perfeitamente, é necessário que sejam passados os caminhos para os
arquivos de entrada na ordem devida a ser apresentada abaixo. O último argumento é o diretório para os arquivos de saída.

Mais informações podem ser encontradas no arquivo \href{https://gitlab.com/pragmacode/vital-strategies-sadvt/-/blob/master/lib/pairing/HACKING.md}{\texttt{lib/pairing/HACKING.md}}.

\paragraph{Explicando as entradas}

Para que o leitor não se perca em meio de um monte de acrônimos, nem o documento fique demasiadamente repetido, guardou
-se uma parte da documentação para explicar o que significa cada um dos termos utilizados.
\begin{description}
  \item[\texttt{VEHICLES CSV PATH}] - Caminho para o arquivo CSV com os dados de veículos;
  \item[\texttt{VICTMS CSV PATH}] - Caminho para o arquivo CSV com os dados de vítimas;
  \item[\texttt{ACCIDENTS CSV PATH}] - Caminho para o arquivo CSV com os dados de acidentes;
  \item[\texttt{SIM CSV PATH}] - Caminho para o arquivo da base de dados SIM;
  \item[\texttt{SIH CSV PATH}] - Caminho para o arquivo da base de dados SIH;
  \item[\texttt{SAMU CSV PATH}] - Caminho para o arquivo da base de dados SAMU;
  \item[\texttt{NORMALIZED DATASET CSV PATH}] - Caminho para o arquivo de saída de um dos scripts de normalização (com nomes na forma \textbf{normalize\_*.R});
  \item[\texttt{PAIRS CSV PATH}] - Caminho para o arquivo de saída do script \textbf{pair.R};
  \item[\texttt{LINKED CSV PATH}] - Caminho para o arquivo que preserva todas as linhas do primeiro data set mesclando nelas os dados do segundo data set de acordo com pares classificados como verdadeiros;
  \item[\texttt{PROBABLE CSV PATH}] - Caminho para o arquivo para revisão manual que preserva todas as linhas do primeiro data set mesclando nelas os dados do segundo data set de acordo com pares classificados como prováveis;
  \item[\texttt{INPUT COLUMNS}] - Série de colunas selecionadas pelo usuário para serem exibidas na versão filtrada dos resultados finais;
  \item[\texttt{OUTPUT DIRECTORY}] - Caminho para o diretório de saída.
\end{description}

\begin{itemize}
  \item $\texttt{Rscript normalize\_sat.R <1> <2> <3> <4>}$
  \begin{itemize}
    \item Entradas
    \begin{enumerate}
      \item \texttt{VEHICLES CSV PATH}
      \item \texttt{VICTMS CSV PATH}
      \item \texttt{ACCIDENTS CSV PATH}
      \item \texttt{OUTPUT DIRECTORY}
    \end{enumerate}
    \item Saídas
    \begin{itemize}
      \item \textbf{SAT\_norm.csv} Arquivo com as entradas dos data sets combinados e colunas adicionais com dados normalizados para o processo de pareamento.
    \end{itemize}
  \end{itemize}
  \item $\texttt{Rscript normalize\_sih.R <1> <2>}$
  \begin{itemize}
    \item Entradas
    \begin{enumerate}
      \item \texttt{SIH CSV PATH}
      \item \texttt{OUTPUT DIRECTORY}
    \end{enumerate}
    \item Saídas
    \begin{itemize}
      \item \textbf{SIH\_norm.csv} Arquivo com as entradas dos data sets combinados e colunas adicionais com dados normalizados para o processo de pareamento.
    \end{itemize}
  \end{itemize}
  \item $\texttt{Rscript normalize\_sim.R <1> <2>}$
  \begin{itemize}
    \item Entradas
    \begin{enumerate}
      \item \texttt{SIM CSV PATH}
      \item \texttt{OUTPUT DIRECTORY}
    \end{enumerate}
    \item Saídas
    \begin{itemize}
      \item \textbf{SIM\_norm.csv} Arquivo com as entradas dos data sets combinados e colunas adicionais com dados normalizados para o processo de pareamento.
    \end{itemize}
  \end{itemize}
  \item $\texttt{Rscript normalize\_samu.R <1> <2>}$
  \begin{itemize}
    \item Entradas
    \begin{enumerate}
      \item \texttt{SAMU CSV PATH}
      \item \texttt{OUTPUT DIRECTORY}
    \end{enumerate}
    \item Saídas
    \begin{itemize}
      \item \textbf{SAMU\_norm.csv} Arquivo com as entradas dos data sets combinados e colunas adicionais com dados normalizados para o processo de pareamento.
    \end{itemize}
  \end{itemize}
  \item $\texttt{Rscript normalize\_infosiga.R <1> <2>}$
  \begin{itemize}
    \item Entradas
    \begin{enumerate}
      \item \texttt{InfoSIGA CSV PATH}
      \item \texttt{OUTPUT DIRECTORY}
    \end{enumerate}
    \item Saídas
    \begin{itemize}
      \item \textbf{InfoSIGA\_norm.csv} Arquivo com as entradas dos data sets combinados e colunas adicionais com dados normalizados para o processo de pareamento.
    \end{itemize}
  \end{itemize}
  \item $\texttt{Rscript pair.R <1> <2> <3>}$
  \begin{itemize}
    \item Entradas
    \begin{enumerate}
      \item \texttt{NORMALIZED DATASET CSV PATH}
      \item \texttt{NORMALIZED DATASET CSV PATH}
      \item \texttt{OUTPUT DIRECTORY}
    \end{enumerate}
    \item Saídas
    \begin{itemize}
      \item \textbf{<DATASET 1>\_<DATASET 2>\_pairs.csv} arquivo com os pares encontrados e sua classificação
    \end{itemize}
  \end{itemize}
  \item $\texttt{Rscript merge\_by\_pairing.R <1> <2> <3> <4>}$
  \begin{itemize}
    \item Entradas
    \begin{enumerate}
      \item \texttt{NORMALIZED DATASET CSV PATH}
      \item \texttt{NORMALIZED DATASET CSV PATH}
      \item \texttt{PAIRS CSV PATH}
      \item \texttt{OUTPUT DIRECTORY}
    \end{enumerate}
    \item Saídas
    \begin{itemize}
      \item \textbf{<DATASET 1>\_<DATASET 2>\_linked\_merged.csv} arquivo que preserva todas as linhas do primeiro data set mesclando nelas os dados do segundo data set de acordo com pares classificados como verdadeiros
      \item \textbf{<DATASET 1>\_<DATASET 2>\_probable\_merged.csv} arquivo para revisão manual que preserva todas as linhas do primeiro data set mesclando nelas os dados do segundo data set de acordo com pares classificados como prováveis
    \end{itemize}
  \end{itemize}
  \item $\texttt{Rscript normalize\_paired.R <1> <2>}$
  \begin{itemize}
    \item Entradas
    \begin{enumerate}
      \item \texttt{PAIRED CSV PATH}
      \item \texttt{OUTPUT DIRECTORY}
    \end{enumerate}
    \item Saídas
    \begin{itemize}
      \item \textbf{<PAIRED>\_norm.csv} Arquivo com os dados normalizados para um novo processo de pareamento.
    \end{itemize}
  \end{itemize}
  \item $\texttt{Rscript filter\_columns.R <1> <2> <3> <4>}$
  \begin{itemize}
    \item Entradas
    \begin{enumerate}
      \item \texttt{LINKED CSV PATH}
      \item \texttt{PROBABLE CSV PATH}
      \item \texttt{OUTPUT DIRECTORY}
      \item \texttt{INPUT COLUMNS}
    \end{enumerate}
    \item Saídas
    \begin{itemize}
      \item \textbf{<LINKED CSV>\_filtered.csv} Versão filtrada do arquivo de pares mesclados e ligados.
      \item \textbf{<PROBABLE CSV>\_filtered.csv} Versão filtrada do arquivo de pares mesclados para revisão.
    \end{itemize}
  \end{itemize}
  \item $\texttt{Rscript merge\_by\_doc.R <1> <2> <3>}$
   
  OBS:(Nesse caso, os documentos são mesclados pelo documento.)
  \begin{itemize}
    \item Entradas
    \begin{enumerate}
      \item \texttt{NORMALIZED DATASET CSV PATH}
      \item \texttt{NORMALIZED DATASET CSV PATH}
      \item \texttt{OUTPUT DIRECTORY}
    \end{enumerate}
    \item Saídas
    \begin{itemize}
      \item \textbf{<DATASET 1>\_<DATASET 2>\_deterministic\_merged.csv} Arquivo que preserva todas as linhas do primeiro data set mesclando nelas os dados do segundo data set de acordo com pares que possuem o mesmo número de documento.
    \end{itemize}
  \end{itemize}
  \item $\texttt{Rscript remove\_paired\_by\_doc.R <1> <2> <3>}$ 
  
  OBS: Esse processo melhora a precisão e tempo do probabilístico ao reduzir o universo de possibilidades.
  \begin{itemize}
    \item Entradas
    \begin{enumerate}
      \item \texttt{NORMALIZED DATASET CSV PATH}
      \item \texttt{DETERMINISTIC MERGED DATASET CSV PATH}
      \item \texttt{OUTPUT DIRECTORY}
    \end{enumerate}
    \item Saídas
    \begin{itemize}
      \item \textbf{<DATASET 1>\_cleaned.csv} Arquivo que preserva todas as linhas do primeiro dataset cujo os documentos não constam no segundo.
    \end{itemize}
  \end{itemize}
\end{itemize}

\subsection{Como configurar um ambiente de desenvolvimento}

Caso seja de interesse criar um ambiente de desenvolvimento em uma máquina local, o que se deve fazer é seguir as
instruções no arquivo  \href{https://gitlab.com/pragmacode/vital-strategies-sadvt/-/blob/master/HACKING.md}{\texttt{HACKING.md}}.
Neste arquivo, encontram-se as principais dependências de desenvolvimento a serem instaladas e uma indicação de que
script executar para configurar as demais (bancos de dados, pacotes JS e as gemas necessárias)

\subsection{Adicionar um novo tipo de base de dados ao sistema}

Para adicionar um novo tipo de base de dados (chamado de \texttt{InputType} no contexto de desenvolvimento), é
necessário que se sigam os passos abaixo
\begin{enumerate}
  \item No arquivo \texttt{app/models/input\_type.rb}, alterar o valor da constante \textbf{\texttt{NAMES}}, colocando o
  nome do novo tipo de entrada.
  \item Gera migração para criar o tipo novo de base de dados
  \begin{itemize}
    \item Para ter uma ideia de como escrever a migração, pode-se olhar o arquivo abaixo
    \item $\texttt{db/migrate/20210116155138\_add\_samu\_input\_type.rb}$
    \item Não esquecer de rodar o comando \texttt{rails db:migrate}
  \end{itemize}
  \item Editar o arquivo \texttt{db/seeds.rb} de modo a acrescentar à lista de tipos de dados a nova entrada
  \item No arquivo $\texttt{config/locales/models/input\_type.pt-BR.yml}$, colocar o nome do tipo da base de dados nova
\end{enumerate}

\subsection{Adicionar um novo pareamento ao sistema}

Para adicionar um novo tipo de pareamento, é necessário que se tome as devidas providências:
\begin{itemize}
  \item coloque o nome do novo tipo de pareamento ao final da lista \texttt{KINDS} dentro do modelo \texttt{Pairing}
  \item elencar, na constante $\texttt{REQUIRED\_INPUTS}$ os tipos de bases de dados necessárias para a execução do novo
  tipo de pareamento.
  \item no arquivo \texttt{config/locales/models/pairing.pt-BR.yml}, dentro da chave \texttt{kinds}\footnote{linha 7},
  colocar uma outra chave com o nome desse novo tipo em \href{https://en.wikipedia.org/wiki/Snake_case}{snake case} com
  o seu nome escrito `normalmente' em seguida, como nos demais pares chave e valor presentes neste arquivo.
  \item por último, no arquivo \texttt{app/services/pairings/pipeline.rb}, é necessário incluir a pipeline do pareamento
  novo na constante $\texttt{PREPARE\_SAT\_SCRIPT}$
  \begin{itemize}
    \item A pipeline de um pareamento contém uma lista de scripts
    \item Cada script é um objeto com o seu nome, o do arquivo R, uma lista com as entradas e outra com as saídas esperadas
  \end{itemize}
\end{itemize}

\end{document}

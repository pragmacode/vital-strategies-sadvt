# SPVT - Disaster Recovery

## Data loss

### Prepare back ups

In order to avoid losing data, we need regular back-ups. These will follow these steps.
- Produce DB dumps in order to preserve the instances
  - it can be done by following the steps in `Generate DB dump through docker` section in
    [HACKING.md](https://gitlab.com/pragmacode/vital-strategies-sadvt/-/blob/master/HACKING.md)
- Copy ActiveStorage files in some other place
  - By copying the content from `${HOME}/storage` to another place, we can be sure that, if the
    uploads are lost, they can be easily restored

### Recovering data

Assuming the previous subsection measures have been taken before disaster, we can follow these
steps in order to recover lost data:
- You can recover the database data by running this command
  - `docker exec -it sadvt-database-postgresql13 pg_restore -U sadvt -d sadvt_production <db_dumb_file_path>`
- Copy the folder with the backed up file into the new application storage folder

## Data leak

Once data has leaked we can only mitigate the damage:

* if the data is still leaking
  - take down the application
  - fix the leak
  - bring the application back up
* warn affected users about what has leaked
* force password change for affected users

## Access peak

TODO: write this once the deployment is set

## Denial of Service attack

* contact the infrastrucure provider and ask for support on mitigating the attack
* indentify if the attack comes from a limited set of IP addresses
  - if so, block them

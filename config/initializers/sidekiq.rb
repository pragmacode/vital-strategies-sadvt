# Perform Sidekiq jobs immediately in development and test,
# so you don't have to run a separate process.
# You'll also benefit from code reloading.
unless Rails.env.production?
  require 'sidekiq/testing'
  Sidekiq::Testing.inline!
end

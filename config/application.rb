require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module VitalStrategiesSadvt
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.1

    # Configuration for the application, engines, and railties goes here.
    #
    # These settings can be overridden in specific environments using the files
    # in config/environments, which are processed later.
    #
    config.time_zone = 'America/Sao_Paulo'
    # config.eager_load_paths << Rails.root.join("extras")

    config.active_job.queue_adapter     = :sidekiq
    config.active_job.queue_name_prefix = 'spvt'

    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml}')]
    config.i18n.default_locale = :"pt-BR"
    config.i18n.available_locales = :"pt-BR"
  end
end

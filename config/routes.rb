Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root 'pairings#index'

  resources :pairings, except: %i[edit index] do
    get 'inputs', to: 'pairings#new_inputs'
    get 'run', to: 'pairings#run'
    get 'official_base', to: 'pairings#official_file_input'
    get 'select_input_columns', to: 'pairings#select_input_columns'
    patch 'pairing_columns', to: 'pairings#pick_pairing_columns'
    patch 'official_base_save', to: 'pairings#save_official_file'
  end

  get 'inputs/official_files'
  get 'descriptions/abbreviations'
end

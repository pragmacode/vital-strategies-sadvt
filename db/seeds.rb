# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
[
  {
    name: 'sat_vehicles',
    required_columns: %w[id_acidente id_veiculo]
  },
  {
    name: 'sat_victims',
    required_columns: %w[id_acidente id_veiculo nome sexo data_nascimento2 data_obito]
  },
  {
    name: 'sat_accidents',
    required_columns: %w[id_acidente data]
  },
  {
    name: 'sim',
    required_columns: %w[NOME SEXO DTNASC DTOBITO]
  },
  {
    name: 'sih',
    required_columns: %w[NOME P_SEXO P_DATN DT_INTERNA CNES P_DOC P_NDOC]
  },
  {
    name: 'samu',
    required_columns: %w[SEXO VÍTIMA ABERTURA CNES]
  },
  {
    name: 'official_file',
    required_columns: %w[-]
  },
  {
    name: 'sim_with_doc',
    required_columns: %w[NOME SEXO DTNASC DTOBITO DOC] # TODO: replace DOC by the correct doc col name
  },
  {
    name: 'samu_with_doc',
    required_columns: %w[SEXO VÍTIMA ABERTURA CNES DOC] # TODO: replace DOC by the correct doc col name
  },
  {
    name: 'infosiga',
    required_columns: %w[Nome\ pessoa Sexo\ da\ vítima Data\ de\ nascimento Data\ do\ óbito CPF]
  }
].each do |data|
  InputType.create!(name: data[:name], csv_columns_names: data[:required_columns].join(';'))
end

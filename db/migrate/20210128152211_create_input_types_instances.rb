class CreateInputTypesInstances < ActiveRecord::Migration[6.1]
  def up
    sat_type = InputType.find_by(name: 'sat')

    unless sat_type.nil?
      sat_type.inputs.destroy_all
      sat_type.destroy
    end

    InputType::NAMES.each do |name|
      InputType.create!(name: name, csv_columns_names: '-') unless InputType.exists?(name: name)
    end
  end

  def down
    InputType::NAMES.each do |name|
      input_type = InputType.find_by(name: name)

      unless input_type.nil?
        input_type.inputs.destroy_all
        input_type.detroy
      end
    end
  end
end

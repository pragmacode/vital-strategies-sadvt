class CreateInfosigaInputType < ActiveRecord::Migration[6.1]
  def up
    data = {
      name: 'infosiga',
      required_columns: %w[id_acidente id_veiculo nome sexo data_nascimento2 data_obito data DOC] # TODO: use the actual infosiga column names
    }

    input_type = InputType.find_by(name: data[:name])

    if input_type.nil?
      InputType.create!(name: data[:name], csv_columns_names: data[:required_columns].join(';'))
    else
      input_type.update!(csv_columns_names: data[:required_columns].join(';'))
    end
  end

  def down
    input_type = InputType.find_by(name: 'infosiga')
    input_type.inputs.destroy_all
    input_type.destroy!
  end
end

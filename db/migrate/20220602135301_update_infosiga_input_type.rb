class UpdateInfosigaInputType < ActiveRecord::Migration[6.1]
  def up
    data = {
      name: 'infosiga',
      required_columns: %w[Nome\ pessoa Sexo\ da\ vítima Data\ de\ nascimento Data\ do\ óbito CPF]
    }

    input_type = InputType.find_by(name: data[:name])

    if input_type.nil?
      raise Exception.new "Exception: The InputType InfoSIGA should exist"
    else
      input_type.update!(csv_columns_names: data[:required_columns].join(';'))
    end
  end

  def down
    old_data = {
      name: 'infosiga',
      required_columns: %w[id_acidente id_veiculo nome sexo data_nascimento2 data_obito data DOC]
    }
    input_type = InputType.find_by(name: old_data[:name])
    input_type.update!(csv_columns_names: old_data[:required_columns].join(';'))
  end
end

class AddIsFinalToOutputs < ActiveRecord::Migration[6.1]
  def change
    add_column :outputs, :is_final, :boolean, default: false
    add_index :outputs, :is_final
  end
end

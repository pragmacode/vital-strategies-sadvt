class CreatePairings < ActiveRecord::Migration[6.0]
  def change
    create_table :pairings do |t|
      t.string :script_name, null: false

      t.timestamps
    end
  end
end

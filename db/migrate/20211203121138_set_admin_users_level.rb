class SetAdminUsersLevel < ActiveRecord::Migration[6.1]
  def change
    User.where(admin: true).update_all(level: 'admin')
  end
end

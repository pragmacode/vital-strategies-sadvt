class AddAasmStateToPairings < ActiveRecord::Migration[6.0]
  def change
    add_column :pairings, :aasm_state, :string
  end
end

class RenamePairingsScriptNameToKind < ActiveRecord::Migration[6.0]
  def change
    rename_column :pairings, :script_name, :kind
  end
end

class CreateSimWithDocInputType < ActiveRecord::Migration[6.1]
  def up
    data = {
      name: 'sim_with_doc',
      required_columns: %w[NO_FALECIDO SG_SEXO DT_NASCIMENTO DT_OBITO DOC] # TODO: replace DOC by the correct doc col name
    }

    input_type = InputType.find_by(name: data[:name])

    if input_type.nil?
      InputType.create!(name: data[:name], csv_columns_names: data[:required_columns].join(';'))
    else
      input_type.update!(csv_columns_names: data[:required_columns].join(';'))
    end
  end

  def down
    InputType.find_by(name: 'sim_with_doc').destroy!
  end
end

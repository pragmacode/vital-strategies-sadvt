class AllowInputsToHaveNullPairing < ActiveRecord::Migration[6.1]
  def change
    change_column_null :inputs, :pairing_id, true
  end
end

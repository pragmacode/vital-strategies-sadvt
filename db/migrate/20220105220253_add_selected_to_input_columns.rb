class AddSelectedToInputColumns < ActiveRecord::Migration[6.1]
  def change
    add_column :input_columns, :selected, :boolean, default: true
  end
end

class CreatePairingOutput < ActiveRecord::Migration[6.0]
  def change
    create_table :pairing_outputs do |t|
      t.references :pairing, null: false, foreign_key: true
      t.string :script_name, null: false
      t.timestamps
    end
  end
end

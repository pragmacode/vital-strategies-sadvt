class AddRequiredColumnsToInputType < ActiveRecord::Migration[6.1]
  def change
    add_column :input_types, :csv_columns_names, :string
  end
end

class AddUniquenessIndexToInputTypeName < ActiveRecord::Migration[6.1]
  def change
    add_index :input_types, :name, unique: true
  end
end

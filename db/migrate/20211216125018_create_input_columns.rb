class CreateInputColumns < ActiveRecord::Migration[6.1]
  def change
    create_table :input_columns do |t|
      t.string :title
      t.references :input, null: false, foreign_key: true

      t.timestamps
    end

    add_index :input_columns, [:input_id, :title], unique: true
  end
end

# frozen_string_literal: true

class CreateSamuWithDocInputType < ActiveRecord::Migration[6.1]
  def up
    data = {
      name: 'samu_with_doc',
      required_columns: %w[SEXO VÍTIMA ABERTURA CNES DOC].join(';')
    }

    input_type = samu_with_doc_input_type

    if input_type.nil?
      InputType.create!(name: data[:name], csv_columns_names: data[:required_columns])
    else
      input_type.update!(csv_columns_names: data[:required_columns])
    end
  end

  def down
    samu_with_doc_input_type.inputs.destroy_all
    samu_with_doc_input_type.destroy!
  end

  private

  def samu_with_doc_input_type
    InputType.find_by(name: 'samu_with_doc')
  end
end

# frozen_string_literal: true

class FixSihWithDocColumns < ActiveRecord::Migration[6.1]
  OLD_COLUMNS = %w[NO_PACIENTE P_SEXO DT_NASCIMENTO DT_INTERNA CNES DOC].join(';')
  NEW_COLUMNS = %w[NOME P_SEXO P_DATN DT_INTERNA CNES DOC].join(';')

  def up
    update_input_type(NEW_COLUMNS)
  end

  def down
    update_input_type(OLD_COLUMNS)
  end

  private

  def update_input_type(joined_set_of_columns)
    input_type = InputType.find_by(name: 'sih_with_doc')
    input_type&.update!(csv_columns_names: joined_set_of_columns)
  end
end

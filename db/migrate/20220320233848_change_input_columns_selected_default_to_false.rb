class ChangeInputColumnsSelectedDefaultToFalse < ActiveRecord::Migration[6.1]
  def change
    change_column_default :input_columns, :selected, false
  end
end

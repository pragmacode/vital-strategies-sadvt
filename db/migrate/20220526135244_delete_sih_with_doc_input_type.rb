class DeleteSihWithDocInputType < ActiveRecord::Migration[6.1]
  def up
    data = {
      name: 'sih_with_doc',
      required_columns: %w[NOME P_SEXO P_DATN DT_INTERNA CNES DOC],
    }

    input_type = InputType.find_by(name: data[:name])

    if input_type.nil?
      raise Exception.new "Exception: The InputType SIH with DOC should exist"
    else
      input_type.inputs.destroy_all
      input_type.destroy!
    end
  end

  def down
    data = {
      name: 'sih_with_doc',
      required_columns: %w[NOME P_SEXO P_DATN DT_INTERNA CNES DOC],
    }

    input_type = InputType.find_by(name: data[:name])
    InputType.create!(name: data[:name], csv_columns_names: data[:required_columns].join(';'))
  end 
end

class SetNecessaryColumnsToInputTypes < ActiveRecord::Migration[6.1]
  def change
    [
      {
        name: 'sat_vehicles',
        required_columns: %w[id_acidente id_veiculo]
      },
      {
        name: 'sat_victims',
        required_columns: %w[id_acidente id_veiculo nome sexo data_nascimento2 data_obito]
      },
      {
        name: 'sat_accidents',
        required_columns: %w[id_acidente data]
      },
      {
        name: 'sim',
        required_columns: %w[NO_FALECIDO SG_SEXO DT_NASCIMENTO DT_OBITO]
      },
      {
        name: 'sih',
        required_columns: %w[NOME P_SEXO P_DATN DT_INTERNA CNES]
      },
      {
        name: 'samu',
        required_columns: %w[SEXO VÍTIMA ABERTURA CNES]
      }
    ].each do |type|
      input_type = InputType.find_by(name: type[:name])

      return unless input_type

      input_type.csv_columns_names = type[:required_columns].join(';')
      input_type.save!
    end
  end

  def down
    InputType::NAMES.each do |input_names|
      input_type = InputType.find_by(name: input_names)

      return unless input_type

      input_type.csv_columns_names = ''
      input_type.save!
    end
  end
end

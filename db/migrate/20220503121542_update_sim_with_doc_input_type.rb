class UpdateSimWithDocInputType < ActiveRecord::Migration[6.1]
  def up
    data = {
      name: 'sim_with_doc',
      required_columns: %w[NOME SEXO DTNASC DTOBITO DOC] # TODO: replace DOC by the correct doc col name
    }

    input_type = InputType.find_by(name: data[:name])

    if input_type.nil?
      raise Exception.new "Exception: The InputType SIM with DOC should exist"
    else
      input_type.update!(csv_columns_names: data[:required_columns].join(';'))
    end
  end

  def down
    old_data = {
      name: 'sim_with_doc',
      required_columns: %w[NO_FALECIDO SG_SEXO DT_NASCIMENTO DT_OBITO DOC]
    }
    input_type = InputType.find_by(name: old_data[:name])
    input_type.update!(csv_columns_names: old_data[:required_columns].join(';'))
  end
end

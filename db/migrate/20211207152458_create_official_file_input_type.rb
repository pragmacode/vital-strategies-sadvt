class CreateOfficialFileInputType < ActiveRecord::Migration[6.1]
  def up
    return if InputType.find_by(name: 'official_file')

    InputType.create(name: 'official_file', csv_columns_names: '-')
  end

  def down
    input_type = InputType.find_by(name: 'official_file')

    return if input_type.nil?

    input_type.inputs.destroy_all
    input_type.destroy

  end
end

class UpdateSihRequiredColumns < ActiveRecord::Migration[6.1]
  def up
    data = {
      name: 'sih',
      required_columns: %w[NOME P_SEXO P_DATN DT_INTERNA CNES P_DOC P_NDOC]
    }

    input_type = InputType.find_by(name: data[:name])

    if input_type.nil?
      raise Exception.new "Exception: The InputType SIM should exist"
    else
      input_type.update!(csv_columns_names: data[:required_columns].join(';'))
    end
  end

  def down
    old_data = {
      name: 'sih',
      required_columns: %w[NOME P_SEXO P_DATN DT_INTERNA]
    }
    input_type = InputType.find_by(name: old_data[:name])
    input_type.update!(csv_columns_names: old_data[:required_columns].join(';'))
  end
end

  
class CreateSihWithDocInputType < ActiveRecord::Migration[6.1]
  def up
    data = {
      name: 'sih_with_doc',
      required_columns: %w[NO_PACIENTE P_SEXO DT_NASCIMENTO DT_INTERNA CNES DOC],
    }

    input_type = InputType.find_by(name: data[:name])
    puts input_type

    if input_type.nil?
      InputType.create!(name: data[:name], csv_columns_names: data[:required_columns].join(';'))
    else
      input_type.update!(csv_columns_names: data[:required_columns].join(';'))
    end
  end

  def down
    input_type = InputType.find_by(name: 'sih_with_doc')
    input_type.inputs.destroy_all
    input_type.destroy!
  end
end
